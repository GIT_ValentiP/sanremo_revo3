/*
 *****************************************************************************
 *  @file      : balance.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 05 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

/* Includes ----------------------------------------------------------*/
#include "balance.h"
#include "gpio.h"
#include "usart.h"
#include "error.h"
#include "ifx9201sg_doser.h"
#include "string.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/


#define TIMEOUT1_BALANCE_RX_WEIGHT_100ms  3
#define TIMEOUT1_BALANCE_RX_ACK_100ms    10

#define DUTY_DOSER_TIME_WEIGHING  60
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
extern UART_HandleTypeDef huart2;
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;
/* USART2_RX Init */
//   hdma_usart2_rx.Instance = DMA1_Stream5;
//   hdma_usart2_rx.Init.Channel = DMA_CHANNEL_4;
/* USART2_TX Init */
//   hdma_usart2_tx.Instance = DMA1_Stream6;
//   hdma_usart2_tx.Init.Channel = DMA_CHANNEL_4;

uint16_t balance_delta_weight[20]={150,130,120,105,  85, 70, 50, 45, 35, 30, 25, 23, 20, 18, 15, 13, 12,  8,  5, 3};
uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10,10};//FW 0.78
//FW 0.79 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,12};
//FW 0.78 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10,10};
//FW 0.77 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100, 12, 12, 12,  8,  8,  8,  8,  8,  8,  8, 8};
//FW 0.76 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100,  8,  8,  8,  6,  6,  6,  6,  6,  6,  6, 6};
//FW 0.67 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100,100,100, 12, 12, 12,  8,  8,  8,  8,  8,  8,  8, 8};
//uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100, 90, 90, 60, 50, 40, 35, 25, 15, 15, 12, 12, 12,12};
//FW v0.60 uint16_t   balance_duty_doser[20]={100,100,100,100, 100,100,100, 12, 12, 12, 12, 12, 8, 8, 8, 8, 8, 8, 8,8};
// Fiera : uint16_t   balance_duty_doser[20]={ 60, 60, 60, 60, 60, 45, 35, 35, 25,15, 15,15,15,15,15,15,15,15,15,15};


#ifdef HBM_AD105x
  uint8_t hbm_AD105_command[LAST_HBM_CMD][16]={
											  "TAR;",
											  "CWT;",
											  "MSV?;",
											  "BDR115200,0;",
											  "ICR0;",
											  "TDD1;",
											  "IDN?;",//"SWV?;",
											  "BDR?;",
											  "DPT3;",
											  "RSN1;",
											  "NOV300000;",
											  "LDW2147483647;",
											  "LWT128542;",
											  "LDW?;",
											  "LWT?;",
											  "CWT133333;",
											  "LDW",
											  "LWT2147483647;",
											  "COF0;",
											  "SPWAED;",
											  "HSM0;",
											  "FMD3;",
											  "ASF4;",
											  "FTL0;",
											  "NTF10;",
											  "NTF20;",
											};

  void balance_send_message(uint8_t *msg,uint32_t timeout,uint8_t id_msg,uint8_t nbyte_rx);
#endif

static uint32_t peso_gr_dec;
/* Private function prototypes -----------------------------------------------*/
uint32_t balance_read_weight(void);
void balance_send_cmd(balance_CMD_enum_t cmd,uint16_t len);
void balance_update_weight_act(uint16_t wgt);
void balance_update_main_status(void);
void send_tara_cmd(void);
void balance_end_weighing_status(void);

/* Private user code ---------------------------------------------------------*/



void balance_update_weight_act(uint16_t wgt){
	switch(balance.doser){
	   case(DOSER_1):
		             hopper_burr[HOPPER_1].hopper_weight_rd=wgt;
	                 hopper_burr[HOPPER_1].hopper_time_rd=balance.time_target-balance.timeout_Weight;
		    	     break;
	   case(DOSER_2):
	 		         hopper_burr[HOPPER_2].hopper_weight_rd=wgt;
	                 hopper_burr[HOPPER_2].hopper_time_rd=balance.time_target-balance.timeout_Weight;
	 		    	 break;
	   case(DOSER_3):
	 		         hopper_burr[HOPPER_3].hopper_weight_rd=wgt;
	                 hopper_burr[HOPPER_3].hopper_time_rd=balance.time_target-balance.timeout_Weight;
	 		         break;
	   default:
		   hopper_burr[HOPPER_1].hopper_weight_rd=wgt;
		   hopper_burr[HOPPER_2].hopper_weight_rd=wgt;
		   hopper_burr[HOPPER_3].hopper_weight_rd=wgt;
		   hopper_burr[HOPPER_1].hopper_time_rd=balance.time_target-balance.timeout_Weight;
		   hopper_burr[HOPPER_2].hopper_time_rd=balance.time_target-balance.timeout_Weight;
		   hopper_burr[HOPPER_3].hopper_time_rd=balance.time_target-balance.timeout_Weight;

		  break;
	}
}

void balance_update_main_status(void){
	/*Main status*/

	  switch(balance.doser){
	  	   case(DOSER_1):
//						if ((balance.state==START_MEASURE_BALANCE)||(balance.state==START_MEASURE_TIME)){
						revo_state=REVO_WEIGHT1_RUN;
//						}
//						else{
//						 revo_state=REVO_WEIGHT1_END;
//						}
	  		    	     break;
	  	   case(DOSER_2):
//		                if ((balance.state==START_MEASURE_BALANCE)||(balance.state==START_MEASURE_TIME)){
						revo_state=REVO_WEIGHT2_RUN;
//						}
//		                else{
//						 revo_state=REVO_WEIGHT2_END;
//						}
	  	 		    	 break;
	  	   case(DOSER_3):
//		                if ((balance.state==START_MEASURE_BALANCE)||(balance.state==START_MEASURE_TIME)){
						revo_state=REVO_WEIGHT3_RUN;
//						}else{
//						 revo_state=REVO_WEIGHT3_END;
//						}
	  	 		         break;
	  	   default:
	  		       break;
	  	}

}

void balance_end_weighing_status(void){
	/*Main status*/

	  switch(balance.doser){
	  	   case(DOSER_1):
						 revo_state=REVO_WEIGHT1_END;
	  		    	     break;
	  	   case(DOSER_2):
						 revo_state=REVO_WEIGHT2_END;
	  	 		    	 break;
	  	   case(DOSER_3):
						 revo_state=REVO_WEIGHT3_END;
	  	 		         break;
	  	   default:
	  		 revo_state=REVO_WEIGHT1_END;
	  	     revo_state=REVO_WEIGHT2_END;
	  		 revo_state=REVO_WEIGHT3_END;
	  		 break;
	  	}

	  cmd_rx_timeout=150;

}




bool balance_weighing_procedure(doser_enum_t id_doser,uint16_t accuracy,uint16_t target){
    uint32_t delta =1000;
	bool result=FALSE ;
	uint16_t pwm_duty;
	uint8_t i;
	uint16_t target_to_reach;
	target_to_reach=target;
    if (balance.new_weight==TRUE){
    	//HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port , LED5_SERVICE_Pin,GPIO_PIN_SET);
        if (balance.weight_rd < target_to_reach){
        	delta=target-balance.weight_rd ;
        }else{
        	delta=balance.weight_rd- target_to_reach;
        }

        if ((delta < accuracy)||(balance.weight_rd > (target_to_reach - 2) )){
        	result=TRUE;
        	doser_vibro[id_doser].DIS_pin=STOP_MOT;
        	doser_vibro[id_doser].DIR_pin=DIR_FORWARD;
        	pwm_duty=0;
        	switch(balance.doser){
        		   case(DOSER_1):
		                         //HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port , LED4_SERVICE_Pin,GPIO_PIN_RESET);
        			    	     break;
        		   case(DOSER_2):
		                         //HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port , LED5_SERVICE_Pin,GPIO_PIN_RESET);
        		 		    	 break;
        		   case(DOSER_3):
		                         //HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port , LED6_SERVICE_Pin,GPIO_PIN_RESET);
        		 		         break;
        		   default:
        			       break;
        		}

        }else{
			   i=0;
			   while(i<20){
				   if (delta < balance_delta_weight[i]){
					   i++;
				   }else {
					   break;
				   }
			   }
			   if (i==20){
				   i--;
			   }
			   doser_vibro[id_doser].DIS_pin=START_MOT;
			   doser_vibro[id_doser].DIR_pin=DIR_REVERSE;
			   pwm_duty=balance_duty_doser[i];
             }

        doser_vibro[id_doser].cmd.b.DIS=1;//new DIS for DOSER
    	doser_vibro[id_doser].cmd.b.DUTY=1;//new DUTY for DOSER
    	doser_vibro[id_doser].cmd.b.DIR =1;
    	doser_vibro[id_doser].pwm_duty =pwm_duty ;
    	//printf("\n\r delta gr/10:%lu",delta);
    	balance.new_weight=FALSE;
    }

	return(result);
}






#ifdef HBM_AD105x


uint32_t balance_read_weight(void){
	peso_gr_dec=balance.rs422_rx[2];
	peso_gr_dec=(peso_gr_dec << 8)+balance.rs422_rx[3];
	peso_gr_dec=(peso_gr_dec << 8)+balance.rs422_rx[4];
	peso_gr_dec=(peso_gr_dec/10);
	return(peso_gr_dec);
}

void check_rx_balance(void){

	if (revo_calibration_cmd.cal_b.WEIGH_CAL_START==1){
		 //Start Calibration
		 nrepeat_calibra=0;
		 nrepeat_tara=0;
		 balance.state= CALIB_BALANCE;//START_CALIB_BALANCE;//
		 revo_calibration_cmd.cal_b.WEIGH_CAL_START=0;

	}else if ((new_data_rx_balance==TRUE)&&(balance.state==WEIGHING_BALANCE)){
			//if((balance.rs422_rx[1]==START2_WEIGHT_FRAME)&&(balance.rs422_rx[5]==END_WEIGHT_FRAME)){
				peso_gr_dec=0;
				peso_gr_dec=((balance.rs422_rx[0]*0x10000)+(balance.rs422_rx[1]*256)+balance.rs422_rx[2]);
				if (peso_gr_dec < PESO_MAX_MG){
					if (peso_gr_dec > (balance.weight_rd*100)){
					  balance.weight_rd=(peso_gr_dec/100);//balance_read_weight();
					  balance.timeout_Weight=TIMEOUT_WEIGHING;
					  if ( balance.weight_rd > (balance.weight_target + 1)){
						  balance.weight_rd=balance.weight_rd-1;
					  }
					}
				 }else{
					balance.weight_rd=0;//balance_read_weight();
				 }
				balance_update_weight_act(balance.weight_rd) ;
			    balance.new_weight=TRUE;
			//}
			balance.state=WEIGHING_BALANCE;
			balance.len_RX=6;
			new_data_rx_balance=FALSE;
    }

}

void balance_send_message(uint8_t *msg,uint32_t timeout,uint8_t id_msg,uint8_t nbyte_rx){
	 uint8_t len_tx;
	 RS422_RX_CELL_DISABLE;//RS422_RX_CELL_ENABLE;//
	 RS422_TX_CELL_ENABLE;
	 len_tx=strlen((char *)hbm_AD105_command[id_msg]);
	 balance.cmd=id_msg;
     HAL_UART_Transmit(&huart2,&hbm_AD105_command[id_msg][0],len_tx, 10000);
	 balance.timeout_RX=timeout;
	 while(balance.timeout_RX!=0){}
	 if (nbyte_rx!=0){
		 RS422_TX_CELL_DISABLE;//
		 RS422_RX_CELL_ENABLE;
		 HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX ,nbyte_rx);
	 }

}

void init_balance(void){
//Initialize UART2 RS422 Full-Duplex
//115200 bps ;1 bit STOP;No parity;8bits  data
	//uint8_t i;

	  RS422_RX_CELL_DISABLE;//RS422_RX_CELL_ENABLE;//
	  RS422_TX_CELL_ENABLE;
	  MX_USART2_UART_Init(9600,UART_PARITY_NONE);
	  HAL_UART_MspInit(&huart2);//BALANCE RS422 Full-Duplex Communication
	  HAL_UART_Transmit(&huart2,&hbm_AD105_command[SW_VER_HBM_CMD][0],5, 10000);
	  RS422_TX_CELL_DISABLE;//
	  RS422_RX_CELL_ENABLE;
	  balance.len_RX=33;
	  balance.cmd=SW_VER_HBM_CMD;
	  HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX ,33);
	  balance.state=SCAN_BAUD_BALANCE;
	  balance.timeout_RX=50;
	  balance.sw_ver[0]=0;
	  balance.sw_ver[1]=0;
	  balance.baud=9600;
	  while((balance.timeout_RX!=0)&&(balance.sw_ver[0]==0)){}
	  if(balance.sw_ver[0]!=0) {
			  RS422_RX_CELL_DISABLE;//RS422_RX_CELL_ENABLE;//
			  RS422_TX_CELL_ENABLE;
			  balance.sw_ver[0]=0;
			  balance.sw_ver[1]=0;
			  balance.len_RX=0;
			  balance.cmd=BAUD_HBM_CMD;
			  HAL_UART_Transmit(&huart2,&hbm_AD105_command[BAUD_HBM_CMD][0],12, 10000);
			  balance.timeout_RX=5;
			  while(balance.timeout_RX!=0){}
			  balance.cmd=SAVE_HBM_CMD;
			  HAL_UART_Transmit(&huart2,&hbm_AD105_command[SAVE_HBM_CMD][0],5, 10000);
			  balance.timeout_RX=5;
			  while(balance.timeout_RX!=0){}
	  }
	  MX_USART2_UART_Init(115200,UART_PARITY_NONE);
	  HAL_UART_MspInit(&huart2);//BALANCE RS422 Full-Duplex Communication
	  balance.timeout_RX=5;
	  while(balance.timeout_RX!=0){}
	  RS422_RX_CELL_DISABLE;//RS422_RX_CELL_ENABLE;//
	  RS422_TX_CELL_ENABLE;
	  balance.sw_ver[0]=0;
	  balance.sw_ver[1]=0;
	  balance.baud=115200;
	  balance.state=SCAN_BAUD_BALANCE;
	  balance.len_RX=33;
	  balance.timeout_RX=15;
	  HAL_UART_Transmit(&huart2,&hbm_AD105_command[SW_VER_HBM_CMD][0],5, 10000);
	  RS422_TX_CELL_DISABLE;//
	  RS422_RX_CELL_ENABLE;
	  HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX ,33);
	  //balance.state=SCAN_BAUD_BALANCE;
	  while((balance.timeout_RX!=0)&&(balance.sw_ver[0]==0)){}
	  balance.len_RX=0;

	  balance_send_message(hbm_AD105_command[SPW_PSW_HBM_CMD],5,SPW_PSW_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[DPT_HBM_CMD],5,DPT_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[RSN_HBM_CMD],5,RSN_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[NOM_VAL_HBM_CMD],5,NOM_VAL_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[HSM_HBM_CMD],5,HSM_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[CONV_RATE_HBM_CMD],5,CONV_RATE_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[FMD_HBM_CMD],5,FMD_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[ASF_HBM_CMD],5,ASF_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[FTL_HBM_CMD],5,FTL_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[NOTCH1_HBM_CMD],5,NOTCH1_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[NOTCH2_HBM_CMD],5,NOTCH2_HBM_CMD,0);
	  balance_send_message(hbm_AD105_command[SAVE_HBM_CMD],5,SAVE_HBM_CMD,0);
//	  balance.cmd=SAVE_HBM_CMD;
//	  HAL_UART_Transmit(&huart2,&hbm_AD105_command[SAVE_HBM_CMD][0],5, 10000);
//	  balance.timeout_RX=5;
//	  while(balance.timeout_RX!=0){}




	  balance.state=IDLE_BALANCE;
	  nrepeat_tara=0;
	  //printf("\n\r Balance FW:%3ld.%3ld",balance.sw_ver[0],balance.sw_ver[1]);
	  version_fw[2]=(((uint16_t)(balance.sw_ver[0]<<8)+((uint16_t)balance.sw_ver[1])));
	  printf("\n\r Balance FW:%2ld.%ld",version_fw[2]/256,balance.sw_ver[1]);
}

void scan_baudrate_balance(void){
//Initialize UART2 RS422 Full-Duplex
//115200 bps ;1 bit STOP;No parity;8bits  data


}


bool balance_RX_IT(void){
   bool ret=FALSE;
   uint8_t i=0;
   //bool trovato=FALSE;
   switch(balance.cmd){
     case(BAUD_HBM_CMD):
     case(SAVE_HBM_CMD):
	                         break;
     case(SW_VER_HBM_CMD):
	                    	if (balance.len_RX>32){
								   RS422_RX_CELL_DISABLE;
								   RS422_TX_CELL_ENABLE;
								   i=(balance.len_RX-1);
								   if ((buffer_BALANCE_RX[i-1]==END1_HBM_FRAME)&& (buffer_BALANCE_RX[i]==END2_HBM_FRAME)){
									   balance.sw_ver[0]=((buffer_BALANCE_RX[i-3]- 0x30)*10+(buffer_BALANCE_RX[i-2]- 0x30));
									   balance.sw_ver[1]=0;
									   ret=TRUE;
								   }
								   //printf("\n\rBalance:%c%c%c",buffer_BALANCE_RX[0],buffer_BALANCE_RX[1],buffer_BALANCE_RX[2]);
								   for(i=0;i<balance.len_RX;i++){
									   buffer_BALANCE_RX[i]=0;
								   }
                               balance.timeout_RX=2;
							}
                            //printf("\n\rFW :%ld.%ld",balance.sw_ver[0],balance.sw_ver[1]);


    		              break;
     default:
						 if (balance.len_RX>1){
							   RS422_RX_CELL_DISABLE;
							   RS422_TX_CELL_ENABLE;//
							   //trovato=FALSE;
							   i=0;
							   if ((buffer_BALANCE_RX[4]==END1_HBM_FRAME)&& (buffer_BALANCE_RX[5]==END2_HBM_FRAME)){
								   balance.rs422_rx[0]=buffer_BALANCE_RX[0];
								   balance.rs422_rx[1]=buffer_BALANCE_RX[1];
								   balance.rs422_rx[2]=buffer_BALANCE_RX[2];
								   balance.rs422_rx[3]=buffer_BALANCE_RX[3];
								   balance.rs422_rx[4]=buffer_BALANCE_RX[4];
								   balance.rs422_rx[5]=buffer_BALANCE_RX[5];
								   //trovato=TRUE;
								   ret=TRUE;
							   }else{
								   printf("\n\r Frame:%2X-%2X-%2X",buffer_BALANCE_RX[0],buffer_BALANCE_RX[1],buffer_BALANCE_RX[2]);
							   }

							   for(i=0;i<6;i++){
								   buffer_BALANCE_RX[i]=0;
							   }
							   //RS422_RX_CELL_DISABLE;
							   //RS422_TX_CELL_ENABLE;//
							   //HAL_UART_Transmit(&huart2,&hbm_AD105_command[WEIGHT_HBM_CMD][0],5, 1000);
							   RS422_TX_CELL_DISABLE;//
							   RS422_RX_CELL_ENABLE;
							   HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX , 6);
							   balance.timeout_RX=1;
						   }
						 break;


   }


   return(ret);
}



void balance_send_cmd(balance_CMD_enum_t cmd,uint16_t len){
uint8_t i;
    RS422_RX_CELL_DISABLE;//
	RS422_TX_CELL_ENABLE;
	for(i=0;i<6;i++){
 		balance.rs422_rx[i]=0;
    }
	switch(cmd){
	  case(TARA_BAL_CMD):

		                      HAL_UART_Transmit(&huart2,&hbm_AD105_command[TARA_HBM_CMD][0],4, 1000);
		 	                  break;
	  case(CALIB_BAL_CMD):
	 		 	              break;
	  case(WEIGHT_BAL_CMD):

		                      HAL_UART_Transmit(&huart2,&hbm_AD105_command[WEIGHT_HBM_CMD][0],5, 1000);
	 		 	              break;
	  case(STOP_BAL_CMD):
	 		 	              break;
	  default:
		      break;
	}
	RS422_TX_CELL_DISABLE;//
	RS422_RX_CELL_ENABLE;
	//printf("\n\rTx:%d",cmd);
	if (len>1){
		RS422_TX_CELL_DISABLE;//
		RS422_RX_CELL_ENABLE;
		HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX , 6);
	}

}

void balance_weight_manager(void){


	check_rx_balance();


	switch(balance.state){

	  case(IDLE_BALANCE):
		                    //balance.state=TARA_BALANCE;
			                break;
	  case(INIT_BALANCE):
		                    init_balance();
	                        //balance.state=IDLE_BALANCE;
	                        //nrepeat_tara=0;
	 			            break;
	  case(SCAN_BAUD_BALANCE):

			                  break;
	  case(START_CALIB_BALANCE):
		 			            break;
	  case(CALIB_BALANCE):
	 			            break;
	  case(START_MEASURE_BALANCE):
		                   if (balance.timeout_RX==0){
								balance.weight_rd=0;
								balance_update_weight_act(balance.weight_rd) ;
								balance.timeout_Weight=TIMEOUT_WEIGHING;
								balance.cmd=TARA_BAL_CMD;
								balance.len_RX=1;
								balance_send_cmd (balance.cmd,1);
								balance.state=WAIT_END_MEAS_BALANCE;
								balance.timeout_RX=1;
								nrepeat_tara=0;
		                    }
	 			            break;

	  case(START_MEASURE_TIME):

							balance.weight_rd=0;
							balance.timeout_Weight=balance.time_target;
							balance_update_main_status();
							balance_update_weight_act(balance.weight_rd) ;
							doser_vibro[balance.doser].DIS_pin=START_MOT;
							doser_vibro[balance.doser].DIR_pin=DIR_REVERSE;
							doser_vibro[balance.doser].pwm_duty =DUTY_DOSER_TIME_WEIGHING ;
							doser_vibro[balance.doser].cmd.b.DIS =1;//new DIS for DOSER
							doser_vibro[balance.doser].cmd.b.DUTY=1;//new DUTY for DOSER
							doser_vibro[balance.doser].cmd.b.DIR =1;

							balance.state=WAIT_END_MEAS_TIME;
	  	 			        break;
	  case(WAIT_END_MEAS_TIME):

							if (balance.timeout_Weight>0){
								 balance_update_weight_act(balance.weight_rd) ;
							}else{//timeout expired
								//balance.time_target=0;
								balance.timeout_Weight=0;
								balance_update_weight_act(balance.weight_rd) ;

								balance_end_weighing_status();//balance_update_main_status();
								doser_vibro[balance.doser].DIS_pin=STOP_MOT;
								doser_vibro[balance.doser].pwm_duty  =0;
								doser_vibro[balance.doser].cmd.b.DIS =1;//new DIS for DOSER
								doser_vibro[balance.doser].cmd.b.DUTY=1;//new DUTY for DOSER
								balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
							}
		 	  	 		    break;

	  case(STOP_BALANCE):


	 			            break;
	  case(ERR_BALANCE):

	 			            break;
	  case(RESEND_CMD_BALANCE):

	 			            break;
	  case(WAIT_ACK_BALANCE):
	 			            break;
	  case(WAIT_END_MEAS_BALANCE)://Start weighing measure 'M' after
		                    if (balance.timeout_RX==0){
								balance.cmd=WEIGHT_BAL_CMD;
								balance.len_RX=6;
								balance.timeout_RX=1;
								balance.state=WEIGHING_BALANCE;
								balance_send_cmd (balance.cmd,6);
								nrepeat_tara=0;
		                    }
	  	 			        break;
	  case(TARA_BALANCE):

	  	 			        break;

	  case(END_TARA_WEIGHT_BALANCE)://wait ACK after TARA command 'T' at start of weighing cycle

	 	  	 			    break;
	  case(WEIGHING_BALANCE):
//  if (balance.timeout_RX==0){
//	  balance.len_RX=6;
//	  balance_RX_IT();
//	  balance.state=WEIGHING_BALANCE;
//  }
							  balance.len_RX=6;
							  if (balance.timeout_Weight>0){

								 if (balance_weighing_procedure(balance.doser,TOLERANCE_WEIGHT_BALANCE,balance.weight_target)==TRUE){
									 balance_end_weighing_status();//balance_update_main_status();
									 balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
									 peso_gr_dec=0;
									 balance.len_RX=0;
									 nrepeat_tara=0;
								 }else{
									 if (balance.timeout_RX==0){
											balance.cmd=WEIGHT_BAL_CMD;
											balance.len_RX=6;
											balance.timeout_RX=1;
											balance.state=WEIGHING_BALANCE;
											balance_send_cmd (balance.cmd,6);
											nrepeat_tara=0;
									 }
								 }

							  }else{//timeout expired
										balance.weight_rd=9999;
										balance.new_weight=TRUE;
										balance_update_weight_act(balance.weight_rd) ;
										balance_weighing_procedure(balance.doser,TOLERANCE_WEIGHT_BALANCE,balance.weight_target);
										balance_end_weighing_status();//balance_update_main_status();
										balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
										peso_gr_dec=0;
										balance.len_RX=0;
										nrepeat_tara=0;
										error_set(ERROR_LOADCELL,0,10);

							  }
	 	  	 			    break;

	  default :
		        break;
	}

}

#else



uint32_t balance_read_weight(void){
	peso_gr_dec=balance.rs422_rx[2];
	peso_gr_dec=(peso_gr_dec << 8)+balance.rs422_rx[3];
	peso_gr_dec=(peso_gr_dec << 8)+balance.rs422_rx[4];
	peso_gr_dec=(peso_gr_dec/10);
	return(peso_gr_dec);
}
void check_rx_balance(void){

	if (revo_calibration_cmd.cal_b.WEIGH_CAL_START==1){
		 //Start Calibration
		 nrepeat_calibra=0;
		 nrepeat_tara=0;
		 balance.state= CALIB_BALANCE;//START_CALIB_BALANCE;//
		 revo_calibration_cmd.cal_b.WEIGH_CAL_START=0;

	}else if (new_data_rx_balance==TRUE){
			//if((balance.rs422_rx[1]==START2_WEIGHT_FRAME)&&(balance.rs422_rx[5]==END_WEIGHT_FRAME)){
				peso_gr_dec=0;
				peso_gr_dec=((balance.rs422_rx[2]*0x10000)+(balance.rs422_rx[3]*256)+balance.rs422_rx[4]);
				if (peso_gr_dec < PESO_MAX_MG){
					if (peso_gr_dec > (balance.weight_rd*100)){
					  balance.weight_rd=(peso_gr_dec/100);//balance_read_weight();
					  balance.timeout_Weight=TIMEOUT_WEIGHING;
					  if ( balance.weight_rd > (balance.weight_target + 1)){
						  balance.weight_rd=balance.weight_rd-1;
					  }

					}
				 }else{
					balance.weight_rd=0;//balance_read_weight();
				 }
				balance_update_weight_act(balance.weight_rd) ;
			    balance.new_weight=TRUE;
			//}
			balance.state=WEIGHING_BALANCE;
			balance.len_RX=6;
			new_data_rx_balance=FALSE;
    }

}

void init_balance(void){
//Initialize UART2 RS422 Full-Duplex
//115200 bps ;1 bit STOP;No parity;8bits  data
	  RS422_RX_CELL_DISABLE;//RS422_RX_CELL_ENABLE;//
	  RS422_TX_CELL_ENABLE;

	  MX_USART2_UART_Init(115200,UART_PARITY_NONE);
	  HAL_UART_MspInit(&huart2);//BALANCE RS422 Full-Duplex Communication
//	  balance.doser=DOSER_1;
//	  balance.weight_rd=0;
//	  balance.weight_target=153;
//	  balance.state=START_MEASURE_BALANCE;//IDLE_BALANCE;
	  balance.sw_ver[0]=0x01;
	  balance.sw_ver[1]=0x13;
	  version_fw[2]=(((uint16_t)(balance.sw_ver[0]<<8)+((uint16_t)balance.sw_ver[1])));

}



bool balance_RX_IT(void){
   bool ret=FALSE;
   uint8_t i=0;
   bool trovato=FALSE;

   if (balance.len_RX>1){

	   RS422_TX_CELL_ENABLE;// RS422_TX_CELL_DISABLE;//
	   RS422_RX_CELL_ENABLE;
	   trovato=FALSE;
	   i=0;
	   while ((trovato==FALSE) && (i<7)){
		   if ((buffer_BALANCE_RX[i]==START1_WEIGHT_FRAME)&& (buffer_BALANCE_RX[i+1]==START2_WEIGHT_FRAME)){
			   balance.rs422_rx[0]=buffer_BALANCE_RX[i];
			   balance.rs422_rx[1]=buffer_BALANCE_RX[i+1];
			   balance.rs422_rx[2]=buffer_BALANCE_RX[i+2];
			   balance.rs422_rx[3]=buffer_BALANCE_RX[i+3];
			   balance.rs422_rx[4]=buffer_BALANCE_RX[i+4];
			   balance.rs422_rx[5]=buffer_BALANCE_RX[i+5];
			   trovato=TRUE;
			   ret=TRUE;
		   }else{
			   i++;
		   }
	   }
	   for(i=0;i<12;i++){
		   buffer_BALANCE_RX[i]=0;
	   }
	   balance.timeout_RX=TIMEOUT1_BALANCE_RX_WEIGHT_100ms;
	   HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX , 12);
   }

   return(ret);
}





void balance_send_cmd(balance_CMD_enum_t cmd,uint16_t len){
uint8_t i;
    RS422_RX_CELL_ENABLE;//RS422_RX_CELL_DISABLE;//
	RS422_TX_CELL_ENABLE;
	HAL_UART_Transmit(&huart2,&cmd,1, 1000);
	//printf("\n\rTx:%d",cmd);
	if (len>1){
		for(i=0;i<6;i++){
				balance.rs422_rx[i]=0;
		}
		RS422_TX_CELL_ENABLE;//RS422_TX_CELL_DISABLE;//
		RS422_RX_CELL_ENABLE;
		HAL_UART_Receive_IT(&huart2, (uint8_t *)buffer_BALANCE_RX , 12);

    }

}

void balance_weight_manager(void){


	check_rx_balance();


	switch(balance.state){

	  case(IDLE_BALANCE):
		                    //balance.state=TARA_BALANCE;
			                break;
	  case(INIT_BALANCE):
		                    init_balance();
	                        balance.state=IDLE_BALANCE;
	                        nrepeat_tara=0;
	 			            break;
	 case(START_CALIB_BALANCE):
			                   if (balance.timeout_RX==0){
										balance.weight_rd=0;
										balance_update_weight_act(balance.weight_rd) ;
										revo_state=REVO_WEIGHT_POS_CAL_RUN;
										if(++nrepeat_tara >2){
										   balance.state=CALIB_BALANCE;
										   //balance_update_main_status();
										   nrepeat_calibra=0;
										   nrepeat_tara=0;
										}else{
											balance.cmd=TARA_BAL_CMD;
											balance.len_RX=1;
											balance.timeout_RX=TIMEOUT1_BALANCE_RX_ACK_100ms;
											balance.state=START_CALIB_BALANCE;
											balance_send_cmd (balance.cmd,1);

										}
			                   }


		 			            break;
	  case(CALIB_BALANCE):

    	                    if (balance.timeout_RX==0){
    	                    	balance.weight_rd=1000;
    	                    	balance_update_weight_act(balance.weight_rd) ;
								revo_state=REVO_WEIGHT_POS_CAL_RUN;
								if(++nrepeat_calibra >3){
								   balance.state=IDLE_BALANCE;
								   revo_state=REVO_WEIGHT_POS_CAL_END;
								   balance.weight_rd=0;
								   balance_update_weight_act(balance.weight_rd) ;
								   nrepeat_calibra=0;
								}else{
									balance.cmd=CALIB_BAL_CMD;
									balance.state=CALIB_BALANCE;
									balance.len_RX=1;
									balance.timeout_RX=15;
									balance_send_cmd (balance.cmd,1);
								}
    			            }


	 			            break;
	  case(START_MEASURE_BALANCE):
		                   if (balance.timeout_RX==0){
								balance.weight_rd=0;
								balance_update_weight_act(balance.weight_rd) ;
								balance.timeout_Weight=TIMEOUT_WEIGHING;
								if(++nrepeat_tara >2){
								   balance.state=WAIT_END_MEAS_BALANCE;
								   //balance_update_main_status();
								   balance.timeout_RX=0;//TIMEOUT1_BALANCE_RX_ACK_100ms;
								   nrepeat_tara=0;
								}else{
									balance.cmd=TARA_BAL_CMD;
									balance.len_RX=1;
									balance.timeout_RX=TIMEOUT1_BALANCE_RX_ACK_100ms;
									balance_send_cmd (balance.cmd,1);
									balance.state=START_MEASURE_BALANCE;
								}


		                    }


	 			            break;

	  case(START_MEASURE_TIME):

	  									balance.weight_rd=0;
	                                    balance.timeout_Weight=balance.time_target;
	                                    balance_update_main_status();
	  									balance_update_weight_act(balance.weight_rd) ;

	  								    doser_vibro[balance.doser].DIS_pin=START_MOT;
	  									doser_vibro[balance.doser].DIR_pin=DIR_REVERSE;
	  									doser_vibro[balance.doser].pwm_duty =DUTY_DOSER_TIME_WEIGHING ;
	  								    doser_vibro[balance.doser].cmd.b.DIS =1;//new DIS for DOSER
	  								  	doser_vibro[balance.doser].cmd.b.DUTY=1;//new DUTY for DOSER
	  						        	doser_vibro[balance.doser].cmd.b.DIR =1;

	  									balance.state=WAIT_END_MEAS_TIME;


	  	 			            break;
	  case(WAIT_END_MEAS_TIME):

								  if (balance.timeout_Weight>0){
										 balance_update_weight_act(balance.weight_rd) ;
								  }else{//timeout expired
									    //balance.time_target=0;
									    balance.timeout_Weight=0;
									    balance_update_weight_act(balance.weight_rd) ;

									    balance_end_weighing_status();//balance_update_main_status();
									    doser_vibro[balance.doser].DIS_pin=STOP_MOT;
										doser_vibro[balance.doser].pwm_duty  =0;
										doser_vibro[balance.doser].cmd.b.DIS =1;//new DIS for DOSER
										doser_vibro[balance.doser].cmd.b.DUTY=1;//new DUTY for DOSER
										balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
								  }
		 	  	 			    break;

	  case(STOP_BALANCE):
		                  if(++nrepeat_tara >1){
							 balance_update_main_status();
							 balance.cmd=STOP_BAL_CMD;
							 balance.state=IDLE_BALANCE;
							 peso_gr_dec=0;
							 balance.len_RX=0;
							 balance_send_cmd (balance.cmd,0);
						   }

	 			            break;
	  case(ERR_BALANCE):

	 			            break;
	  case(RESEND_CMD_BALANCE):
			                if (balance.cmd!=WEIGHT_BAL_CMD){
			                	balance.state=WAIT_ACK_BALANCE;
			                	balance.len_RX=1;
								balance_send_cmd (balance.cmd,1);
			                }else{
			                	balance.len_RX=6;
			                	balance.state=WAIT_END_MEAS_BALANCE;
			                	balance_send_cmd(balance.cmd,6);
			                }
	 			            break;
	  case(WAIT_ACK_BALANCE):

		                    //balance.state=IDLE_BALANCE;//WAIT_ACK_BALANCE;
	 			            break;
	  case(WAIT_END_MEAS_BALANCE)://Start weighing measure 'M' after

								   if (balance.timeout_RX==0){
									if(++nrepeat_tara >1){
										balance.cmd=WEIGHT_BAL_CMD;
										balance.len_RX=6;
										balance.timeout_RX=TIMEOUT1_BALANCE_RX_WEIGHT_100ms;
										balance.state=WEIGHING_BALANCE;
										balance_send_cmd (balance.cmd,6);
										nrepeat_tara=0;
									}else{
										balance.cmd=WEIGHT_BAL_CMD;
										balance.state=WAIT_END_MEAS_BALANCE;
										balance.len_RX=1;
										balance.timeout_RX=TIMEOUT1_BALANCE_RX_ACK_100ms;
										balance_send_cmd (balance.cmd,1);
									}
								   }

	  	 			        break;
	  case(TARA_BALANCE):
		                    balance.cmd=TARA_BAL_CMD;
	                        balance.state=WAIT_ACK_BALANCE;//CALIB_BALANCE;//
	                        balance.len_RX=1;
				            balance_send_cmd (balance.cmd,1);
	  	 			        break;
	  case(END_TARA_WEIGHT_BALANCE)://wait ACK after TARA command 'T' at start of weighing cycle

	 	  	 			    break;
	  case(WEIGHING_BALANCE):

							  if (balance.timeout_RX==0){
								  balance.len_RX=6;
								  balance_RX_IT();
								  balance.state=WEIGHING_BALANCE;
							  }
							  balance.len_RX=6;
							  if (balance.timeout_Weight>0){
								 if (balance_weighing_procedure(balance.doser,TOLERANCE_WEIGHT_BALANCE,balance.weight_target)==TRUE){

									 balance_end_weighing_status();//balance_update_main_status();

									 balance.cmd=STOP_BAL_CMD;
									 balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
									 peso_gr_dec=0;
									 balance.len_RX=0;
									 nrepeat_tara=0;
									 balance_send_cmd(balance.cmd,0);
								 }
							  }else{//timeout expired
								    balance.weight_rd=9999;
								    balance.new_weight=TRUE;
								    balance_update_weight_act(balance.weight_rd) ;
								    balance_weighing_procedure(balance.doser,TOLERANCE_WEIGHT_BALANCE,balance.weight_target);
								    balance_end_weighing_status();//balance_update_main_status();
								     balance.cmd=STOP_BAL_CMD;
									 balance.state=IDLE_BALANCE;//balance.state=STOP_BALANCE;
									 peso_gr_dec=0;
									 balance.len_RX=0;
									 nrepeat_tara=0;
									 balance_send_cmd(balance.cmd,0);
									 error_set(ERROR_LOADCELL,0,10);
							  }
	 	  	 			    break;

	  default :
		        break;
	}

}

#endif


