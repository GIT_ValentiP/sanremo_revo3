/*
 ******************************************************************************
 *  @file      : fan.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "fan.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define FAN_TIMER_PWM_CLOCK_FREQ  100000  //freq timer clock in Hz
#define FAN_TIMER_PWM_MAX_FREQ     30000//
/* Private macro -------------------------------------------------------------*/
const uint8_t FAN_SPEED_RPM_DUTY_STEP10[11]={0,25,35,45,55,65,75,85,93,102,104};
/* Private variables ---------------------------------------------------------*/

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

/* Private function prototypes -----------------------------------------------*/
void fan_temp_duty_regulation(void);
/* Private user code ---------------------------------------------------------*/



void set_new_freq_fan(uint8_t id_fan,uint16_t nfreq_hz ){

	uint32_t freq_set;
    //ARR = (TIMx counter clock / TIMx output clock) - 1  = 665
    if (nfreq_hz>FAN_TIMER_PWM_MAX_FREQ){
    	freq_set=FAN_TIMER_PWM_CLOCK_FREQ/FAN_TIMER_PWM_MAX_FREQ;
    }else{
    	freq_set=FAN_TIMER_PWM_CLOCK_FREQ/nfreq_hz;

    }
    //printf("FAN_%1d  pwm freq:%d Hz \n\r",id_fan,nfreq_hz);
	switch (id_fan){
			case(FAN_1)://TIM3 channel 2
					          htim1.Instance->ARR=freq_set-1;
			                  htim1.Init.Period  =freq_set-1;
						      break;
			case(FAN_2)://TIM3 channel 3
		                      htim1.Instance->ARR=freq_set-1;
			                  htim1.Init.Period  =freq_set-1;
							  break;

			default:
					          break;

	}





}



void set_new_duty_fan(uint8_t id_fan,uint16_t nduty_x100){

	 uint32_t duty_cycle=0;
	 uint32_t period_pwm;
	 uint16_t step_rpm=0;
		if (nduty_x100 <101){
		   duty_cycle=100-nduty_x100;
		   if (nduty_x100>9){
			  step_rpm=(nduty_x100/10);
		   }

		}else{
			step_rpm=10;
		}
        fan[id_fan].speed_rpm= FAN_SPEED_RPM_DUTY_STEP10[step_rpm]*60;
		switch (id_fan){
					case(FAN_1):
			                          period_pwm = (htim1.Init.Period+1);
					                  duty_cycle =((duty_cycle*period_pwm)/100);
							          htim1.Instance->CCR1=(duty_cycle);
								      break;
					case(FAN_2):
			                          period_pwm = (htim1.Init.Period+1);
					                  duty_cycle =((duty_cycle*period_pwm)/100);
			                          htim1.Instance->CCR2=(duty_cycle);
									  break;

					default:
							          break;

			}

		//printf("FAN_%1d  period:%d ;pwm duty:%d \n\r",id_fan,(uint16_t)period_pwm ,(uint16_t)duty_cycle);
}


void power_on_off_fan(uint8_t id_fan,uint8_t power ){
	// printf("FAN_%1d  STOP:%2d \n\r",(id_fan+1),power);
	switch (id_fan){
				case(FAN_1):
						          if (power == GPIO_PIN_RESET){
						        	 HAL_TIM_PWM_Stop(&htim1,fan[id_fan].timer_channel);

						          }else{
						        	 HAL_TIM_PWM_Start(&htim1,fan[id_fan].timer_channel);

						          }
		                          HAL_GPIO_WritePin(POWER_FAN1_GPIO_Port, POWER_FAN1_Pin, power);

								  break;
				case(FAN_2):
		                          if (power == GPIO_PIN_RESET ){
										HAL_TIM_PWM_Stop(&htim1,fan[id_fan].timer_channel);
								  }else{
										HAL_TIM_PWM_Start(&htim1,fan[id_fan].timer_channel);
								  }
                                  HAL_GPIO_WritePin(POWER_FAN2_GPIO_Port, POWER_FAN2_Pin, power);

				                  break;

				default:
						          break;

		}

}

void fan_temp_duty_regulation(void){
	uint16_t new_duty[2];
if (fan[FAN_1].cooling_set.fan_b.AUTO_OFF==1){
		if(temp_sens[NTC_TEMP_SENS].temperature > fan[FAN_1].temp[3]){
			new_duty[FAN_1]=60;
			new_duty[FAN_2]=80;
		}else if(temp_sens[NTC_TEMP_SENS].temperature > fan[FAN_1].temp[2]){
				new_duty[FAN_1]=50;
				new_duty[FAN_2]=70;
			  }else if(temp_sens[NTC_TEMP_SENS].temperature > fan[FAN_1].temp[1]){
					 new_duty[FAN_1]=40;
					 new_duty[FAN_2]=60;
				   }else if(temp_sens[NTC_TEMP_SENS].temperature > fan[FAN_1].temp[0]){
							new_duty[FAN_1]=30;
							new_duty[FAN_2]=50;
						 }else{
								new_duty[FAN_1]=FAN1_DUTY_STANDBY;
								new_duty[FAN_2]=FAN2_DUTY_STANDBY;
							  }
		if(new_duty[FAN_1] != fan[FAN_1].pwm_duty[0]){
			fan[FAN_1].pwm_duty[0]=new_duty[FAN_1];
			fan[FAN_1].cmd.b.DUTY=1;
			fan[FAN_1].POWER_pin = GPIO_PIN_SET;
			fan[FAN_1].cmd.b.DIS=1;
		}
		if(new_duty[FAN_2] != fan[FAN_2].pwm_duty[0]){
			fan[FAN_2].pwm_duty[0]=new_duty[FAN_2];
			fan[FAN_2].cmd.b.DUTY=1;
			fan[FAN_2].POWER_pin = GPIO_PIN_SET;
			fan[FAN_2].cmd.b.DIS=1;
		}

	}else if (fan[FAN_1].cooling_set.fan_b.MANUAL==1){
		       new_duty[FAN_1]=(fan[FAN_1].cooling_set.fan_b.DUTY_FAN_MAN >> 8);
		       if(new_duty[FAN_1] != fan[FAN_1].pwm_duty[0]){
					fan[FAN_1].pwm_duty[0]=new_duty[FAN_1];
					fan[FAN_2].pwm_duty[0]=new_duty[FAN_2];
					fan[FAN_1].cmd.b.DUTY=1;
					fan[FAN_2].cmd.b.DUTY=1;
					fan[FAN_1].POWER_pin = GPIO_PIN_SET;
					fan[FAN_2].POWER_pin = GPIO_PIN_SET;
					fan[FAN_1].cmd.b.DIS=1;
					fan[FAN_2].cmd.b.DIS=1;
				}
	     }else {

//	    	 if(fan[FAN_1].POWER_pin != GPIO_PIN_RESET){
//					fan[FAN_1].POWER_pin = GPIO_PIN_RESET;
//					fan[FAN_2].POWER_pin = GPIO_PIN_RESET;
//					fan[FAN_1].cmd.b.DIS=1;
//					fan[FAN_2].cmd.b.DIS=1;
//	    	 }
	 	}
}

void  fan_manager(){
	uint8_t id_mot=0;

	fan_temp_duty_regulation();

	for(id_mot=0;id_mot<2;id_mot++){
		if (fan[id_mot].cmd.b.FREQ!=0){//new command for FAN
			set_new_freq_fan(id_mot,fan[id_mot].pwm_freq );
			fan[id_mot].cmd.b.FREQ=0;
		}
		if (fan[id_mot].cmd.b.DUTY!=0){//new command for FAN
			set_new_duty_fan(id_mot,fan[id_mot].pwm_duty[0] );
			fan[id_mot].cmd.b.DUTY=0;
		}
		if (fan[id_mot].cmd.b.DIS!=0){//new command for FAN
			power_on_off_fan(id_mot,fan[id_mot].POWER_pin );
			fan[id_mot].cmd.b.DIS=0;
		}

	}



}

void init_fan(void){
uint8_t id;

    fan[FAN_1].timer_channel=TIM_CHANNEL_1;
	fan[FAN_2].timer_channel=TIM_CHANNEL_2;
	fan[FAN_1].temp[3]=30;
	fan[FAN_1].temp[2]=28;
	fan[FAN_1].temp[1]=27;
	fan[FAN_1].temp[0]=19;
	fan[FAN_1].cooling_set.fan_b.AUTO_OFF=1;
	for(id=0;id <2;id++){
		fan[id].POWER_pin=GPIO_PIN_SET;//GPIO_PIN_RESET;
		if (id==0){
		   fan[id].pwm_duty[0] = FAN1_DUTY_STANDBY;//1KHz
		}else{
			fan[id].pwm_duty[0] = FAN2_DUTY_STANDBY;//1KHz
		}
		fan[id].speed_rpm=0;
		fan[id].pwm_freq=1000;
			set_new_freq_fan(id,fan[id].pwm_freq );
	        set_new_duty_fan(id,fan[id].pwm_duty[0]);
	        power_on_off_fan(id,fan[id].POWER_pin);
	}
}

