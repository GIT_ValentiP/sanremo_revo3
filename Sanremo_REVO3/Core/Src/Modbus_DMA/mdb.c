/*
 * mdb.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */


/*=== Include files ==================================================================================================*/

#include "globals.h"
#include "mdb_registers.h"
#include "mdb.h"

/*=== Constant definitions (File scope) ==============================================================================*/

#define MDB_FN_READCOILSTATUS		0x01
#define MDB_FN_READDISCRETEINPUT	0x02
#define MDB_FN_READHOLDINGREG		0x03
#define MDB_FN_READINPUTREG			0x04
#define MDB_FN_FORCESINGLECOIL		0x05
#define MDB_FN_PRESETSINGLEREG		0x06
#define MDB_FN_READEXCEPTION		0x07
#define MDB_FN_FORCEMULTIPLECOILS 	0x0F
#define MDB_FN_FORCEMULTIPLEREGS	0x10

#define MDB_ERROR_SIZE	((uint32_t)2)
#define MDB_MULTIPLE_REG_HEADER_SIZE ((uint32_t)5)

#define MDB_MINIMUM_REPLY_SIZE	((uint32_t)2)


#define MDB_NUM_BYTES_ADDR ((uint32_t)1)
#define MDB_NUM_BYTES_CRC ((uint32_t)2)
#define MDB_SER_OVERHEAD	(MDB_NUM_BYTES_ADDR + MDB_NUM_BYTES_CRC)

#define MDB_INVALID_MDB_ADDRESS ((uint8_t)255)


/*=== Macro definitions (File scope) =================================================================================*/

#define READ_UNS16_BE(buffer,offset) ( (uint16_t)((uint8_t*)buffer)[offset]<<8|((uint8_t*)buffer)[(offset)+1] )
#define WRITE_UNS16_BE(buffer,offset,value) ((uint8_t*)buffer)[offset]=(uint8_t)((value)>>8), ((uint8_t*)buffer)[(offset)+1]=(uint8_t)((value)&0xFF)


/*=== Type definitions (File scope) ==================================================================================*/

//Internal parameter passing structure
typedef struct {
	uint8_t* rcv_data;
	uint32_t rcv_size;
	uint8_t* reply_data;
	uint32_t reply_max_size;
} ModbusMessage;


typedef enum {
	DISCRETE_INPUT,
	COIL,
	INPUT_REGISTER,
	HOLDING_REGISTER
} REGISTER_TYPE;

/*=== Constant definitions (File scope) ==============================================================================*/


/*=== Data definitions (File scope) ==================================================================================*/

static uint8_t mdb_slave_addr ;//= 0;


/*=== Function declarations (File scope) =============================================================================*/

static uint32_t reply_error(ModbusMessage*, uint8_t error);
static uint32_t reply_echo(ModbusMessage*, uint32_t size);

static uint32_t read_bits(REGISTER_TYPE type, ModbusMessage *msg);
static uint32_t read_registers(REGISTER_TYPE type, ModbusMessage *msg);
static uint32_t force_single_coil(ModbusMessage*);
static uint32_t preset_single_register(ModbusMessage*);
static uint32_t force_multiple_coils(ModbusMessage*);
static uint32_t force_multiple_registers(ModbusMessage*);

static uint16_t mdb_crc16(uint8_t *block, uint32_t size);
/*=== Global Function definitions ========================================================================*/

uint32_t mdb_receive_message(uint8_t* rcv_data, uint32_t rcv_len, uint8_t* reply_data, uint32_t reply_max_size)
{
	//structure to avoid passing 4 parameters
	ModbusMessage msg = {rcv_data, rcv_len, reply_data, reply_max_size};

	if ( rcv_len > 1 && reply_max_size >= MDB_MINIMUM_REPLY_SIZE )
		switch(rcv_data[0]) {
		case MDB_FN_READCOILSTATUS:
			return read_bits(COIL, &msg);
		case MDB_FN_READDISCRETEINPUT:
			return read_bits(DISCRETE_INPUT, &msg);
		case MDB_FN_READHOLDINGREG:
			return read_registers(HOLDING_REGISTER, &msg);
		case MDB_FN_READINPUTREG:
			return read_registers(INPUT_REGISTER, &msg);
		case MDB_FN_FORCESINGLECOIL:
			return force_single_coil(&msg);
		case MDB_FN_PRESETSINGLEREG:
			return preset_single_register(&msg);
		case MDB_FN_READEXCEPTION:
			break;
		case MDB_FN_FORCEMULTIPLECOILS:
			return force_multiple_coils(&msg);
		case MDB_FN_FORCEMULTIPLEREGS:
			return force_multiple_registers(&msg);
		default:
			break;
		}

	return reply_error(&msg, MDB_ERR_ILLEGAL_FUNCTION);
}


/*=== Local Function definitions (File scope) ========================================================================*/

static uint32_t reply_error(ModbusMessage* msg, uint8_t error)
{
	if (msg->reply_max_size >= MDB_ERROR_SIZE) {
		msg->reply_data[0] = 0x80 | msg->rcv_data[0];
		msg->reply_data[1] = error;
		return MDB_ERROR_SIZE;
	} else {
		return 0;
	}
}


static uint32_t reply_echo(ModbusMessage* msg, uint32_t size)
{
	int32_t i;
	if ( msg->reply_max_size >= size ) {
		for (i=0; i<size; i++) msg->reply_data[i] = msg->rcv_data[i];
		return size;
	} else {
		return 0;
	}
}


static uint32_t read_bits(REGISTER_TYPE type, ModbusMessage *msg)
{
	int32_t i;
	uint8_t mask;
	uint16_t index, num;
	int32_t value;

	if (msg->rcv_size < 5) return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);


	index = READ_UNS16_BE(msg->rcv_data , 1);
	num = READ_UNS16_BE(msg->rcv_data, 3);

	msg->reply_data[0] = msg->rcv_data[0];
	msg->reply_data[1] = (uint8_t)((num+7)>>3); //numero byte risposta = 1 byte ogni 8 coil richiesti

	//assicurati di non sforare la massima lunghezza della risposta
	if ( msg->reply_data[1] > msg->reply_max_size - 2 ) {
		return reply_error( msg, MDB_ERR_ILLEGAL_FUNCTION );
	}

	// Leggi tutti i bit e impacchettali nei bytes della risposta
	i = 2; //offset della prima word nella risposta
	while ( num>0 ) {
		msg->reply_data[i] = 0; //azzera byte di risposta
		mask = 1; //maschera per impostare i bit. Bit 0=1
		while (mask>0 && num>0) {
			if (type==DISCRETE_INPUT) value = mdb_read_status(index++);
			else value = mdb_read_coil(index++);

			if (value<0) {
				return reply_error( msg, (uint8_t) -value );
			} else if (value>0) {
				msg->reply_data[i] |= mask;
			}

			mask<<=1;
			num--;
		}
		i++;
	}
	return (uint32_t) msg->reply_data[1] + 2;
}


static uint32_t read_registers(REGISTER_TYPE type, ModbusMessage *msg)
{
	int32_t i;
	int32_t value;
	uint16_t index, num;

	if (msg->rcv_size < 5){
		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
	}

	index = READ_UNS16_BE(msg->rcv_data,1);
	num = READ_UNS16_BE(msg->rcv_data,3);

	msg->reply_data[0] = msg->rcv_data[0];
	msg->reply_data[1] = (uint8_t)(num*2); //numero byte risposta = 2 byte per word richiesta

	// controlla che non stiamo chiedendo di creare un messaggio di risposta troppo lungo
	if ( msg->reply_data[1] > msg->reply_max_size - 2 ) {
		return reply_error( msg, MDB_ERR_ILLEGAL_FUNCTION );
	}

	i=2; //offset della prima word nella risposta
	while ( num-- > 0 ) {
		if (type==INPUT_REGISTER) value = mdb_read_input_register(index);
		else value = mdb_read_holding_register(index);

		if (value<0) {
			return reply_error( msg, (uint8_t) -value );

		} else {
			WRITE_UNS16_BE(msg->reply_data, i, (uint16_t) value );
		}
		i+=2;
		index++;
	}

	return msg->reply_data[1] + 2;
}


static uint32_t force_single_coil(ModbusMessage *msg)
{
	if (msg->rcv_size < 5) return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);

	int index = READ_UNS16_BE(msg->rcv_data,1);
	int value = READ_UNS16_BE(msg->rcv_data,3);

	int32_t error = mdb_write_coil(index, value);

	if (error) {
		return reply_error(msg, (uint8_t) -error);
	}
	else{
		return reply_echo(msg, msg->rcv_size);
	}
}


static uint32_t preset_single_register(ModbusMessage *msg)
{
	if (msg->rcv_size < 5) {
		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
	}

	uint16_t index = READ_UNS16_BE(msg->rcv_data,1);
	uint16_t value = READ_UNS16_BE(msg->rcv_data,3);

	int32_t error = mdb_write_holding_register(index, value);

	if (error) return reply_error(msg, (uint8_t) -error);
	else return reply_echo(msg, msg->rcv_size);
}


static uint32_t force_multiple_coils(ModbusMessage *msg)
{
	if (msg->rcv_size < MDB_MULTIPLE_REG_HEADER_SIZE) {
		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
	}

	uint16_t index = READ_UNS16_BE(msg->rcv_data,1); // data start
	uint16_t num_words = READ_UNS16_BE(msg->rcv_data,3);
	uint8_t num_bytes = msg->rcv_data[5];

	int i=6; //offset primo gruppo di 8 coil nella request
	int b;
	int32_t error;

	if ( num_bytes != (num_words+7)/8 || (num_bytes + MDB_MULTIPLE_REG_HEADER_SIZE) > msg->rcv_size )  {
		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
	}

	while (num_words>0) {
		b=msg->rcv_data[i++] | BIT_8; //leggi prossimo gruppo di 8 bit ed aggiungi marcatore
		while ( (num_words)>0 && b != BIT_0) {
			error= mdb_write_coil(index++, b&1);
			if (error) return reply_error(msg, (uint8_t) -error);
			b>>=1;
			num_words--;
		}
	}

	return reply_echo(msg, MDB_MULTIPLE_REG_HEADER_SIZE);
}


static uint32_t force_multiple_registers(ModbusMessage *msg)
{
	if (msg->rcv_size < MDB_MULTIPLE_REG_HEADER_SIZE) return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);

	uint16_t index = READ_UNS16_BE(msg->rcv_data,1); // data start
	uint16_t num_words = READ_UNS16_BE(msg->rcv_data,3);
	uint8_t num_bytes = msg->rcv_data[5];
	int i=6;
	int32_t error;

	if ( (num_bytes&1) != 0 || num_bytes != num_words*2 || (num_bytes + MDB_MULTIPLE_REG_HEADER_SIZE) > msg->rcv_size ) {
		return reply_error(msg, MDB_ERR_ILLEGAL_FUNCTION);
	}

	while ( (num_words--) > 0 ) {
		uint16_t value = READ_UNS16_BE(msg->rcv_data,i);
		error = mdb_write_holding_register(index++, value);
		if (error) return reply_error(msg, (uint8_t) -error);
		i+=2;
	}

	return reply_echo(msg, MDB_MULTIPLE_REG_HEADER_SIZE);
}





/*=== Global function definitions (Global system or Local sub-system scope) ============================================================*/

void mdb_serial_setSlaveAddress(uint8_t slave_address)
{

	mdb_slave_addr = slave_address;

}


uint32_t mdb_serial_receiveMessage(uint8_t* rcv_data, uint32_t rcv_len, uint8_t* txBuffer, uint32_t txBufferLen)
{


	uint16_t crc;
	uint8_t addr = MDB_INVALID_MDB_ADDRESS;
	uint32_t reply_len = (uint32_t) 0;

	// exclude messages too short or too long  TODO verify if txBufferLen is a good boundary
	if ( rcv_len > MDB_SER_OVERHEAD && rcv_len < txBufferLen ) {
		addr = rcv_data[0];
	}

	if ( addr == MDB_BROADCAST_ADDR || addr == mdb_slave_addr ) {
		// check CRC
		crc = mdb_crc16(rcv_data, rcv_len - MDB_NUM_BYTES_CRC);
		if ( (crc&0xFF) == rcv_data[rcv_len - MDB_NUM_BYTES_CRC] &&
			 (crc>>8) == rcv_data[rcv_len-MDB_NUM_BYTES_CRC+1] )
		{
			// crc OK, decode payload message
			reply_len = mdb_receive_message(rcv_data + MDB_NUM_BYTES_ADDR,
					rcv_len - MDB_SER_OVERHEAD,
					txBuffer + MDB_NUM_BYTES_ADDR,
					txBufferLen - MDB_SER_OVERHEAD);

			//prepare serial reply prepending slave number and appending crc
			if (addr != MDB_BROADCAST_ADDR && reply_len > 0 ) {
				//head slave number
				txBuffer[0] = mdb_slave_addr;

				//and tail crc
				crc = mdb_crc16(txBuffer, reply_len + MDB_NUM_BYTES_ADDR);
				txBuffer[reply_len + MDB_NUM_BYTES_ADDR] = (uint8_t)(crc&0xFF);
				txBuffer[reply_len + MDB_NUM_BYTES_ADDR + 1 ] = (uint8_t)(crc>>8);

				reply_len += MDB_SER_OVERHEAD;
			} else {
				//do not reply
				reply_len = (uint32_t)0;
			}
		}
	}


	return reply_len;
}

/*=== Local Function definitions (File scope) ========================================================================*/

//TODO faster crc16 calculation
static uint16_t mdb_crc16(uint8_t *block, uint32_t size)
{


	int i,j;
	uint16_t crc=0xffff;

	for (i=0; i<size; i++) {
		crc^= (uint16_t)block[i];
		for (j=0; j<8; j++) {
			if ((crc & 0x01)==0)
				crc>>=1;
			else
				crc=(crc>>1)^0xA001;
		}
	}



	return crc;
}

