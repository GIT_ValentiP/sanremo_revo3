/*
 * mdb_registers.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */


/*=== Include files ====================================================================================================================*/


#include "mdb_registers.h"
#include "globals.h"

/*=== Global function definitions (Local sub-system scope) ============================================================*/

int32_t mdb_read_status(uint16_t index)
{
	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_coil(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_write_coil(uint16_t index, uint16_t value)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;

}

int32_t mdb_read_input_register(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_holding_register(uint16_t index)
{
uint16_t value_rd;
//	int16_t result_rd=0;
	switch ( index ) {
	   case MDB_FW_REL_REG      :
				return (uint16_t) version_fw[0];
	   case MDB_FW_INV_REG      :
	  		    return (uint16_t) version_fw[1];
	   case MDB_FW_LOADCELL_REG      :
	  		    return (uint16_t) version_fw[2];



/*REVO : REVO3 or REVO1 configuration  */
	   case MDB_BOARD_CONFIG_REG:
            return  ((uint16_t) revo_board_conf) ;




/*REVO CONFIGURATION MODE*/
      case MDB_OPMODE_REG:
           return  ((uint16_t) revo_op_mode.mode_op_reg);

/*REVO OPERATIONAL COMMAND*/
	   case MDB_OPMODE_CMD_REG:
            return ((uint16_t) revo_op_cmd.cmd_reg16);

/*REVO CALIBRATION COMMAND*/
	   case MDB_CAL_CMD_REG :
	  			return ((uint16_t) revo_calibration_cmd.cal16);

/*REVO MAIN STATUS */
	   case MDB_BOARD_STATUS_REG :
			return ((uint16_t) revo_state);

/*HALL SENSOR and PORTA FILTER*/
	   case MDB_POS_SENSOR_REG :
		    return ((uint16_t) pos_sensor.reg_w);

/*HOPPER WEIGHT ACTUAL*/
		case MDB_HOP1_WEIGHT_REG:
			 return ((uint16_t) hopper_burr[HOPPER_1].hopper_weight_rd);

		case MDB_HOP2_WEIGHT_REG:
			 return ((uint16_t) hopper_burr[HOPPER_2].hopper_weight_rd);

		case MDB_HOP3_WEIGHT_REG:
			 return ((uint16_t) hopper_burr[HOPPER_3].hopper_weight_rd);

 /*HOPPER TIME ACTUAL*/
		case MDB_HOP1_TIME_REG:
			 return ((uint16_t) hopper_burr[HOPPER_1].hopper_time_rd);

		case MDB_HOP2_TIME_REG:
			 return ((uint16_t) hopper_burr[HOPPER_2].hopper_time_rd);

		case MDB_HOP3_TIME_REG:
			 return ((uint16_t) hopper_burr[HOPPER_3].hopper_time_rd);

 /*TEMPERATURE & HUMIDITY SENSORS*/
		case MDB_EXT_TEMPERATURE_REG:
			if(temp_sens[EXT_TEMP_SENS].temperature>0){
		       return ((uint16_t) temp_sens[EXT_TEMP_SENS].temperature);
			}else{
			   return (0);
			}

		case MDB_EXT_HUMIDITY_REG:
		return ((uint16_t) temp_sens[EXT_TEMP_SENS].humidity);

		case MDB_INT_TEMPERATURE_REG:
			if(temp_sens[INT_TEMP_SENS].temperature>0){
				return ((uint16_t) temp_sens[INT_TEMP_SENS].temperature);
			}else{
			   return (0);
			}


		case MDB_INT_HUMIDITY_REG:
		return ((uint16_t) temp_sens[INT_TEMP_SENS].humidity);


		case MDB_NTC_GRIND_TEMP_REG:
			if( temp_sens[NTC_TEMP_SENS].temperature>0){
		    		return ((uint16_t) temp_sens[NTC_TEMP_SENS].temperature);
			}else{
				   return (0);
				}

/*ENCODER POSITION BURR*/
		case MDB_BURR_ENC_STEP_REG:
			 return ((uint16_t) hopper_burr[BURR_0].burr_distance_rd);

 /*FAN SPEED RPM*/
		case MDB_FAN1_RPM_REG:
			return ((uint16_t) fan[FAN_1].speed_rpm);

		case MDB_FAN2_RPM_REG:
			return ((uint16_t) fan[FAN_2].speed_rpm);

 /*HOPPER Time SET*/
	 case MDB_SET_HOP1_TIME_REG:
				return (uint16_t) hopper_burr[HOPPER_1].hopper_time_target;

	  case MDB_SET_HOP2_TIME_REG:
				return ((uint16_t)	hopper_burr[HOPPER_2].hopper_time_target);

	  case MDB_SET_HOP3_TIME_REG:
			    return ((uint16_t)	hopper_burr[HOPPER_3].hopper_time_target);

/*HOPPER SPEED*/
   case MDB_SET_HOP1_PWM_REG:

				return ((uint16_t)hopper_burr[HOPPER_1].hopper_speed_target);

   case MDB_SET_HOP2_PWM_REG:

				return ((uint16_t)hopper_burr[HOPPER_2].hopper_speed_target);

   case MDB_SET_HOP3_PWM_REG:

				return ((uint16_t)hopper_burr[HOPPER_3].hopper_speed_target);
/*HOPPER OFFSET Weight*/
   case MDB_OFSET_HOP1_REG:

				return ((uint16_t)hopper_burr[HOPPER_1].hopper_weight_offset);

   case MDB_OFSET_HOP2_REG:

				return ((uint16_t)hopper_burr[HOPPER_2].hopper_weight_offset);

  case MDB_OFSET_HOP3_REG:

			return ((uint16_t)hopper_burr[HOPPER_3].hopper_weight_offset);
/*GRINDING Burr distance */

	 case MDB_SET_GRIND_SIZE_REG:
		  return ((uint16_t)  grinder.grinding_size);

	 case MDB_SET_GRIND_SPEED_REG:
		  return ((uint16_t)  grinder.speed_rpm);

	 case MDB_SET_GRIND_TIME_REG:
		 return ((uint16_t)  grinder.time_grinding);

	 case MDB_SET_GRIND_POWER_REG:
			 return ((uint16_t)  grinder.input_reg[INV_PWR]);

/*VIBRO */
	 case MDB_SET_VIBRO_REG:
		 value_rd=(doser_vibro[VIBRO_0].pwm_duty<<8)+(doser_vibro[VIBRO_0].delay_over_stop/1000);
				// doser_vibro[VIBRO_0].delay_over_stop=((value & 0x00FF)*1000);
				// doser_vibro[VIBRO_0].pwm_duty=value>>8;
		   return ((uint16_t)value_rd);

/* FAN Speed*/

 case MDB_SET_FAN_SPEED_REG:
			return fan[FAN_1].cooling_set.fan_w;
 case MDB_SET_TEMP_THRES1_REG:
	        value_rd=((fan[FAN_1].temp[1]<<8)+fan[FAN_1].temp[0]);
			return value_rd;
 case MDB_SET_TEMP_THRES2_REG:
	        value_rd=((fan[FAN_1].temp[3]<<8)+fan[FAN_1].temp[2]);
			return value_rd;


/*DOSER and VIBRO PWM frequency*/
	    case MDB_SET_DOS1_FREQ_REG:
			  return ((uint16_t) doser_vibro[DOSER_1].pwm_freq);

		case MDB_SET_DOS2_FREQ_REG:
			  return ((uint16_t) doser_vibro[DOSER_2].pwm_freq);

		case MDB_SET_DOS3_FREQ_REG:
			  return ((uint16_t) doser_vibro[DOSER_3].pwm_freq);

		case MDB_SET_VIBRO_FREQ_REG:
			  return ((uint16_t) doser_vibro[VIBRO_0].pwm_freq);

/*DOSER and VIBRO PWM duty cycle*/
		case MDB_SET_DOS1_DUTY_REG:
			  return ((uint16_t) doser_vibro[DOSER_1].pwm_duty);
		case MDB_SET_DOS2_DUTY_REG:
			  return ((uint16_t) doser_vibro[DOSER_2].pwm_duty);
		case MDB_SET_DOS3_DUTY_REG:
			  return ((uint16_t) doser_vibro[DOSER_3].pwm_duty);
		case MDB_SET_VIBRO_DUTY_REG:
			  return ((uint16_t) doser_vibro[VIBRO_0].pwm_duty);

/*DOSER and VIBRO Direction*/
		case MDB_SET_DIR_DOS1_REG:
			  return ((uint16_t) doser_vibro[DOSER_1].DIR_pin);
		case MDB_SET_DIR_DOS2_REG:
			 return ((uint16_t) doser_vibro[DOSER_2].DIR_pin);
		case MDB_SET_DIR_DOS3_REG:
			 return ((uint16_t) doser_vibro[DOSER_3].DIR_pin);
		case MDB_SET_DIR_VIBRO_REG:
			 return ((uint16_t) doser_vibro[VIBRO_0].DIR_pin);

/*DOSER and VIBRO DISable*/
		case MDB_SET_STOP_DOS1_REG:
			 return ((uint16_t) doser_vibro[DOSER_1].DIS_pin);

		case MDB_SET_STOP_DOS2_REG:
			 return ((uint16_t) doser_vibro[DOSER_2].DIS_pin);

		case MDB_SET_STOP_DOS3_REG:
			 return ((uint16_t) doser_vibro[DOSER_3].DIS_pin);

		case MDB_SET_STOP_VIBRO_REG:
			 return ((uint16_t) doser_vibro[VIBRO_0].DIS_pin);


/*FAN PWM frequency*/
		case MDB_SET_FAN1_FREQ_REG:
			  return ((uint16_t) fan[FAN_1].pwm_freq);

		case MDB_SET_FAN2_FREQ_REG:
			  return ((uint16_t) fan[FAN_2].pwm_freq);


/*FAN PWM duty cycle*/
		case MDB_SET_FAN1_DUTY_REG:
			 return ((uint16_t) fan[FAN_1].pwm_duty[0]);
		case MDB_SET_FAN2_DUTY_REG:
			 return ((uint16_t) fan[FAN_2].pwm_duty[0]);

/*FAN Power*/
		case MDB_SET_POWER_FAN1_REG:
			 return ((uint16_t) fan[FAN_1].POWER_pin);
		case MDB_SET_POWER_FAN2_REG:
			 return ((uint16_t) fan[FAN_2].POWER_pin);

 /*HOPPER BURR STEPPER Pos ACTUAL*/
			case MDB_SET_HOP1_POS_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_1].step_pos);

			case MDB_SET_HOP2_POS_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_2].step_pos);

			case MDB_SET_HOP3_POS_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_3].step_pos);

			case MDB_SET_BURR0_POS_ACT_REG:
				 return ((uint16_t) hopper_burr[BURR_0].step_pos);


/*HOPPER BURR STEPPER Pos TARGET*/
			case MDB_SET_HOP1_POS_REG:
				 return ((uint16_t) hopper_burr[HOPPER_1].step_from_home);

			case MDB_SET_HOP2_POS_REG:
				 return ((uint16_t) hopper_burr[HOPPER_2].step_from_home);

			case MDB_SET_HOP3_POS_REG:
				 return ((uint16_t) hopper_burr[HOPPER_3].step_from_home);

			case MDB_SET_BURR0_POS_REG:
				 return ((uint16_t) hopper_burr[BURR_0].step_from_home);


/*HOPPER BURR STEPPER Freq*/
			case MDB_SET_HOP1_FREQ_REG:
				 return ((uint16_t) hopper_burr[HOPPER_1].pwm_freq);

			case MDB_SET_HOP2_FREQ_REG:
				 return ((uint16_t) hopper_burr[HOPPER_2].pwm_freq);

			case MDB_SET_HOP3_FREQ_REG:
				 return ((uint16_t) hopper_burr[HOPPER_3].pwm_freq);

			case MDB_SET_BURR0_FREQ_REG:
				 return ((uint16_t) hopper_burr[BURR_0].pwm_freq);

 /*HOPPER BURR STEPPER DIR*/
			case MDB_SET_HOP1_DIR_REG:
				 return ((uint16_t) hopper_burr[HOPPER_1].DIR_pin);

			case MDB_SET_HOP2_DIR_REG:
				 return ((uint16_t) hopper_burr[HOPPER_2].DIR_pin);

			case MDB_SET_HOP3_DIR_REG:
				 return ((uint16_t) hopper_burr[HOPPER_3].DIR_pin);

			case MDB_SET_BURR0_DIR_REG:
				 return ((uint16_t) hopper_burr[BURR_0].DIR_pin);

 /*HOPPER BURR STEPPER SPEED [pps]*/
			case MDB_SET_HOP1_SPEED_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_1].speed_rpm);

			case MDB_SET_HOP2_SPEED_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_2].speed_rpm);

			case MDB_SET_HOP3_SPEED_ACT_REG:
				 return ((uint16_t) hopper_burr[HOPPER_3].speed_rpm);

			case MDB_SET_BURR0_SPEED_ACT_REG:
				 return ((uint16_t) hopper_burr[BURR_0].speed_rpm);

 /*ILLUMINA PWM duty cycle*/
		   case MDB_SET_ILLUMINA_DUTY_REG:
			  return ((uint16_t)illuminazione.pwm_duty);

/*DEFAULT*/

		default:
						 return -MDB_ERR_ILLEGAL_DATAADDRESS;

	}


}




int32_t mdb_write_holding_register(uint16_t index, uint16_t value)
{

	//uint16_t new_value;
	 new_data_rx=true;



//#define MDB_SET_STOP_DOS1_REG                              0x070D
//#define MDB_SET_STOP_DOS2_REG                              0x070E
//#define MDB_SET_STOP_DOS3_REG                              0x070F
//#define MDB_SET_STOP_VIBRO_REG                             0x0710
	switch ( index ) {

	   case MDB_BOARD_CONFIG_REG:
		    new_data_rx=false;

		    if (value <2){
                   revo_board_conf=value;
//				if (revo_state==REVO_IDLE){
//				    revo_state=REVO_READY_TO_USE;
//				 }
		    }
            return MDB_OK;
/*REVO CONFIGURATION MODE*/
       case MDB_OPMODE_REG:

    	   revo_op_mode.mode_op_reg=value;

            return MDB_OK;

/*REVO OPERATIONAL COMMAND*/
       case MDB_OPMODE_CMD_REG:
    	 // if(revo_state==REVO_READY_TO_USE){  //((revo_state == REVO_IDLE )||(revo_state==REVO_READY_TO_USE))
    	     revo_op_cmd.cmd_reg16=value;
             return MDB_OK;
    	 // }
    	 // return -MDB_ERR_ILLEGAL_DATAVALUE;

/*REVO CALIBRATION COMMAND*/
    	case MDB_CAL_CMD_REG :
            revo_calibration_cmd.cal16=value;
            return MDB_OK;


/*HOPPER Weight SET*/
	   case MDB_SET_HOP1_WEIGHT_REG:


	  		hopper_burr[HOPPER_1].hopper_weight_target=value;
		    //hopper_burr[HOPPER_1].hopper_weight_rd=0;
	        return MDB_OK;

	   case MDB_SET_HOP2_WEIGHT_REG:


		  		hopper_burr[HOPPER_2].hopper_weight_target=value;
			    //hopper_burr[HOPPER_2].hopper_weight_rd=0;
		        return MDB_OK;

	   case MDB_SET_HOP3_WEIGHT_REG:


		  		hopper_burr[HOPPER_3].hopper_weight_target=value;
			    //hopper_burr[HOPPER_3].hopper_weight_rd=0;
		        return MDB_OK;
 /*HOPPER Time SET*/
	   case MDB_SET_HOP1_TIME_REG:

		  		hopper_burr[HOPPER_1].hopper_time_target=value;
			    //hopper_burr[HOPPER_1].hopper_time_rd=0;
		        return MDB_OK;

	  case MDB_SET_HOP2_TIME_REG:

			  		hopper_burr[HOPPER_2].hopper_time_target=value;
				    //hopper_burr[HOPPER_2].hopper_time_rd=0;
			        return MDB_OK;

	  case MDB_SET_HOP3_TIME_REG:

			  		hopper_burr[HOPPER_3].hopper_time_target=value;
				    //hopper_burr[HOPPER_3].hopper_time_rd=0;
			        return MDB_OK;
/*HOPPER SPEED*/
	   case MDB_SET_HOP1_PWM_REG:

			  		hopper_burr[HOPPER_1].hopper_speed_target=value;
			        return MDB_OK;

	   case MDB_SET_HOP2_PWM_REG:

				  	hopper_burr[HOPPER_2].hopper_speed_target=value;
				    return MDB_OK;

	   case MDB_SET_HOP3_PWM_REG:

				    hopper_burr[HOPPER_3].hopper_speed_target=value;
				    return MDB_OK;
/*HOPPER OFFSET Weight*/
	   case MDB_OFSET_HOP1_REG:

					hopper_burr[HOPPER_1].hopper_weight_offset=value;
					return MDB_OK;

	   case MDB_OFSET_HOP2_REG:

					hopper_burr[HOPPER_2].hopper_weight_offset=value;
					return MDB_OK;

	  case MDB_OFSET_HOP3_REG:

				   hopper_burr[HOPPER_3].hopper_weight_offset=value;
				   return MDB_OK;

/*GRINDER Power*/

	  case MDB_SET_GRIND_POWER_REG:
	 		                grinder.power_target=value;
	 				        return MDB_OK;

/*GRINDING Burr distance */

	 case MDB_SET_GRIND_SIZE_REG:
		                grinder.grinding_size=value;
				        return MDB_OK;

	 case MDB_SET_GRIND_SPEED_REG:
					    grinder.speed_rpm=value;
				        return MDB_OK;

	 case MDB_SET_GRIND_TIME_REG:
					    grinder.time_grinding=value;
				        return MDB_OK;

/* VIBRO REGULATION*/
	 case MDB_SET_VIBRO_REG:
		 doser_vibro[VIBRO_0].delay_over_stop=((value & 0x00FF)*1000);
		 doser_vibro[VIBRO_0].pwm_duty=value>>8;
		 return MDB_OK;

/* FAN Speed*/

	 case MDB_SET_FAN_SPEED_REG:
	 					fan[FAN_1].cooling_set.fan_w=value;
	 					fan[FAN_2].cooling_set.fan_w=value;
	 					return MDB_OK;
	 case MDB_SET_TEMP_THRES1_REG:
	 	 					fan[FAN_1].temp[0]=(value & 0x00FF);
	 	 					fan[FAN_1].temp[1]=(value>>8);
	 	 					fan[FAN_2].temp[0]=fan[FAN_1].temp[0];
	 	 					fan[FAN_2].temp[1]=fan[FAN_1].temp[1];
	 	 					return MDB_OK;
	 case MDB_SET_TEMP_THRES2_REG:
						    fan[FAN_1].temp[2]=(value & 0x00FF);
							fan[FAN_1].temp[3]=(value>>8);
							fan[FAN_2].temp[2]=fan[FAN_1].temp[2];
							fan[FAN_2].temp[3]=fan[FAN_1].temp[3];
	 	 					return MDB_OK;

/*DOSER and VIBRO PWM frequency*/
	    case MDB_SET_DOS1_FREQ_REG:
			        doser_vibro[DOSER_1].pwm_freq=value;
			        doser_vibro[DOSER_1].cmd.b.FREQ=1;
			        return MDB_OK;

		case MDB_SET_DOS2_FREQ_REG:
					doser_vibro[DOSER_2].pwm_freq=value;
					doser_vibro[DOSER_2].cmd.b.FREQ=1;
					return MDB_OK;

		case MDB_SET_DOS3_FREQ_REG:
					doser_vibro[DOSER_3].pwm_freq=value;
					doser_vibro[DOSER_3].cmd.b.FREQ=1;
					return MDB_OK;

		case MDB_SET_VIBRO_FREQ_REG:
					doser_vibro[VIBRO_0].pwm_freq=value;
					doser_vibro[VIBRO_0].cmd.b.FREQ=1;
					return MDB_OK;

/*DOSER and VIBRO PWM duty cycle*/
		case MDB_SET_DOS1_DUTY_REG:
					if (value<101){
						doser_vibro[DOSER_1].pwm_duty=value;
						doser_vibro[DOSER_1].cmd.b.DUTY=1;
					}
					return MDB_OK;
		case MDB_SET_DOS2_DUTY_REG:
						if (value<101){
							doser_vibro[DOSER_2].pwm_duty=value;
							doser_vibro[DOSER_2].cmd.b.DUTY=1;
						}
						return MDB_OK;
		case MDB_SET_DOS3_DUTY_REG:
						if (value<101){
							doser_vibro[DOSER_3].pwm_duty=value;
							doser_vibro[DOSER_3].cmd.b.DUTY=1;
						}
						return MDB_OK;
		case MDB_SET_VIBRO_DUTY_REG:
						if (value<101){
							doser_vibro[VIBRO_0].pwm_duty=value;
							doser_vibro[VIBRO_0].cmd.b.DUTY=1;
						}
						return MDB_OK;

/*DOSER and VIBRO Direction*/
		case MDB_SET_DIR_DOS1_REG:
						if (value<2){
							doser_vibro[DOSER_1].DIR_pin=value;
							doser_vibro[DOSER_1].DIR=value;
							//HAL_GPIO_WritePin(DIR_DOSER1_GPIO_Port, DIR_DOSER1_Pin, value);
							doser_vibro[DOSER_1].cmd.b.DIR=1;
						}
						return MDB_OK;
		case MDB_SET_DIR_DOS2_REG:
						if (value<2){
							doser_vibro[DOSER_2].DIR_pin=value;
							doser_vibro[DOSER_2].DIR=value;
							doser_vibro[DOSER_2].cmd.b.DIR=1;
						}
						return MDB_OK;
		case MDB_SET_DIR_DOS3_REG:
						if (value<2){
							doser_vibro[DOSER_3].DIR_pin=value;
							doser_vibro[DOSER_3].DIR=value;
							doser_vibro[DOSER_3].cmd.b.DIR=1;
						}
						return MDB_OK;

		case MDB_SET_DIR_VIBRO_REG:
						if (value<2){
							doser_vibro[VIBRO_0].DIR_pin=value;
							doser_vibro[VIBRO_0].DIR=value;
							doser_vibro[VIBRO_0].cmd.b.DIR=1;
						}
						return MDB_OK;

/*DOSER and VIBRO DISable*/
		case MDB_SET_STOP_DOS1_REG:
						if (value<2){
							doser_vibro[DOSER_1].DIS_pin=value;
							doser_vibro[DOSER_1].DIS=value;
							//HAL_GPIO_WritePin(DIS_DOSER1_GPIO_Port, DIS_DOSER1_Pin, value);
							doser_vibro[DOSER_1].cmd.b.DIS=1;
						}
						return MDB_OK;

		case MDB_SET_STOP_DOS2_REG:
								if (value<2){
									doser_vibro[DOSER_2].DIS_pin=value;
									doser_vibro[DOSER_2].DIS=value;
									doser_vibro[DOSER_2].cmd.b.DIS=1;
								}
								return MDB_OK;
		case MDB_SET_STOP_DOS3_REG:
								if (value<2){
									doser_vibro[DOSER_3].DIS_pin=value;
									doser_vibro[DOSER_3].DIS=value;
									doser_vibro[DOSER_3].cmd.b.DIS=1;
								}
								return MDB_OK;
		case MDB_SET_STOP_VIBRO_REG:
								if (value<2){
									doser_vibro[VIBRO_0].DIS_pin=value;
									doser_vibro[VIBRO_0].DIS=value;
									doser_vibro[VIBRO_0].cmd.b.DIS=1;
								}
								return MDB_OK;

/*FAN PWM frequency*/
		case MDB_SET_FAN1_FREQ_REG:
					fan[FAN_1].pwm_freq=value;
					fan[FAN_1].cmd.b.FREQ=1;
					return MDB_OK;

		case MDB_SET_FAN2_FREQ_REG:
					fan[FAN_2].pwm_freq=value;
					fan[FAN_2].cmd.b.FREQ=1;
					return MDB_OK;


/*FAN PWM duty cycle*/
		case MDB_SET_FAN1_DUTY_REG:
					if (value<101){
						fan[FAN_1].pwm_duty[0]=value;
						fan[FAN_1].cmd.b.DUTY=1;
					}
					return MDB_OK;
		case MDB_SET_FAN2_DUTY_REG:
						if (value<101){
							fan[FAN_2].pwm_duty[0]=value;
							fan[FAN_2].cmd.b.DUTY=1;
						}
				return MDB_OK;

/*FAN Power pin*/
		case MDB_SET_POWER_FAN1_REG:
						if (value<2){
							fan[FAN_1].POWER_pin=value;
							fan[FAN_1].cmd.b.DIS=1;
						}
						return MDB_OK;

		case MDB_SET_POWER_FAN2_REG:
								if (value<2){
									fan[FAN_2].POWER_pin=value;

									fan[FAN_2].cmd.b.DIS=1;
								}
						return MDB_OK;


/*HOPPER BURR STEPPER Pos TARGET*/
			case MDB_SET_HOP1_POS_REG:
				  hopper_burr[HOPPER_1].step_from_home=value;
				 return MDB_OK;

			case MDB_SET_HOP2_POS_REG:
				  hopper_burr[HOPPER_2].step_from_home=value;
				 return MDB_OK;

			case MDB_SET_HOP3_POS_REG:
				  hopper_burr[HOPPER_3].step_from_home=value;
				 return MDB_OK;

			case MDB_SET_BURR0_POS_REG:
				  hopper_burr[BURR_0].step_from_home=value;
				 return MDB_OK;


/*HOPPER BURR STEPPER Freq*/
			case MDB_SET_HOP1_FREQ_REG:
				  hopper_burr[HOPPER_1].pwm_freq=value;
				 return MDB_OK;

			case MDB_SET_HOP2_FREQ_REG:
				  hopper_burr[HOPPER_2].pwm_freq=value;
				 return MDB_OK;

			case MDB_SET_HOP3_FREQ_REG:
				  hopper_burr[HOPPER_3].pwm_freq=value;
				 return MDB_OK;

			case MDB_SET_BURR0_FREQ_REG:
				  hopper_burr[BURR_0].pwm_freq=value;
				 return MDB_OK;

 /*HOPPER BURR STEPPER DIR*/
			case MDB_SET_HOP1_DIR_REG:
				  hopper_burr[HOPPER_1].DIR_pin=value;
				 return MDB_OK;

			case MDB_SET_HOP2_DIR_REG:
				  hopper_burr[HOPPER_2].DIR_pin=value;
				 return MDB_OK;

			case MDB_SET_HOP3_DIR_REG:
				  hopper_burr[HOPPER_3].DIR_pin=value;
				 return MDB_OK;

			case MDB_SET_BURR0_DIR_REG:
				  hopper_burr[BURR_0].DIR_pin=value;
				 return MDB_OK;

/*ILLUMINA PWM duty cycle*/
	       case MDB_SET_ILLUMINA_DUTY_REG:
	    	     if (value<101){
	    	    	 illuminazione.pwm_duty=value;
					 illuminazione.cmd.b.DUTY=1;
				 }
				 return MDB_OK;

		default:

			     new_data_rx=false;
			     return -MDB_ERR_ILLEGAL_DATAADDRESS;

		}


}
