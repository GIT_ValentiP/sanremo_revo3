/*
 ******************************************************************************
 *  @file      : st_l6474_steppe.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 24 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "st_l6474_stepper.h"
#include "gpio.h"
#include "x_nucleo_ihmxx.h"
#include "l6474.h"
#include "spi.h"
#include "tim.h"
#include "mdb_com.h"
#include <stdlib.h>
#include <stdio.h>
//#include "usart.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define FIRST_GRIND_MULTIPLIER           1485// 1um=148,5step
#define SECOND_GRIND_MULTIPLIER            10 //2000
#define SIZE_MAX_STEPS_BURR            148480//=1mm  1um=148,5step  //steps to do 2000 ucron
#define BURRR_ENCODER_ZERO_ANGLE            0   // angle encoder for Grind position
#define BURR_1DEG_TO_STEPS                106//98
#define BURR_STEP_MICRON                  149//150 //num step to move of 1 micron

#define MIN_ANGLE_MOVEMENT_ZERO_FINDER     10//absolute angle degree encoder movement
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi4;
extern TIM_HandleTypeDef htim5;
/* Private function prototypes -----------------------------------------------*/
void stepper_read_FLAG_status(stepper_enum_t id);
static void MyFlagInterruptHandler(void);
void init_stepper_STCK(void);
void stepper_test_SPI(void);
void stepper_DIR(stepper_enum_t id,uint16_t ndir);
uint8_t read_single_sensor_hall(stepper_enum_t id);

void stepper_burr_calibration(stepper_enum_t id);
void stepper_homing(stepper_enum_t id);
//uint16_t burr_step_from_size(void);
//void stepper_burr_position(void);
bool stepper_open_hopper(stepper_enum_t id,uint16_t topen,uint32_t pos_open);
bool stepper_stall_open(stepper_enum_t id,uint16_t topen,uint32_t pos_open);
/* Private user code ---------------------------------------------------------*/

uint8_t read_single_sensor_hall(stepper_enum_t id){
	uint8_t hall_status;

	switch(id){
			  case(BURR_0):
			 	 			 	   pos_sensor.reg_b.burr_far=HAL_GPIO_ReadPin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin);
			 	 			 	   pos_sensor.reg_b.burr_near=HAL_GPIO_ReadPin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin);
			 	 			 	   hall_status=(pos_sensor.reg_b.burr_far||pos_sensor.reg_b.burr_near);
					               break;

			  case(HOPPER_1):
		                           pos_sensor.reg_b.hop1_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL1_GPIO_Port, SENS_CLOSE_HALL1_Pin);
			                       hall_status=pos_sensor.reg_b.hop1_close;
			 			           break;

			  case(HOPPER_2):
		                           pos_sensor.reg_b.hop2_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL2_GPIO_Port, SENS_CLOSE_HALL2_Pin);
					               hall_status=pos_sensor.reg_b.hop2_close;
				 			       break;

			  case(HOPPER_3):
		                           pos_sensor.reg_b.hop3_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL3_GPIO_Port, SENS_CLOSE_HALL3_Pin);
					               hall_status=pos_sensor.reg_b.hop3_close;
				 			       break;
			  default:
				        break;

			}

return (hall_status);

}



void stepper_homing(stepper_enum_t id){

	 int32_t pos;
	 uint8_t status_hall=0x01;
     motorState_t stepper_state;

	 stepper_state=BSP_MotorControl_GetDeviceState(id);
     if (stepper_state == INACTIVE){
    	  status_hall=read_single_sensor_hall(id);
    	  if (status_hall != 0){
    		  switch(id){
				  case(HOPPER_1):
		                             BSP_MotorControl_Move(id,BACKWARD,64);
									 break;
				  case(HOPPER_2):
		                             BSP_MotorControl_Move(id,BACKWARD,64);
									 break;
				  case(HOPPER_3):

				                     BSP_MotorControl_Move(id,FORWARD,64);
									 break;
				  default:
							break;
			 }
//    		  BSP_MotorControl_Move(id,hopper_burr[id].DIR_pin,96);
    	  }else{//reached home position
    		 // led_service_ON_OFF(id+1,status_hall);

			  pos = BSP_MotorControl_GetPosition(id);
			  BSP_MotorControl_SetHome(id, pos);
			  BSP_MotorControl_CmdDisable(id);
			  hopper_burr[id].step_pos=0;
			  hopper_burr[id].STOP_pin=GPIO_PIN_RESET;
			  hopper_burr[id].cmd=IDLE_STEPPER;
    	  }

     }

}


void read_encoder_pos(void){
uint8_t enc_data[2];
//uint16_t old_angle;

//old_angle=encoder_burr.angle_pos;
HAL_GPIO_WritePin(SPI2_NSS_ENCODER_MAC_GPIO_Port, SPI2_NSS_ENCODER_MAC_Pin, GPIO_PIN_RESET);

	HAL_SPI_Receive(&hspi2,(uint8_t *)enc_data,1,1000);//(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
	encoder_burr.ang_pos.pos_w=(enc_data[1]<<8);
	encoder_burr.ang_pos.pos_w+=enc_data[0];
	encoder_burr.angle_pos=361;
	//if (encoder_burr.ang_pos.pos_bit.E2==1){
	  encoder_burr.ang_pos.pos_bit.ANG_POS<<=1;
	  encoder_burr.angle_pos=((encoder_burr.ang_pos.pos_bit.ANG_POS*360)/1023);
//	  if (encoder_burr.angle_pos >encoder_burr.angle_zero_pos){
//	 	     hopper_burr[BURR_0].burr_distance_rd=((((360-encoder_burr.angle_pos)+encoder_burr.angle_zero_pos)*BURR_1DEG_TO_STEPS)/BURR_STEP_MICRON);
//	 	  }else{
//	 		 hopper_burr[BURR_0].burr_distance_rd=((((360-encoder_burr.angle_zero_pos)+encoder_burr.angle_pos)*BURR_1DEG_TO_STEPS)/BURR_STEP_MICRON);
//	 	  }
	//}

HAL_GPIO_WritePin(SPI2_NSS_ENCODER_MAC_GPIO_Port, SPI2_NSS_ENCODER_MAC_Pin, GPIO_PIN_SET);

}


void stepper_burr_find_zero(stepper_enum_t id){

	 int32_t pos;
     motorState_t stepper_state;
     uint16_t diff;
    // uint16_t delta_angle_pos;

	  switch( hopper_burr[id].init_cmd){
		      case(START_INIT_STEPPER):
										   BSP_MotorControl_Init(id, NULL);  /* Initialisation of first device */
										   pos = BSP_MotorControl_GetPosition(id); /* Get current position of device 0*/
										   BSP_MotorControl_SetHome(id, pos); /* Set Current position to be the home position for device 0 */
										   hopper_burr[id].step_pos=pos;
										   hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//ATTIVO PWM //DISATTIVO PWM GPIO_PIN_SET;
										   read_encoder_pos();
										   encoder_burr.old_pos=encoder_burr.angle_pos;
										   printf("\n\r STARTAngle :%d�",encoder_burr.angle_pos);
										   BSP_MotorControl_CmdEnable(id);
										   encoder_burr.delta_angle_pos=15;
										   encoder_burr.direction=AVVICINA_MACINA;
										   encoder_burr.micron_moved_pos=0;
										   BSP_MotorControl_Move(id,AVVICINA_MACINA,1485);//148480=1000um);//24000);//BSP_MotorControl_Move(id,FORWARD,6400);
										   hopper_burr[id].init_cmd=ENABLE_INIT_STEPPER;
		                                   timeout_zero_stepper=450;//unit 100ms
		    		                       break;
		      case(ENABLE_INIT_STEPPER):
		                                   stepper_state=BSP_MotorControl_GetDeviceState(BURR_0);
		                                   if ((stepper_state != INACTIVE) && (timeout_zero_stepper)){
				                        	   hopper_burr[id].init_cmd=ENABLE_INIT_STEPPER;
				                           }else if (stepper_state == INACTIVE){//10
				                        	          read_encoder_pos();
													  if((encoder_burr.angle_pos+1)>  encoder_burr.old_pos){
														  encoder_burr.delta_angle_pos=(encoder_burr.angle_pos-encoder_burr.old_pos);
													  }else{
														  encoder_burr.delta_angle_pos=(encoder_burr.angle_pos+(360-encoder_burr.old_pos));
													  }
													  encoder_burr.old_pos=encoder_burr.angle_pos;
													  encoder_burr.micron_moved_pos+=(( encoder_burr.delta_angle_pos*BURR_1DEG_TO_STEPS)/(BURR_STEP_MICRON*BURR_DISTANCE_COEFF));
													  printf("\n\r BurrAngle :%d�,Dist.:%d[um],Move:%d�",encoder_burr.angle_pos,encoder_burr.micron_moved_pos, encoder_burr.delta_angle_pos);
													  if( encoder_burr.delta_angle_pos > MIN_ANGLE_MOVEMENT_ZERO_FINDER){//BURRs are far from zero position
													     BSP_MotorControl_Move(id,AVVICINA_MACINA,1485);//148480=1000um);//24000);//BSP_MotorControl_Move(id,FORWARD,6400);
														 //timeout_zero_stepper=450;
														 hopper_burr[id].init_cmd=ENABLE_INIT_STEPPER;
													   }else{//BURRs are close
														      stepper_burr_stall(1000);//(600);//increase torque to detach burrs
															  read_encoder_pos();
															  encoder_burr.old_pos=encoder_burr.angle_pos;
															  BSP_MotorControl_Move(id,ALLONTANA_MACINA,1485*6*BURR_DISTANCE_COEFF);//=10um;//148480=1000um);//24000);//BSP_MotorControl_Move(id,FORWARD,6400);
															  timeout_zero_stepper=100;
															  hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
													   }
				                                 }else{//timeout
				                                 	    printf("\n\rTIMEOUT Positioning HOPPER_%d",id);
				                                		hopper_burr[id].init_cmd=END_INIT_STEPPER;
				                                 }
		     	    		               break;
		      case(STOP_MOVE_INIT_STEPPER)://ALLONTANA MACINA fino al distaccamento
			                               stepper_state=BSP_MotorControl_GetDeviceState(id);
										   if ((stepper_state != INACTIVE) && (timeout_zero_stepper)){
											   hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
										   }else if (stepper_state == INACTIVE){//stepper reached position target
											        read_encoder_pos();
											   		encoder_burr.angle_zero_pos=encoder_burr.angle_pos;//encoder_old_pos;
											   		if(encoder_burr.angle_pos >  encoder_burr.old_pos){
											   		  diff=((360-encoder_burr.angle_pos)+ encoder_burr.old_pos);
											   		}else{
											   		  diff=( encoder_burr.old_pos-encoder_burr.angle_pos);
											   		}
											   	   hopper_burr[BURR_0].burr_distance_rd=((diff*BURR_1DEG_TO_STEPS)/(BURR_STEP_MICRON*BURR_DISTANCE_COEFF));//Offset 0f 60um
												   if (hopper_burr[BURR_0].burr_distance_rd < 10){//BURRs are yet too  close
													 read_encoder_pos();
												     encoder_burr.old_pos=encoder_burr.angle_pos;
													 BSP_MotorControl_Move(id,ALLONTANA_MACINA,1485*6*BURR_DISTANCE_COEFF);//=10um;//148480=1000um);//24000);//BSP_MotorControl_Move(id,FORWARD,6400);
													// timeout_zero_stepper=450;
												     hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
												   }else{//BURRs are detached enough
												        hopper_burr[id].init_cmd=END_INIT_STEPPER;
												   }
												 }else{//timeout
													  hopper_burr[id].init_cmd=END_INIT_STEPPER;
												 }
		   	     	    		           break;
		      case(MOVE_INIT_STEPPER):
		   	     	    		           break;
		      case(HOME_INIT_STEPPER):
		   	     	    		           break;
		      case(END_INIT_STEPPER):
											if ( encoder_burr.micron_moved_pos > hopper_burr[BURR_0].burr_distance_rd){
												encoder_burr.micron_moved_pos -= hopper_burr[BURR_0].burr_distance_rd;
											}else{
												encoder_burr.micron_moved_pos =0;
											}
											printf("\n From last pos. moved:%d[um]\n",encoder_burr.micron_moved_pos);
											stepper_burr_stall(0);
											pos = BSP_MotorControl_GetPosition(id);
											BSP_MotorControl_SetHome(id, pos);
											hopper_burr[id].step_pos=0;
											BSP_MotorControl_CmdDisable(id);
											hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//GPIO_PIN_RESET;
											hopper_burr[id].cmd=IDLE_STEPPER;
											hopper_burr[BURR_0].init_cmd=HOME_INIT_STEPPER;//finish INIT HOPPER and BURRS

		   	     	    		           break;
		      default:
		    	                           break;
	     }




}

void stepper_burr_calibration(stepper_enum_t id){

	 int32_t pos;
	 uint8_t status_cal=0x01;
     motorState_t stepper_state;

     BSP_MotorControl_CmdEnable(id);
     status_cal=revo_calibration_cmd.cal_b.BURR_POS_CAL_START;
     timeout_zero_stepper=400;
     while ((status_cal != 0) && (timeout_zero_stepper)){
 	     BSP_MotorControl_Move(id,FORWARD,1000);
		 do{
			 mdb_comm_processMessage();
			 stepper_state=BSP_MotorControl_GetDeviceState(id);
		 }while(stepper_state != INACTIVE); /* Wait for the motor of device id ends moving */
		  status_cal=revo_calibration_cmd.cal_b.BURR_POS_CAL_START;
     }
	  pos = BSP_MotorControl_GetPosition(id);
	  BSP_MotorControl_SetHome(id, pos);
	  BSP_MotorControl_CmdDisable(id);
	  hopper_burr[id].step_pos=0;
	  hopper_burr[id].STOP_pin=GPIO_PIN_RESET;
	  hopper_burr[id].cmd=IDLE_STEPPER;
}



void stepper_find_zero(stepper_enum_t id){

	 int32_t pos;
	 //int32_t step_inc=0;
	 uint8_t status_hall=0x01;
     motorState_t stepper_state;

     switch( hopper_burr[id].init_cmd){
	      case(START_INIT_STEPPER):
		                               printf("\n\rHoming Hop:%d",id);
									   BSP_MotorControl_Init(id, NULL);  /* Initialisation of first device */
									   pos = BSP_MotorControl_GetPosition(id); /* Get current position of device 0*/
									   BSP_MotorControl_SetHome(id, pos); /* Set Current position to be the home position for device 0 */
									   hopper_burr[id].step_pos=pos;
									   hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//ATTIVO PWM //DISATTIVO PWM GPIO_PIN_SET;
		                               BSP_MotorControl_CmdEnable(id);
	                                   hopper_burr[id].init_cmd=ENABLE_INIT_STEPPER;
	                                   timeout_zero_stepper=100;
	    		                       break;
	      case(ENABLE_INIT_STEPPER):

										   stepper_state=BSP_MotorControl_GetDeviceState(id);
										   if (stepper_state != INACTIVE){
											   hopper_burr[id].init_cmd=ENABLE_INIT_STEPPER;
										   }else{
											   pos = BSP_MotorControl_GetPosition(id);

											   BSP_MotorControl_SetHome(id, pos);/* Set Current position to be the home position for device 0 */
											   switch(id){
													  case(HOPPER_1):
																		 BSP_MotorControl_SetMaxSpeed(id, 500);
																		 BSP_MotorControl_SetMinSpeed(id, 100);
																		 BSP_MotorControl_Move(id,BACKWARD,4800);
																		 break;
													  case(HOPPER_2):
																		 BSP_MotorControl_SetMaxSpeed(id, 300);
																		 BSP_MotorControl_SetMinSpeed(id, 50);
																		 BSP_MotorControl_Move(id,BACKWARD,3200);
																		 break;
													  case(HOPPER_3):
																		 BSP_MotorControl_SetMaxSpeed(id, 300);
																		 BSP_MotorControl_SetMinSpeed(id, 50);
																		 BSP_MotorControl_Move(id,FORWARD,1600);
																		 break;
													  default:
																		break;
												 }
											 hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
											 timeout_zero_stepper=250;
										   }

	     	    		               break;
	      case(STOP_MOVE_INIT_STEPPER):
		                               stepper_state=BSP_MotorControl_GetDeviceState(id);
									   if ((stepper_state != INACTIVE) && (timeout_zero_stepper)){
										   hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
									   }else if (stepper_state == INACTIVE){//stepper reached position target
											   status_hall=read_single_sensor_hall(id);
											   if (status_hall != 0){//not in zero position
												   switch(id){
													  case(HOPPER_1):
																		 BSP_MotorControl_Move(id,BACKWARD,64);
																		 break;
													  case(HOPPER_2):
																		 BSP_MotorControl_Move(id,BACKWARD,64);
																		 break;
													  case(HOPPER_3):
																		 BSP_MotorControl_Move(id,FORWARD,64);
																		 break;
													  default:
																break;
												  }
											     hopper_burr[id].init_cmd=STOP_MOVE_INIT_STEPPER;
											   }else{//stepper reach home
											        hopper_burr[id].init_cmd=END_INIT_STEPPER;
											   }
											 }else{//timeout
												  printf("\n\rTIMEOUT Positioning HOPPER_%d",id);
												  hopper_burr[id].init_cmd=END_INIT_STEPPER;
											 }
	   	     	    		           break;
	      case(MOVE_INIT_STEPPER):
	   	     	    		           break;
	      case(HOME_INIT_STEPPER):
	   	     	    		           break;
	      case(END_INIT_STEPPER):
								       pos = BSP_MotorControl_GetPosition(id);
									   BSP_MotorControl_CmdDisable(id);
									   BSP_MotorControl_SetHome(id, pos);/* Set Current position to be the home position for device 0 */
									   hopper_burr[id].step_pos=0;
									   hopper_burr[id].STOP_pin=GPIO_PIN_SET;
									   hopper_burr[id].cmd=IDLE_STEPPER;
									   switch(id){
											  case(HOPPER_1):
		                                                         hopper_burr[HOPPER_1].cmd=IDLE_STEPPER;
											                     hopper_burr[HOPPER_1].init_cmd=HOME_INIT_STEPPER;
		                                                         hopper_burr[HOPPER_2].cmd=INIT_HOPPER2;
											                     hopper_burr[HOPPER_2].init_cmd=START_INIT_STEPPER;
																 break;
											  case(HOPPER_2):
		                                                         hopper_burr[HOPPER_2].cmd=IDLE_STEPPER;
											                     hopper_burr[HOPPER_2].init_cmd=HOME_INIT_STEPPER;
		                                                         hopper_burr[HOPPER_3].cmd=INIT_HOPPER3;
													             hopper_burr[HOPPER_3].init_cmd=START_INIT_STEPPER;
																 break;
											  case(HOPPER_3):
															  	 hopper_burr[HOPPER_3].cmd=IDLE_STEPPER;
											                     hopper_burr[HOPPER_3].init_cmd=HOME_INIT_STEPPER;
																 hopper_burr[BURR_0].cmd=INIT_BURR;
																 hopper_burr[BURR_0].init_cmd=START_INIT_STEPPER;
																 break;
											  default:
																break;
									   }
	   	     	    		           break;
	      default:
	    	                           break;
     }

}

void init_stepper_STCK(void){
	uint8_t id;

	for(id=0;id < HOPPER_MAX;id++){
		hopper_burr[id].STOP_pin=GPIO_PIN_SET;//ATTIVO PWM //DISATTIVO PWM GPIO_PIN_SET;

		if (id==HOPPER_3){
			hopper_burr[id].DIR_pin=GPIO_PIN_RESET;//REVERSE dir is possible to set trough SPI bus
		}else{
			hopper_burr[id].DIR_pin=GPIO_PIN_SET;//REVERSE dir is possible to set trough SPI bus
		}


		hopper_burr[id].step_from_home=0;
	}
}
void stepper_test_SPI(void){
	 uint8_t id,i;
	 uint8_t *pByteToTransmit;
	 uint8_t *pReceivedByte;
	 uint8_t spi1TX[4];
	 uint8_t spi1RX[4];
	 uint8_t  status;
    // uint32_t spiRxData;


	 for (id=0;id<HOPPER_MAX;id++){
		L6474_Board_Set_CS_Gpio(id, GPIO_PIN_RESET);
		//printf("\n\r SPI1 Tx \n\r");
		spi1TX[0] = L6474_ENABLE;
		spi1TX[1] = L6474_ENABLE;
		spi1TX[2] = L6474_NOP;
		spi1TX[3] = L6474_ENABLE;
		spi1RX[1] = 0;
		spi1RX[2] = 0;
		spi1RX[3] = 0;
		pByteToTransmit=&spi1TX[0];
		pReceivedByte=&spi1RX[0];
	    for (i = 0;i <4;i++){
	    	status = HAL_SPI_TransmitReceive(&hspi1, pByteToTransmit, pReceivedByte, 1, 1000);
			if (status != HAL_OK){
				//printf("\n\rERR.SPI1\n\r");
				break;
			}
			pByteToTransmit++;
			pReceivedByte++;
		}

	    //spiRxData = (((uint32_t)spi1RX[1] << 16)|(spi1RX[2]<< 8) |spi1RX[3]);
		//printf("\n\r SPI1:%lu \n\r",spiRxData);
	    L6474_Board_Set_CS_Gpio(id, GPIO_PIN_SET);

	 }


}

void init_encoder_burr(void){
	HAL_SPI_MspInit(&hspi2);
	HAL_GPIO_WritePin(SPI2_NSS_ENCODER_MAC_GPIO_Port, SPI2_NSS_ENCODER_MAC_Pin, GPIO_PIN_SET);
}

void init_stepper(void){

	  //int32_t pos;

	  init_stepper_STCK();
	 // HAL_SPI_MspInit(&hspi2);
	 /* Set the L6474 library to use 3 device */
	  BSP_MotorControl_SetNbDevices(BSP_MOTOR_CONTROL_BOARD_ID_L6474,4);

	  /* When BSP_MotorControl_Init is called with NULL pointer,                  */
	  /* the L6474 registers and parameters are set with the predefined values from file   */
	  /* l6474_target_config.h, otherwise the registers are set using the   */
	  /* L6474_Init_t pointer structure                */
	  /* The first call to BSP_MotorControl_Init initializes the first device     */
	  /* whose Id is 0.                                                           */
	  /* The nth call to BSP_MotorControl_Init initializes the nth device         */
	  /* whose Id is n-1.                                                         */

	  /* Attach the function MyFlagInterruptHandler (defined below) to the flag interrupt */
	 BSP_MotorControl_AttachFlagInterrupt(MyFlagInterruptHandler);
	  /* Attach the function Error_Handler (defined below) to the error Handler*/
	 BSP_MotorControl_AttachErrorHandler(Error_Handler);


	 hopper_burr[HOPPER_1].cmd=INIT_HOPPER1;
	 hopper_burr[HOPPER_1].init_cmd=START_INIT_STEPPER;
	 hopper_burr[HOPPER_2].cmd=IDLE_STEPPER;
	 hopper_burr[HOPPER_2].init_cmd=HOME_INIT_STEPPER;
	 hopper_burr[HOPPER_3].cmd=IDLE_STEPPER;
     hopper_burr[HOPPER_3].init_cmd=HOME_INIT_STEPPER;
     hopper_burr[BURR_0].cmd=IDLE_STEPPER;
     hopper_burr[BURR_0].init_cmd=MOVE_INIT_STEPPER;

//	  BSP_MotorControl_Init(HOPPER_1, NULL);  /* Initialisation of first device */
//	  pos = BSP_MotorControl_GetPosition(HOPPER_1); /* Get current position of device 0*/
//	  BSP_MotorControl_SetHome(HOPPER_1, pos); /* Set Current position to be the home position for device 0 */
//	  hopper_burr[HOPPER_1].step_pos=pos;
//	  hopper_burr[HOPPER_1].STOP_pin=GPIO_PIN_RESET;//ATTIVO PWM //DISATTIVO PWM GPIO_PIN_SET;
//      stepper_find_zero(HOPPER_1);
//
//      if (revo_board_conf==REVO3_CONFIG){
//		  /* Initialisation of second device */
//		  BSP_MotorControl_Init(HOPPER_2, NULL);
//		  /* Get current position of device 1*/
//		  pos = BSP_MotorControl_GetPosition(HOPPER_2);
//		  /* Set Current position to be the home position for device 1 */
//		  BSP_MotorControl_SetHome(HOPPER_2, pos);
//		  hopper_burr[HOPPER_2].step_pos=pos;
//		  hopper_burr[HOPPER_2].STOP_pin=GPIO_PIN_RESET;
//		  stepper_find_zero(HOPPER_2);
//
//		  /* Initialisation of third device */
//		  BSP_MotorControl_Init(HOPPER_3, NULL);
//		  /* Get current position of device 2*/
//		  pos = BSP_MotorControl_GetPosition(HOPPER_3);
//		  /* Set Current position to be the home position for device 2 */
//		  BSP_MotorControl_SetHome(HOPPER_3, pos);
//		  hopper_burr[HOPPER_3].step_pos=pos;
//		  hopper_burr[HOPPER_3].STOP_pin=GPIO_PIN_RESET;
//		  stepper_find_zero(HOPPER_3);
//      }
//
//	  BSP_MotorControl_Init(BURR_0, NULL);
//	  pos = BSP_MotorControl_GetPosition(BURR_0); /* Get current position of device 0*/
//	  BSP_MotorControl_SetHome(BURR_0, pos);/* Set Current position to be the home position for device 0 */
//	  hopper_burr[BURR_0].step_pos=pos;
//	  hopper_burr[BURR_0].STOP_pin=GPIO_PIN_RESET;//ATTIVO PWM //DISATTIVO PWM GPIO_PIN_SET;
//	  stepper_burr_find_zero(BURR_0);
}



void read_sensor_hall(void){
uint8_t	id=0;
for(id=0;id<SENSOR_POS_MAX;id++){
	switch(id){
	  case(PORTA_FILTER_0):
		                   pos_sensor.reg_b.portafilter=HAL_GPIO_ReadPin(CTRL_IN_P_FILTRO_GPIO_Port, CTRL_IN_P_FILTRO_Pin);
			               break;

	  case(DOSER1_PRESENT):
	 		               pos_sensor.reg_b.dos1_pres=HAL_GPIO_ReadPin(MICRO_DOSER1_GPIO_Port, MICRO_DOSER1_Pin);
	 			           break;

	  case(DOSER2_PRESENT):
		 		           pos_sensor.reg_b.dos2_pres=HAL_GPIO_ReadPin(MICRO_DOSER2_GPIO_Port, MICRO_DOSER2_Pin);
		 			       break;

	  case(DOSER3_PRESENT):
		 		           pos_sensor.reg_b.dos3_pres=HAL_GPIO_ReadPin(MICRO_DOSER3_GPIO_Port, MICRO_DOSER3_Pin);
		 			       break;

	  case(HOPPER1_OPEN):
		 		           pos_sensor.reg_b.hop1_open=HAL_GPIO_ReadPin(SENS_OPEN_HALL1_GPIO_Port, SENS_OPEN_HALL1_Pin);
		 			       break;

	  case(HOPPER1_CLOSE):
			 		       pos_sensor.reg_b.hop1_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL1_GPIO_Port, SENS_CLOSE_HALL1_Pin);
			 			   break;

	  case(HOPPER2_OPEN):
	 		 		       pos_sensor.reg_b.hop2_open=HAL_GPIO_ReadPin(SENS_OPEN_HALL2_GPIO_Port, SENS_OPEN_HALL2_Pin);
	 		 			   break;

	  case(HOPPER2_CLOSE):
	 			 		   pos_sensor.reg_b.hop2_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL2_GPIO_Port, SENS_CLOSE_HALL2_Pin);
	 			 		   break;

	  case(HOPPER3_OPEN):
	 		 		       pos_sensor.reg_b.hop3_open=HAL_GPIO_ReadPin(SENS_OPEN_HALL3_GPIO_Port, SENS_OPEN_HALL3_Pin);
	 		 			   break;

	  case(HOPPER3_CLOSE):
	 			 		   pos_sensor.reg_b.hop3_close=HAL_GPIO_ReadPin(SENS_CLOSE_HALL3_GPIO_Port, SENS_CLOSE_HALL3_Pin);
	 			 		   break;

	  case(BURR_FAR):
	 	 			 	   pos_sensor.reg_b.burr_far=HAL_GPIO_ReadPin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin);
					       break;

	  case(BURR_NEAR):
	 	 			 	   pos_sensor.reg_b.burr_near=HAL_GPIO_ReadPin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin);
	 	 			 	   break;

	  default:
		      break;
	}
}


}



void read_portafilter(void){

}



void stepper_read_FLAG_status(stepper_enum_t id){

	switch(id){

		  case(BURR_0):
		                       hopper_burr[id].FLAG_ERR_pin =HAL_GPIO_ReadPin(FLAG_BURR_STEPPER4_GPIO_Port, FLAG_BURR_STEPPER4_Pin);
				               break;

		  case(HOPPER_1):
	                           hopper_burr[id].FLAG_ERR_pin =HAL_GPIO_ReadPin(FLAG_STEPPER1_GPIO_Port, FLAG_STEPPER1_Pin);
		 			           break;

		  case(HOPPER_2):
		                       hopper_burr[id].FLAG_ERR_pin =HAL_GPIO_ReadPin(FLAG_STEPPER2_GPIO_Port, FLAG_STEPPER2_Pin);
			 			       break;

		  case(HOPPER_3):
		                       hopper_burr[id].FLAG_ERR_pin =HAL_GPIO_ReadPin(FLAG_STEPPER3_GPIO_Port, FLAG_STEPPER3_Pin);
			 			       break;
		  default:
			        break;

		}

}

void stepper_clock_STCK(void){//(stepper_enum_t id,uint16_t new_speed){
uint8_t id;
for (id=0;id<HOPPER_MAX;id++){
		if(hopper_burr[id].STOP_pin==GPIO_PIN_RESET){
				switch(id){

				  case(BURR_0):
									   HAL_GPIO_TogglePin(STCK_BURR_STEPPER4_GPIO_Port, STCK_BURR_STEPPER4_Pin);
									   break;

				  case(HOPPER_1):
									   HAL_GPIO_TogglePin(STCK_STEPPER1_GPIO_Port, STCK_STEPPER1_Pin);
									   break;

				  case(HOPPER_2):
									   HAL_GPIO_TogglePin(STCK_STEPPER2_GPIO_Port, STCK_STEPPER2_Pin);
									   break;

				  case(HOPPER_3):
									   HAL_GPIO_TogglePin(STCK_STEPPER3_GPIO_Port, STCK_STEPPER3_Pin);
									   break;
				  default:
							break;

				}
	   }
		//else{
		 //  BSP_MotorControl_CmdDisable(id);
//			switch(id){
//
//						  case(BURR_0):
//											   HAL_GPIO_WritePin(STCK_BURR_STEPPER4_GPIO_Port, STCK_BURR_STEPPER4_Pin,GPIO_PIN_RESET);
//											   break;
//
//						  case(HOPPER_1):
//		                                       HAL_GPIO_WritePin(STCK_STEPPER1_GPIO_Port, STCK_STEPPER1_Pin,GPIO_PIN_RESET);
//											   break;
//
//						  case(HOPPER_2):
//		                                       HAL_GPIO_WritePin(STCK_STEPPER2_GPIO_Port, STCK_STEPPER2_Pin,GPIO_PIN_RESET);
//											   break;
//
//						  case(HOPPER_3):
//		                                       HAL_GPIO_WritePin(STCK_STEPPER3_GPIO_Port, STCK_STEPPER3_Pin,GPIO_PIN_RESET);
//											   break;
//						  default:
//									break;
//
//						}
//	   }
	   stepper_read_FLAG_status(id);
 }
}

void stepper_DIR(stepper_enum_t id,uint16_t ndir){


		switch (id){
					case(HOPPER_1):
			                          HAL_GPIO_WritePin(DIR_STEPPER1_GPIO_Port, DIR_STEPPER1_Pin, ndir);
								      break;
					case(HOPPER_2):
			                          HAL_GPIO_WritePin(DIR_STEPPER2_GPIO_Port, DIR_STEPPER2_Pin, ndir);
									  break;
					case(HOPPER_3):
			                          HAL_GPIO_WritePin(DIR_STEPPER3_GPIO_Port, DIR_STEPPER3_Pin, ndir);
									  break;
					case(BURR_0):
			                          HAL_GPIO_WritePin(DIR_BURR_STEPPER4_GPIO_Port, DIR_BURR_STEPPER4_Pin, ndir);
									  break;
					default:
							          break;

			}
}

bool stepper_open_hopper(stepper_enum_t id,uint16_t topen,uint32_t pos_open){
	bool ret=FALSE;
	//uint32_t pos;
    //motorState_t stepper_state;

    hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//
    BSP_MotorControl_Init(id, NULL);
	BSP_MotorControl_CmdEnable(id);
	switch(id){
	  case(HOPPER_1):


				         //hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//
		                 BSP_MotorControl_Move(id,BACKWARD,pos_open);
		                 //BSP_MotorControl_GoTo(id, pos_open);
						 break;
	  case(HOPPER_2):
		                 BSP_MotorControl_Move(id,BACKWARD,pos_open);

						 break;
	  case(HOPPER_3):

	                     BSP_MotorControl_Move(id,FORWARD,pos_open);
						 break;
	  default:


						 break;

	 }
	 hopper_burr[id].cmd=FINISH_MOVEMENT_STEPPER;
	 ret=TRUE;

	return(ret);
}

bool stepper_stall_open(stepper_enum_t id,uint16_t topen,uint32_t pos_open){
	bool ret=FALSE;
    hopper_burr[id].STOP_pin=GPIO_PIN_RESET;//
    BSP_MotorControl_Init(id, NULL);
	BSP_MotorControl_CmdEnable(id);
	switch(id){
	  case(HOPPER_1):
		                 BSP_MotorControl_Move(id,BACKWARD,pos_open);

						 break;
	  case(HOPPER_2):
		                 BSP_MotorControl_Move(id,BACKWARD,pos_open);

						 break;
	  case(HOPPER_3):

	                     BSP_MotorControl_Move(id,FORWARD,pos_open);
						 break;
	  default:


						 break;

	 }
	 hopper_burr[id].STOP_pin=GPIO_PIN_RESET;
	 hopper_burr[id].cmd=IDLE_STEPPER;
	 ret=TRUE;

	return(ret);
}


void stepper_burr_stall(float Tval){
 float Tval_float;
 uint8_t Tval_byte;

 if(Tval >0){
	 Tval_float=Tval;
	 Tval_byte=((uint8_t)(((Tval_float - 31.25f)*0.032f)+0.5f));
	 BSP_MotorControl_CmdSetParam(BURR_0,  L6474_TVAL,Tval_byte);
	 hopper_burr[BURR_0].STOP_pin=GPIO_PIN_RESET;//
	 BSP_MotorControl_CmdEnable(BURR_0);
 }else{
	 Tval_float=300;
     Tval_byte=((uint8_t)(((Tval_float - 31.25f)*0.032f)+0.5f));
	 BSP_MotorControl_CmdSetParam(BURR_0,  L6474_TVAL,Tval_byte);
	 // hopper_burr[BURR_0].STOP_pin=GPIO_PIN_SET;//
	 BSP_MotorControl_CmdDisable(BURR_0);
 }


}


void stepper_burr_position(void){
  uint32_t step_from_size=0;
  int32_t delta_step;
  motorState_t stepper_state;
  uint16_t last_enc_pos;
  uint16_t step_angle_enc=0;
  int16_t diff;
  step_from_size=(grinder.grinding_size*FIRST_GRIND_MULTIPLIER*BURR_DISTANCE_COEFF)/SECOND_GRIND_MULTIPLIER;
  if (step_from_size < 740){//step to move below 5um
	  step_from_size=750;
  }


  encoder_burr.start_angle=encoder_burr.angle_pos;
  printf("\n StepToMove:%ld \n", step_from_size);
  if ((hopper_burr[BURR_0].step_pos > (step_from_size +750)) || (hopper_burr[BURR_0].step_pos < (step_from_size - 750))){//changed burr position target

      hopper_burr[BURR_0].STOP_pin=GPIO_PIN_RESET;//
      BSP_MotorControl_CmdEnable(BURR_0);


    if(hopper_burr[BURR_0].step_pos > step_from_size){//AVVICINA MACINA
    	 delta_step=(hopper_burr[BURR_0].step_pos - step_from_size);
    	 BSP_MotorControl_Move(BURR_0,AVVICINA_MACINA,delta_step);
    	 encoder_burr.direction=AVVICINA_MACINA;
    }else{//ALLONTANA MACINA
    	 delta_step=(step_from_size - hopper_burr[BURR_0].step_pos);
    	 BSP_MotorControl_Move(BURR_0,ALLONTANA_MACINA,delta_step);
    	 encoder_burr.direction=ALLONTANA_MACINA;
    }
    step_angle_enc=0;
    last_enc_pos=(encoder_burr.angle_pos);
	timeout_zero_stepper=100;
    do{
    	read_encoder_pos();
        mdb_comm_processMessage();
        diff=(last_enc_pos - encoder_burr.angle_pos);
        if ((diff > 1) || (diff < -1)){
			if (encoder_burr.direction==ALLONTANA_MACINA){
			  if(encoder_burr.angle_pos > last_enc_pos){
				step_angle_enc+=((360-encoder_burr.angle_pos)+last_enc_pos);
			  }else{
			   step_angle_enc+=(last_enc_pos-encoder_burr.angle_pos);
			  }
			}else{
			  if((encoder_burr.angle_pos + 1) > last_enc_pos){
				step_angle_enc+=(encoder_burr.angle_pos-last_enc_pos);
			  }else{
				step_angle_enc+=(encoder_burr.angle_pos+(360-last_enc_pos));
			  }
			}
			last_enc_pos=(encoder_burr.angle_pos);
        }
        stepper_state=BSP_MotorControl_GetDeviceState(BURR_0);
        encoder_burr.micron_moved_pos=((step_angle_enc*BURR_1DEG_TO_STEPS)/(BURR_STEP_MICRON*BURR_DISTANCE_COEFF));
    	//printf("\n\r BurrAngle :%d�,Dist.:%d[um]",encoder_burr.angle_pos,encoder_burr.micron_moved_pos);
    }while((stepper_state != INACTIVE)&&(timeout_zero_stepper!=0)); /* Wait for the motor of device id ends moving */

    if(timeout_zero_stepper==0){//timeout
    	printf("\n\rBurrs Timeout to move of:%d[um]",encoder_burr.micron_moved_pos);
    }
    if (encoder_burr.direction==AVVICINA_MACINA){
   	  hopper_burr[BURR_0].burr_distance_rd -=(encoder_burr.micron_moved_pos);
   	}else{
  	  hopper_burr[BURR_0].burr_distance_rd +=(encoder_burr.micron_moved_pos);
    }

    BSP_MotorControl_CmdDisable(BURR_0);
  }

  read_encoder_pos();

  encoder_burr.pre_grind_pos= encoder_burr.angle_pos;//

  hopper_burr[BURR_0].step_pos=step_from_size;
  hopper_burr[BURR_0].STOP_pin=GPIO_PIN_SET;
  hopper_burr[BURR_0].cmd=IDLE_STEPPER;

	//hopper_burr[BURR_0].cmd=BURR_POSITIONING_END_STEPPER;
}

//Electronic positioning of burr
void calibration_burr(uint32_t step,BurrDir_t dir,uint16_t torque_val,uint16_t speed){

//	int32_t pos;
//	int16_t diff;
    motorState_t stepper_state;
    uint16_t step_angle_enc;
    uint16_t last_enc_pos;

    encoder_burr.start_angle=encoder_burr.angle_pos;
    step_angle_enc=0;
    last_enc_pos=(encoder_burr.angle_pos);
    if(pos_sensor.reg_b.burr_far==GPIO_PIN_SET){
    	 BSP_MotorControl_CmdEnable(BURR_0);
    	 BSP_MotorControl_Move(BURR_0,ALLONTANA_MACINA,(step*BURR_DISTANCE_COEFF));//
		 do{
			 stepper_state=BSP_MotorControl_GetDeviceState(BURR_0);
		 }while(stepper_state != INACTIVE);

//		  pos = BSP_MotorControl_GetPosition(BURR_0);
//		  BSP_MotorControl_SetHome(BURR_0, pos);
//		  hopper_burr[BURR_0].step_pos=0;
		  BSP_MotorControl_CmdDisable(BURR_0);
		  hopper_burr[BURR_0].STOP_pin=GPIO_PIN_RESET;
		  read_encoder_pos();
		  //diff=(last_enc_pos - encoder_burr.angle_pos);
		  //if ((diff > 1) || (diff < -1)){
			  if(encoder_burr.angle_pos > last_enc_pos){
				step_angle_enc+=((360-encoder_burr.angle_pos)+last_enc_pos);
			  }else{
			   step_angle_enc+=(last_enc_pos-encoder_burr.angle_pos);
			  }
		  //}
		  encoder_burr.micron_moved_pos=((step_angle_enc*BURR_1DEG_TO_STEPS)/(BURR_STEP_MICRON*BURR_DISTANCE_COEFF));
		  hopper_burr[BURR_0].burr_distance_rd +=(encoder_burr.micron_moved_pos);

     }else if(pos_sensor.reg_b.burr_near==GPIO_PIN_SET){

    	 BSP_MotorControl_Move(BURR_0,AVVICINA_MACINA,(step*BURR_DISTANCE_COEFF));//
		 do{
		    stepper_state=BSP_MotorControl_GetDeviceState(BURR_0);
		 }while(stepper_state != INACTIVE);
//		  pos = BSP_MotorControl_GetPosition(BURR_0);
//		  BSP_MotorControl_SetHome(BURR_0, pos);
//		  hopper_burr[BURR_0].step_pos=0;
		  BSP_MotorControl_CmdDisable(BURR_0);
		  hopper_burr[BURR_0].STOP_pin=GPIO_PIN_RESET;
		  read_encoder_pos();
		  //diff=(last_enc_pos - encoder_burr.angle_pos);
		  //if ((diff > 1) || (diff < -1)){
			  if((encoder_burr.angle_pos + 1) > last_enc_pos){
				step_angle_enc+=(encoder_burr.angle_pos-last_enc_pos);
			  }else{
				step_angle_enc+=(encoder_burr.angle_pos+(360-last_enc_pos));
			  }
		  //}
		  encoder_burr.micron_moved_pos=((step_angle_enc*BURR_1DEG_TO_STEPS)/(BURR_STEP_MICRON*BURR_DISTANCE_COEFF));
		  if (hopper_burr[BURR_0].burr_distance_rd>encoder_burr.micron_moved_pos){
		  hopper_burr[BURR_0].burr_distance_rd -=(encoder_burr.micron_moved_pos);
		  }else{
			  hopper_burr[BURR_0].burr_distance_rd=0;
		  }
     }

     do{
    	 read_single_sensor_hall(BURR_0);
     }while((pos_sensor.reg_b.burr_near==GPIO_PIN_SET)||(pos_sensor.reg_b.burr_far==GPIO_PIN_SET));

   	 hopper_burr[BURR_0].cmd=IDLE_STEPPER;

}



/**
  * @brief  This function is the User handle L6474 to drive Hopper and Burr stepper motor
  * @param  None
  * @retval None
  */
void hopper_burr_manager(void){

	 int32_t pos;
	 uint8_t id;
	 motorState_t status;
	 //uint16_t speed;

	 // stepper_test_SPI();
	 for (id=0;id<HOPPER_MAX;id++){
		if (hopper_burr[id].cmd >INIT_STEPPER)	{
			  hopper_burr[id].motor_status.status16t =BSP_MotorControl_ReadStatusRegister(id);
			  status=BSP_MotorControl_GetDeviceState(id);
			  if (status == INACTIVE){
				  hopper_burr[id].speed_rpm=0;
			  }else{//ACTIVE
				  hopper_burr[id].speed_rpm=BSP_MotorControl_GetCurrentSpeed(id);
			  }

		}
	  switch( hopper_burr[id].cmd){
	      case(IDLE_STEPPER):
	    		               break;
	      case(INIT_STEPPER):
		                       init_stepper();
	                           printf("\n\rINIT STEPPER Hop:%d",id);
	     	    		       break;
	      case(INIT_HOPPER1):
		                       stepper_find_zero(HOPPER_1);
	   	     	               break;
	      case(INIT_HOPPER2):
		                       stepper_find_zero(HOPPER_2);
	   	     	    		   break;
	      case(INIT_HOPPER3):
		                       stepper_find_zero(HOPPER_3);
	   	     	    		   break;
	      case(INIT_BURR):
		                       stepper_burr_find_zero(BURR_0);
	   	     	    		   break;

	      case(MOVE_STEPPER):
		                               stepper_stall_open(id,2,1500);
	     	    		               break;

	      case(OPEN_STEPPER):
	    		                       stepper_open_hopper(id,2,1500);
	     	    		               break;

	      case(BURR_POSITIONING_START_STEPPER):    stepper_burr_position();
	    		                                   break;
	      case(BURR_POSITIONING_END_STEPPER):
	   	    		                               break;

	      case(STOP_MOVE_STEPPER):
	     	    		               break;
	      case(ERR_STEPPER):
	     	    		               break;
	      case(HOMING_STEPPER):

	     	    		               break;
	      case(STALL_STEPPER):
	     	    		               break;
	      case(FINISH_MOVEMENT_STEPPER):
		                               stepper_homing(id);// stepper_homing_wait(id);
	     	    		               break;


	      case(SERVICE_CONTROL_STEPPER):
										 if (status == INACTIVE){
											 pos = BSP_MotorControl_GetPosition(id);
											 hopper_burr[id].step_pos=pos;
											 //printf("\n\r abs.pos: %lu",pos);
											 if (hopper_burr[id].step_from_home != hopper_burr[id].step_pos){
												  hopper_burr[id].STOP_pin=GPIO_PIN_RESET;
												  stepper_DIR(id,hopper_burr[id].DIR_pin);
												  BSP_MotorControl_GoTo(id,hopper_burr[id].step_from_home);
											 }
										 }

	    		                       break;
	      default:
	    	      break;
	  }

	 }//end_for

}





/**
  * @brief  This function is the User handler for the flag interrupt
  * @param  None
  * @retval None
  */
void MyFlagInterruptHandler(void)
{


  /* Get status of device 0 */
  /* this will clear the flags */
  uint16_t statusRegister = BSP_MotorControl_CmdGetStatus(0);

  //printf("\n\r HOPPER error: %d \n\r",statusRegister);
  /* Check HIZ flag: if set, power brigdes are disabled */
  if ((statusRegister & L6474_STATUS_HIZ) == L6474_STATUS_HIZ)
  {
    // HIZ state
    // Action to be customized
  }

  /* Check direction bit */
  if ((statusRegister & L6474_STATUS_DIR) == L6474_STATUS_DIR)
  {
    // Forward direction is set
    // Action to be customized
  }
  else
  {
    // Backward direction is set
    // Action to be customized
  }

  /* Check NOTPERF_CMD flag: if set, the command received by SPI can't be performed */
  /* This often occures when a command is sent to the L6474 */
  /* while it is in HIZ state */
  if ((statusRegister & L6474_STATUS_NOTPERF_CMD) == L6474_STATUS_NOTPERF_CMD)
  {
       // Command received by SPI can't be performed
       // Action to be customized
  }

  /* Check WRONG_CMD flag: if set, the command does not exist */
  if ((statusRegister & L6474_STATUS_WRONG_CMD) == L6474_STATUS_WRONG_CMD)
  {
     //command received by SPI does not exist
     // Action to be customized
  }

  /* Check UVLO flag: if not set, there is an undervoltage lock-out */
  if ((statusRegister & L6474_STATUS_UVLO) == 0)
  {
     //undervoltage lock-out
     // Action to be customized
  }

  /* Check TH_WRN flag: if not set, the thermal warning threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_WRN) == 0)
  {
    //thermal warning threshold is reached
    // Action to be customized
  }

  /* Check TH_SHD flag: if not set, the thermal shut down threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_SD) == 0)
  {
    //thermal shut down threshold is reached
    // Action to be customized
  }

  /* Check OCD  flag: if not set, there is an overcurrent detection */
  if ((statusRegister & L6474_STATUS_OCD) == 0)
  {
    //overcurrent detection
    // Action to be customized
  }

  /* Get status of device 1 */
  /* this will clear the flags */
  statusRegister = BSP_MotorControl_CmdGetStatus(1);

  /* Check HIZ flag: if set, power brigdes are disabled */
  if ((statusRegister & L6474_STATUS_HIZ) == L6474_STATUS_HIZ)
  {
    // HIZ state
    // Action to be customized
  }

  /* Check direction bit */
  if ((statusRegister & L6474_STATUS_DIR) == L6474_STATUS_DIR)
  {
    // Forward direction is set
    // Action to be customized
  }
  else
  {
    // Backward direction is set
    // Action to be customized
  }

  /* Check NOTPERF_CMD flag: if set, the command received by SPI can't be performed */
  /* This often occures when a command is sent to the L6474 */
  /* while it is in HIZ state */
  if ((statusRegister & L6474_STATUS_NOTPERF_CMD) == L6474_STATUS_NOTPERF_CMD)
  {
       // Command received by SPI can't be performed
       // Action to be customized
  }

  /* Check WRONG_CMD flag: if set, the command does not exist */
  if ((statusRegister & L6474_STATUS_WRONG_CMD) == L6474_STATUS_WRONG_CMD)
  {
     //command received by SPI does not exist
     // Action to be customized
  }

  /* Check UVLO flag: if not set, there is an undervoltage lock-out */
  if ((statusRegister & L6474_STATUS_UVLO) == 0)
  {
     //undervoltage lock-out
     // Action to be customized
  }

  /* Check TH_WRN flag: if not set, the thermal warning threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_WRN) == 0)
  {
    //thermal warning threshold is reached
    // Action to be customized
  }

  /* Check TH_SHD flag: if not set, the thermal shut down threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_SD) == 0)
  {
    //thermal shut down threshold is reached
    // Action to be customized
  }

  /* Check OCD  flag: if not set, there is an overcurrent detection */
  if ((statusRegister & L6474_STATUS_OCD) == 0)
  {
    //overcurrent detection
    // Action to be customized
  }

  /* Get status of device 2 */
  /* this will clear the flags */
  statusRegister = BSP_MotorControl_CmdGetStatus(2);

  /* Check HIZ flag: if set, power brigdes are disabled */
  if ((statusRegister & L6474_STATUS_HIZ) == L6474_STATUS_HIZ)
  {
    // HIZ state
    // Action to be customized
  }

  /* Check direction bit */
  if ((statusRegister & L6474_STATUS_DIR) == L6474_STATUS_DIR)
  {
    // Forward direction is set
    // Action to be customized
  }
  else
  {
    // Backward direction is set
    // Action to be customized
  }

  /* Check NOTPERF_CMD flag: if set, the command received by SPI can't be performed */
  /* This often occures when a command is sent to the L6474 */
  /* while it is in HIZ state */
  if ((statusRegister & L6474_STATUS_NOTPERF_CMD) == L6474_STATUS_NOTPERF_CMD)
  {
       // Command received by SPI can't be performed
       // Action to be customized
  }

  /* Check WRONG_CMD flag: if set, the command does not exist */
  if ((statusRegister & L6474_STATUS_WRONG_CMD) == L6474_STATUS_WRONG_CMD)
  {
     //command received by SPI does not exist
     // Action to be customized
  }

  /* Check UVLO flag: if not set, there is an undervoltage lock-out */
  if ((statusRegister & L6474_STATUS_UVLO) == 0)
  {
     //undervoltage lock-out
     // Action to be customized
  }

  /* Check TH_WRN flag: if not set, the thermal warning threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_WRN) == 0)
  {
    //thermal warning threshold is reached
    // Action to be customized
  }

  /* Check TH_SHD flag: if not set, the thermal shut down threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_SD) == 0)
  {
    //thermal shut down threshold is reached
    // Action to be customized
  }

  /* Check OCD  flag: if not set, there is an overcurrent detection */
  if ((statusRegister & L6474_STATUS_OCD) == 0)
  {
    //overcurrent detection
    // Action to be customized
  }

}

