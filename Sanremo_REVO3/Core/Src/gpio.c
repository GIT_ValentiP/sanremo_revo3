/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
#include "globals.h"
/* USER CODE BEGIN 0 */
extern TIM_HandleTypeDef htim4;
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PB12   ------> USB_OTG_HS_ID
     PB13   ------> USB_OTG_HS_VBUS
     PB14   ------> USB_OTG_HS_DM
     PB15   ------> USB_OTG_HS_DP
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, DIR_DOSER1_Pin|LED5_SERVICE_Pin|LED4_SERVICE_Pin
                          |CTRL_OUT_P_FILTRO_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(GPIOE, SPI4_NSS_DOSER2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, POWER_FAN2_Pin|POWER_FAN1_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(GPIOC, SPI4_NSS_DOSER3_Pin|SPI4_NSS_VIBRO_Pin |SPI1_NSS_STEPPER2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, DIR_BURR_STEPPER4_Pin|STBY_RST_BURR_STEPPER4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, RS485_CELLA_CTS_Pin|RS485_CELLA_RTS_Pin|RS485_OPTO_MACINA_CTS_Pin|RS485_OPTO_MACINA_RTS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, SPI1_NSS_STEPPER4_Pin|STBY_RST_STEPPER3_Pin
                          |STCK_STEPPER3_Pin|DIR_STEPPER3_Pin|RS485_HMI_RTS_Pin|STBY_RST_STEPPER1_Pin 
                          |STCK_STEPPER1_Pin|DIR_STEPPER1_Pin|RS485_HMI_CTS_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(GPIOG, SPI1_NSS_STEPPER4_Pin, GPIO_PIN_SET);

  HAL_GPIO_WritePin(GPIOG, DIS_DOSER1_Pin|DIS_VIBRO_Pin|DIS_DOSER3_Pin , GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, DIR_DOSER2_Pin|DIS_DOSER2_Pin|DIR_DOSER3_Pin|RS232_DBG_CTS_Pin 
                          |RS232_DBG_RTS_Pin|DIR_VIBRO_Pin|STBY_RST_STEPPER2_Pin|STCK_STEPPER2_Pin 
                          |DIR_STEPPER2_Pin|LED3_SERVICE_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(GPIOD, DIS_DOSER2_Pin|SPI1_NSS_STEPPER3_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PEPin PEPin PEPin PEPin 
                           PEPin */
  GPIO_InitStruct.Pin = SPI4_NSS_DOSER2_Pin|DIR_DOSER1_Pin|LED5_SERVICE_Pin|LED4_SERVICE_Pin
                          |CTRL_OUT_P_FILTRO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PEPin PEPin PEPin */
   GPIO_InitStruct.Pin =  LED6_SERVICE_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin PCPin 
                           PCPin */
  GPIO_InitStruct.Pin = SPI4_NSS_DOSER3_Pin|POWER_FAN2_Pin|POWER_FAN1_Pin|SPI4_NSS_VIBRO_Pin 
                          |SPI1_NSS_STEPPER2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PFPin PFPin PFPin PFPin 
                           PFPin PFPin PFPin PFPin 
                           PFPin */
  GPIO_InitStruct.Pin = MICRO_DOSER1_Pin|MICRO_DOSER2_Pin|MICRO_DOSER3_Pin|SYNC_BURR_STEPPER4_Pin 
                          |SENS_CLOSE_HALL1_Pin|SENS_OPEN_HALL2_Pin|SENS_CLOSE_HALL2_Pin|SENS_OPEN_HALL3_Pin 
                          |SENS_CLOSE_HALL3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PFPin PFPin */
  GPIO_InitStruct.Pin = DIR_BURR_STEPPER4_Pin|STBY_RST_BURR_STEPPER4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin */
  GPIO_InitStruct.Pin = RS485_CELLA_CTS_Pin|RS485_CELLA_RTS_Pin|RS485_OPTO_MACINA_CTS_Pin|RS485_OPTO_MACINA_RTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = SENS_OPEN_HALL1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SENS_OPEN_HALL1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PGPin PGPin PGPin PGPin 
                           PGPin PGPin PGPin PGPin 
                           PGPin PGPin PGPin PGPin 
                           PGPin */
  GPIO_InitStruct.Pin = SPI1_NSS_STEPPER4_Pin|DIS_DOSER1_Pin|DIS_VIBRO_Pin|STBY_RST_STEPPER3_Pin
                          |STCK_STEPPER3_Pin|DIR_STEPPER3_Pin|RS485_HMI_RTS_Pin|STBY_RST_STEPPER1_Pin 
                          |STCK_STEPPER1_Pin|DIR_STEPPER1_Pin|RS485_HMI_CTS_Pin|DIS_DOSER3_Pin ;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = CTRL_IN_P_FILTRO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CTRL_IN_P_FILTRO_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB12 PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_OTG_HS_FS;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PDPin PDPin PDPin PDPin 
                           PDPin PDPin PDPin PDPin 
                           PDPin PDPin PDPin */
  GPIO_InitStruct.Pin = DIR_DOSER2_Pin|DIS_DOSER2_Pin|DIR_DOSER3_Pin|RS232_DBG_CTS_Pin 
                          |RS232_DBG_RTS_Pin|DIR_VIBRO_Pin|STBY_RST_STEPPER2_Pin|STCK_STEPPER2_Pin 
                          |DIR_STEPPER2_Pin|LED3_SERVICE_Pin|SPI1_NSS_STEPPER3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = SYNC_STEPPER3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SYNC_STEPPER3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PGPin PGPin */
  GPIO_InitStruct.Pin = FLAG_STEPPER3_Pin|FLAG_STEPPER1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = SYNC_STEPPER2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SYNC_STEPPER2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = FLAG_STEPPER2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(FLAG_STEPPER2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = SYNC_STEPPER1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SYNC_STEPPER1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
//CONFIGURATION as LED
//  GPIO_InitStruct.Pin = LED1_SERVICE_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//  HAL_GPIO_Init(LED1_SERVICE_GPIO_Port, &GPIO_InitStruct);
/* CONFIGURATION as BUTTON BURR */
   GPIO_InitStruct.Pin = LED1_SERVICE_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   HAL_GPIO_Init(LED1_SERVICE_GPIO_Port, &GPIO_InitStruct);

/*CONFIGURATION as LED*/
//  GPIO_InitStruct.Pin = LED2_SERVICE_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//  HAL_GPIO_Init(LED2_SERVICE_GPIO_Port, &GPIO_InitStruct);
/*CONFIGURATION as BUTTON BURR*/
  GPIO_InitStruct.Pin = LED2_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LED2_SERVICE_GPIO_Port, &GPIO_InitStruct);


/* SPI2 INPUTs*/

//     GPIO_InitStruct.Pin = SPI2_MOSI_ENCODER_MAC_Pin|SPI2_MISO_ENCODER_MAC_Pin;
//     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//     GPIO_InitStruct.Pull = GPIO_NOPULL;
//     HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
//
//     GPIO_InitStruct.Pin = SPI2_CLK_ENCODER_MAC_Pin;
//     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//     GPIO_InitStruct.Pull = GPIO_NOPULL;
//     HAL_GPIO_Init(SPI2_CLK_ENCODER_MAC_GPIO_Port, &GPIO_InitStruct);
//
//     GPIO_InitStruct.Pin = SPI2_NSS_ENCODER_MAC_Pin;
//     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//     GPIO_InitStruct.Pull = GPIO_NOPULL;
//     HAL_GPIO_Init(SPI2_NSS_ENCODER_MAC_GPIO_Port, &GPIO_InitStruct);

/*UART8-> LED_9 LED_8*/
  //HAL_GPIO_WritePin(GPIOE, UART8_RX_SERVICE_Pin|UART8_TX_SERVICE_Pin, GPIO_PIN_RESET);
  GPIO_InitStruct.Pin = UART8_RX_SERVICE_Pin|UART8_TX_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(UART8_TX_SERVICE_GPIO_Port, &GPIO_InitStruct);


}

/* USER CODE BEGIN 2 */
void led_service_init(void){



//	HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, GPIO_PIN_SET);

//	HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, GPIO_PIN_SET);

//	HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, GPIO_PIN_RESET);

    HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, GPIO_PIN_RESET);


    /*ILUUMINA Led stripe*/

    htim4.Instance->ARR=990;
    htim4.Init.Period  =999;
   	htim4.Instance->CCR2=0;
    illuminazione.pwm_duty=65;
    illuminazione.cmd.b.DUTY=1;

}

void led_service_ON_OFF(unsigned char id_led,unsigned char status){
 GPIO_PinState set_status_led;
 set_status_led=(status>0 ?GPIO_PIN_SET: GPIO_PIN_RESET);
 if (id_led < 10){
	 switch	(id_led){
		 case(1):
						//HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, set_status_led);
						break;
		 case(2):
						//HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, set_status_led);
						break;
		 case(3):
						HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, set_status_led);
						break;
		 case(4):
						HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, set_status_led);
						break;
		 case(5):
						HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, set_status_led);
						break;
		 case(6):
						HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, set_status_led);
				    	break;
//		 case(7):
//						HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, set_status_led);
//						break;
		 case(8):
						HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, set_status_led);
						break;
		 case(9):
						HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, set_status_led);
						break;
	 }
 }



}


void illumina_manager(void){

		if (illuminazione.cmd.b.DUTY!=0){//new duty cycle for PWM ILLUMINA
			uint32_t duty_cycle=100;
			uint32_t period_pwm;
			if (illuminazione.pwm_duty <100){
			   duty_cycle=illuminazione.pwm_duty;
			}
            period_pwm = (htim4.Init.Period+1);
			duty_cycle =((duty_cycle*period_pwm)/100);
			htim4.Instance->CCR2=(duty_cycle);

			illuminazione.cmd.b.DUTY=0;
		}

}

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
