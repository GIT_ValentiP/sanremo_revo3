/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "crc.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "iwdg.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_otg.h"
#include "gpio.h"
#include "globals.h"
#include "scheduler_main.h"
#include "error.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mdb_com.h"
#include "ifx9201sg_doser.h"
#include "fan.h"
#include "st_l6474_stepper.h"
#include "balance.h"
#include "grinder.h"
#include "sts3x.h"
#include "sht3x.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
//__attribute__((section(".projectvars"))) const uint32_t  VERSION_NUMBER = ((REVO_FW_VERSION<<8)| REVO_FW_SUBVERSION);
//__attribute__((section(".projectvars"))) const uint32_t  CRC_PROGRAM= 0x000000000;
//__attribute__((section(".projectvars"))) const uint16_t  BUILD_ID=0xA5A5;
//__attribute__((section(".projectvars"))) const  uint8_t  OTHER_VAR=0x00;
#define FLASH_APP_ADDRESS 0x8040000
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static volatile uint16_t gLastError;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
void CPU_CACHE_Disable(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	HAL_DeInit();
	SCB->VTOR = FLASH_APP_ADDRESS; /* Vector Table Relocation in Internal FLASH */
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

	/* Disable the CPU Cache */
	 // CPU_CACHE_Disable();

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
 // MX_ADC1_Init();

//  MX_ADC3_Init();
  MX_I2C1_Init();
//  MX_I2C2_Init();
//  MX_I2C3_Init();
  MX_SPI1_Init(); //SPI STEPPER1-2-3 and BURR , for HOPPER and BURR
  MX_SPI2_Init(); //ENCODER STEPPER BURR
  MX_SPI4_Init();
  MX_TIM1_Init();//PWM CH1-CH2 FANs timer
  MX_TIM3_Init();//PWM CH2-CH3-CH4 DOSER timer
  MX_TIM5_Init();//PWM NO-Output stepper - burr
  MX_TIM4_Init();//PWM CH3 Vibro Timer/CH2 Illuminazione Led
//  MX_UART8_Init();
  MX_USART1_UART_Init();
//  MX_USART2_UART_Init(9600,UART_PARITY_NONE);//MX_USART2_UART_Init();
  MX_USART3_UART_Init();
//  MX_USART6_UART_Init();
//  MX_USB_OTG_HS_USB_Init();
//  MX_CRC_Init();
//  MX_IWDG_Init();
//  MX_RTC_Init();
 // MX_TIM6_Init();
//  MX_TIM7_Init();
//  MX_TIM10_Init();
  MX_TIM11_Init(); //BURR stepper STK PWM timer
  MX_TIM13_Init();  //stepper STCK software pwm timer
  MX_TIM14_Init();  //base system clock timer
 // MX_FATFS_Init();

//  HAL_UART_MspInit(&huart1);//INVERTER -GRINDER RS485 Half-Duplex Communication
//  HAL_UART_MspInit(&huart2);//BALANCE RS485 Full-Duplex Communication
//  HAL_UART_MspInit(&huart3);//DEBUG RS232 Monitor
//  HAL_UART_MspInit(&huart6); //HMI RS485 Half-Duplex communication



  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */


 /*LED Init*/
  led_service_init();

 /*TIMERs Init*/
  TIM_PWM_Init();
  Main_Clock_Init();
  HAL_TIM_Base_MspInit(&htim14);
  HAL_TIM_Base_MspInit(&htim13);
  flag_1min=0;

  /*MODBUS HMI Init*/
  version_fw[0]= ((REVO_FW_VERSION<<8)| REVO_FW_SUBVERSION);//VERSION_NUMBER;
  version_fw[1]=0x0115;//FW version INVERTER
  version_fw[2]=0;//(0x01<<8)+0x03;//FW version LOAD CELL HBM
  printf("\n\r *** REVO3 FW Version: %3d.%3d ***\n\r",REVO_FW_VERSION,REVO_FW_SUBVERSION);
  DP_SW_RS485_adr=REVO3_HMI_RS485_ADDR;//address  to identify OPERA Keyboard on ModBus
  MX_USART6_UART_Init();
  mdb_serial_setSlaveAddress(DP_SW_RS485_adr);
  mdb_comm_Initialize(); //Modbus Communication Init
  RS485_TX_HMI_DISABLE;
  RS485_RX_HMI_ENABLE;
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0); //HMI UART6 RX DMA
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
 // HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);//HMI UART6 TX DMA
 // HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
  /* USART6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART6_IRQn);


  /* Initialize ADC1 in DMA Mode for NTC temp sensor */
  adc_init();

  /*DOSER and VIBRO Init */
  init_doser_vibro();

  /*FAN1-2 Init*/
  init_fan();

  /*HOPPER and BURR Stepper Init*/
  //revo_state=REVO_SELF_TEST_RUN;
  //init_stepper();
  init_encoder_burr();
  /*BALANCE LOAD Cell Init*/
  init_balance();

  /*GRINDER Init*/
  init_grinder();

  /* Initialize External Sensor Temperature Humidity Sensirion SHT3x on I2C bus */
  sht3x_init();

  /* Initialize Internal Sensor Temperature Sensirion STS3x on I2C bus */
  sts3x_init();



  /*Main status*/
  revo_op_mode.mode_op_reg=0x0001;
  revo_board_conf =REVO_NOT_SET_CONFIG;
  revo_state=REVO_IDLE;//REVO_SELF_TEST_RUN;//
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {

	//printf("\n\rSTART MAIN LOOP");
    read_sensor_hall();
    //printf("\n\rRead sensor");
    mdb_comm_processMessage();/*Modbus poll update in each run*/
    //printf("\n\rModbus processed");
    scheduler_task();
    //printf("\n\rMain scheduler Task");

    if(revo_board_conf !=REVO_NOT_SET_CONFIG){//(revo_board_conf !=REVO_NOT_SET_CONFIG){//(revo_state>REVO_IDLE)
     //printf("\n\rPower Board configured:load manager");
     balance_weight_manager();
	 //printf("\n\rBalance:load cell");
	 doser_vibro_manager();
	 //printf("\n\rDoser and Vibro");
	 fan_manager();
	 //printf("\n\rFan");
     hopper_burr_manager();
     //printf("\n\rStepper:hopper and burr");
	 grinder_manager();
	 //printf("\n\rGrinder");

   }

    illumina_manager();

    led_service_ON_OFF(1,pos_sensor.reg_b.hop1_close);//TOGGLE_PIN_LED);
    led_service_ON_OFF(2,pos_sensor.reg_b.hop2_close);//TOGGLE_PIN_LED);
    led_service_ON_OFF(3,pos_sensor.reg_b.hop3_close);//TOGGLE_PIN_LED);
    led_service_ON_OFF(9,pos_sensor.reg_b.portafilter);
    //printf("\n\rLeds:service and strip set");

    //if(revo_state==REVO_READY_TO_USE){//(revo_board_conf !=REVO_NOT_SET_CONFIG){
	  if (flag_1sec==1){
			read_encoder_pos();
			printf("\n\r BurrAngle :%d� Zero:%d�",encoder_burr.angle_pos,encoder_burr.angle_zero_pos);
			printf("\n\r Burr Pos.:%d[um] Delta Pos.:%d[um]",hopper_burr[BURR_0].burr_distance_rd,encoder_burr.micron_moved_pos);//encoder_burr.pre_grind_pos);
			sensor_sts3x_update();
			sensor_sht3x_update();
			adc_update();
			//printf("NTC Temp.:%d�C \n",temp_sens[NTC_TEMP_SENS].temperature);//printf("NTC Temp.:%d�C NTC_ADC:%4ld mV \n",temp_sens[NTC_TEMP_SENS].temperature,adc123_ch[NTC_ADC1_CH13].millivolt);
			printf("Status:%d Command:%d \n",revo_state,revo_op_cmd.cmd_reg16);
			//printf("VREF:%4ld mV \n",adc123_ch[VREF_ADC1_CH17].millivolt);
			//printf("ENC_ADC:%4ld mV \n",adc123_ch[ENC_ADC1_CH10].millivolt);
			//printf("FAN2:%d \n",fan[FAN_2].pwm_duty[0]);
			error_print();
			led_service_ON_OFF(6,TOGGLE_PIN_LED);
			flag_1sec=0;
	  }
    //}


   //led_service_ON_OFF(4,TOGGLE_PIN_LED);
   //printf("\n\rEND MAIN LOOP");

   // led_service_ON_OFF(5,TOGGLE_PIN_LED);  // HAL_GPIO_TogglePin(LED5_SERVICE_GPIO_Port , LED5_SERVICE_Pin);//led_service_ON_OFF(5,TOGGLE_PIN_LED);
   //led_service_ON_OFF(6,TOGGLE_PIN_LED);
   /* USER CODE END WHILE */
   /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler(SYSTEM_RCC_CONFIG_ERROR);
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler(SYSTEM_RCC_OVER_DRIVE_ERROR);
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler(SYSTEM_RCC_CLK_CONFIG_ERROR);
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3
                              |RCC_PERIPHCLK_USART6|RCC_PERIPHCLK_UART8
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C2
                              |RCC_PERIPHCLK_I2C3|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Uart8ClockSelection = RCC_UART8CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler(SYSTEM_RCC_PERIPH_CLK_CONFIG_ERROR);
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
 /* DMA1_Stream3_IRQn interrupt configuration */
// HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 5, 0); //DEBUG RS232 DMA TX
//  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA1_Stream5_IRQn interrupt configuration */
// HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 1, 0);//BALANCE RS422 DMA RX
// HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA2_Stream0_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 7, 0);
//  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream1_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0); //HMI UART6 RX DMA
//  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);
  /* DMA2_Stream2_IRQn interrupt configuration */ //INVERTER UART1 RX
//  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 4, 0);
//  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
  /* DMA2_Stream1_IRQn interrupt configuration */
 //   HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);//HMI UART6 TX DMA
 //   HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
  /* DMA2_Stream7_IRQn interrupt configuration */ //INVERTER UART1 TX
//  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 4, 0);
//  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);
  /* EXTI0_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  /* EXTI4_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(EXTI4_IRQn);
  /* EXTI9_5_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
  /* RCC_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(RCC_IRQn);
  /* ADC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(ADC_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(ADC_IRQn);
  /* TIM1_UP_TIM10_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 2, 0);
//  HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
  /* TIM1_TRG_COM_TIM11_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM11_IRQn);

  /* TIM1_CC_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM1_CC_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);

  /* TIM3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM3_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(TIM3_IRQn);
  /* TIM4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM4_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(TIM4_IRQn);
  /* TIM4_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(TIM5_IRQn, 2, 0);
   HAL_NVIC_EnableIRQ(TIM5_IRQn);
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(SPI1_IRQn);
  /* SPI2_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(SPI2_IRQn, 3, 0); //ENCODER STEPPER MACINA
//  HAL_NVIC_EnableIRQ(SPI2_IRQn);
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USART6_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(USART6_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(USART6_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USART3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_IRQn, 10, 0);
  HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* SPI4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI4_IRQn, 9, 0);
  HAL_NVIC_EnableIRQ(SPI4_IRQn);
  /* TIM8_UP_TIM13_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 5, 0);
//  HAL_NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
  /* TIM8_TRG_COM_TIM14_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM8_TRG_COM_TIM14_IRQn, 0, 2);
  HAL_NVIC_EnableIRQ(TIM8_TRG_COM_TIM14_IRQn);

  /* TIM6_DAC_IRQn interrupt configuration */
 // HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);

  /* TIM7_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(TIM7_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM7_IRQn);

  /* UART8_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(UART8_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(UART8_IRQn);
}

/* USER CODE BEGIN 4 */
void CPU_CACHE_Disable(void){

	 /* Disable I-Cache */
	 //SCB_CleanInvalidateDCache();//SCB_DisableICache();

	  /* Disable D-Cache */
	  SCB_DisableDCache();
}
/* USER CODE END 4 */


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  error number of the error
  * @retval None
  */
void Error_Handler(uint16_t error)
{
  /* Backup error number */
  gLastError = error;
  printf("\n\r Error:%d \n\r",error);
  /* Infinite loop */
  while(1)
  {
  }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
