/*
 ******************************************************************************
 *  @file      : ifx9201sg_doser.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 20 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "ifx9201sg_doser.h"
#include "tim.h"
#include "spi.h"
#include "usart.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define TIMER_PWM_CLOCK_FREQ  1000000  //freq timer clock in Hz
#define TIMER_PWM_MAX_FREQ     30000//
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

extern SPI_HandleTypeDef hspi4;
/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

void set_new_freq_doser(doser_enum_t  id_doser,uint16_t nfreq_hz ){
    //  To get TIM3 output clock at 27 KHz, the period (ARR)) is computed as follows:

	uint32_t freq_set;
    //ARR = (TIMx counter clock / TIMx output clock) - 1  = 665
    if (nfreq_hz>TIMER_PWM_MAX_FREQ){
    	freq_set=TIMER_PWM_CLOCK_FREQ/TIMER_PWM_MAX_FREQ;
    }else{
    	freq_set=TIMER_PWM_CLOCK_FREQ/nfreq_hz;

    }
    //printf("DOSER_%1d  pwm freq:%d Hz \n\r",id_doser,nfreq_hz);
	switch (id_doser){
			case(DOSER_1)://TIM3 channel 2
					          htim3.Instance->ARR=freq_set-1;
			                  htim3.Init.Period  =freq_set-1;
						      break;
			case(DOSER_2)://TIM3 channel 3
		                      htim3.Instance->ARR=freq_set-1;
			                  htim3.Init.Period  =freq_set-1;
							  break;
			case(DOSER_3)://TIM3 channel 4
		                      htim3.Instance->ARR=freq_set-1;
			                  htim3.Init.Period  =freq_set-1;
							  break;
			case(VIBRO_0)://TIM4 channel 3
		                      htim4.Instance->ARR=freq_set-1;
			                  htim4.Init.Period  =freq_set-1;
							  break;
			default:
					          break;

	}


}



void set_new_duty_doser(doser_enum_t  id_doser,uint16_t nduty_x100){
//duty cycle = (TIMx_CCRY/ TIMx_ARR + 1)* 100 = 50%  y :channel x: id timer

 uint32_t duty_cycle=100;
 uint32_t period_pwm;
	if (nduty_x100 <100){
	   duty_cycle=nduty_x100;
	}

	switch (id_doser){
				case(DOSER_1):
		                          period_pwm = (htim3.Init.Period+1);
				                  duty_cycle =((duty_cycle*period_pwm)/100);
						          htim3.Instance->CCR2=(duty_cycle);
							      break;
				case(DOSER_2):
		                          period_pwm = (htim3.Init.Period+1);
				                  duty_cycle =((duty_cycle*period_pwm)/100);
		                          htim3.Instance->CCR3=(duty_cycle);
								  break;
				case(DOSER_3):
		                          period_pwm = (htim3.Init.Period+1);
				                  duty_cycle =((duty_cycle*period_pwm)/100);
		                          htim3.Instance->CCR4=(duty_cycle);
								  break;
				case(VIBRO_0):
		                          period_pwm = (htim4.Init.Period+1);
				                  duty_cycle =((duty_cycle*period_pwm)/100);
		                          htim4.Instance->CCR3=(duty_cycle);
								  break;
				default:
						          break;

		}

	//printf("DOSER_%1d  period:%d ;pwm duty:%d \n\r",id_doser,(uint16_t)period_pwm ,(uint16_t)duty_cycle);
}
void set_new_dir_doser(doser_enum_t  id_doser,doser_dir_enum_t ndir){
	//printf("DOSER_%1d  DIRection:%d \n\r",id_doser,ndir);
	switch (id_doser){
				case(DOSER_1):
		                          HAL_GPIO_WritePin(DIR_DOSER1_GPIO_Port, DIR_DOSER1_Pin, ndir);
							      break;
				case(DOSER_2):
		                          HAL_GPIO_WritePin(DIR_DOSER2_GPIO_Port, DIR_DOSER2_Pin, ndir);
								  break;
				case(DOSER_3):
		                          HAL_GPIO_WritePin(DIR_DOSER3_GPIO_Port, DIR_DOSER3_Pin, ndir);
								  break;
				case(VIBRO_0):
		                          HAL_GPIO_WritePin(DIR_VIBRO_GPIO_Port, DIR_VIBRO_Pin, ndir);
								  break;
				default:
						          break;

		}

}

void stop_start_doser(doser_enum_t  id_doser,doser_dis_enum_t ndis){
	// printf("DOSER_%1d  STOP:%2d \n\r",id_doser,ndis);
	switch (id_doser){
				case(DOSER_1):
						          if (ndis== STOP_MOT){
						        	   HAL_TIM_PWM_Stop(&htim3,PWM_DOSER1_CHANNEL);

						          }else{
						        	    HAL_TIM_PWM_Start(&htim3,PWM_DOSER1_CHANNEL);

						          }
		                          HAL_GPIO_WritePin(DIS_DOSER1_GPIO_Port, DIS_DOSER1_Pin, ndis);
								  break;
				case(DOSER_2):
		                          if (ndis== STOP_MOT){
										HAL_TIM_PWM_Stop(&htim3,PWM_DOSER2_CHANNEL);
								  }else{
										HAL_TIM_PWM_Start(&htim3,PWM_DOSER2_CHANNEL);
								  }
								  HAL_GPIO_WritePin(DIS_DOSER2_GPIO_Port, DIS_DOSER2_Pin, ndis);
								  break;
				case(DOSER_3):
		                          if (ndis== STOP_MOT){
									    HAL_TIM_PWM_Stop(&htim3,PWM_DOSER3_CHANNEL);
								  }else{
										HAL_TIM_PWM_Start(&htim3,PWM_DOSER3_CHANNEL);
								  }
								  HAL_GPIO_WritePin(DIS_DOSER3_GPIO_Port, DIS_DOSER3_Pin, ndis);
								  break;
				case(VIBRO_0):
		                          if (ndis== STOP_MOT){
									    HAL_TIM_PWM_Stop(&htim4,PWM_VIBRO_CHANNEL);
								  }else{
										HAL_TIM_PWM_Start(&htim4,PWM_VIBRO_CHANNEL);
								  }
								  HAL_GPIO_WritePin(DIS_VIBRO_GPIO_Port, DIS_VIBRO_Pin, ndis);
								  break;
				default:
						          break;

		}

}
void  read_diagnosis_doser(doser_enum_t  id_doser){

    //doser_DIA_t rd_diag_spi;
    uint8_t SendData;
    uint8_t ReadData;

    SendData = IFX9201_RD_DIA;
 	//SPI_MASTER_Transfer(&SPI, &SendData, &ReadData, 1); // send command RD_DIA
    	//SPI_MASTER_Transfer(&SPI, &SendData, &ReadData, 1); // get answer


    switch (id_doser){
    				case(DOSER_1):
									  HAL_GPIO_WritePin(SPI4_NSS_DOSER1_GPIO_Port, SPI4_NSS_DOSER1_Pin, GPIO_PIN_RESET);
									  HAL_SPI_Transmit(&hspi4, &SendData,1, 100);
									  HAL_GPIO_WritePin(SPI4_NSS_DOSER1_GPIO_Port, SPI4_NSS_DOSER1_Pin, GPIO_PIN_SET);
									  HAL_GPIO_WritePin(SPI4_NSS_DOSER1_GPIO_Port, SPI4_NSS_DOSER1_Pin, GPIO_PIN_RESET);
									  HAL_SPI_Receive(&hspi4, &ReadData,1, 100);//(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
									  HAL_GPIO_WritePin(SPI4_NSS_DOSER1_GPIO_Port, SPI4_NSS_DOSER1_Pin, GPIO_PIN_SET);
    							      break;
    				case(DOSER_2):
									HAL_GPIO_WritePin(SPI4_NSS_DOSER2_GPIO_Port, SPI4_NSS_DOSER2_Pin, GPIO_PIN_RESET);
									HAL_SPI_Transmit(&hspi4, &SendData,1, 100);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER2_GPIO_Port, SPI4_NSS_DOSER2_Pin, GPIO_PIN_SET);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER2_GPIO_Port, SPI4_NSS_DOSER2_Pin, GPIO_PIN_RESET);
									HAL_SPI_Receive(&hspi4, &ReadData,1, 100);//(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER2_GPIO_Port, SPI4_NSS_DOSER2_Pin, GPIO_PIN_SET);
    								  break;
    				case(DOSER_3):
									HAL_GPIO_WritePin(SPI4_NSS_DOSER3_GPIO_Port, SPI4_NSS_DOSER3_Pin, GPIO_PIN_RESET);
									HAL_SPI_Transmit(&hspi4, &SendData,1, 100);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER3_GPIO_Port, SPI4_NSS_DOSER3_Pin, GPIO_PIN_SET);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER3_GPIO_Port, SPI4_NSS_DOSER3_Pin, GPIO_PIN_RESET);
									HAL_SPI_Receive(&hspi4, &ReadData,1, 100);//(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
									HAL_GPIO_WritePin(SPI4_NSS_DOSER3_GPIO_Port, SPI4_NSS_DOSER3_Pin, GPIO_PIN_SET);

    								  break;
    				case(VIBRO_0):
									HAL_GPIO_WritePin(SPI4_NSS_VIBRO_GPIO_Port, SPI4_NSS_VIBRO_Pin, GPIO_PIN_RESET);
									HAL_SPI_Transmit(&hspi4, &SendData,1, 100);
									HAL_GPIO_WritePin(SPI4_NSS_VIBRO_GPIO_Port, SPI4_NSS_VIBRO_Pin, GPIO_PIN_SET);
									HAL_GPIO_WritePin(SPI4_NSS_VIBRO_GPIO_Port, SPI4_NSS_VIBRO_Pin, GPIO_PIN_RESET);
									HAL_SPI_Receive(&hspi4, &ReadData,1, 100);//(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
									HAL_GPIO_WritePin(SPI4_NSS_VIBRO_GPIO_Port, SPI4_NSS_VIBRO_Pin, GPIO_PIN_SET);


    								  break;
    				default:
    						          break;

    		}

        //printf("Doser DIAGNOSIS REG. 0x%X  \n\r",ReadData);
//        rd_diag_spi.all=ReadData;
    	if(ReadData == 0xDF)
    	{
    		// printf("no failure)\r\n");
    	}
//    	else
//    	{
//    		if(!( rd_diag_spi.bit.EN)) // low active /EN = DIS
//    			//printf("DOSER OFF,");
//    		if(!(rd_diag_spi.bit.OT)) // low active
//    			//printf("DOSER OT, ");
//    		if(!(rd_diag_spi.bit.CL)) // low active
//    			//printf("DOSER CL, ");
//
//    		//ReadData &= DIA_BITS; // diagnosis bits only
//
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_GND_OUT1_ERR)
//    			//printf("DOSER ERR. SCG1, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_VS_OUT1_ERR)
//    			//printf("DOSER ERR. SCVS1, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_OPEN_LOAD_ERR)
//    			//printf("DOSER ERR. OL, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_GND_OUT2_ERR)
//    			//printf("DOSER ERR. SCG2, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_GND_OUT1_2_ERR)
//    			//printf("DOSER ERR. SCG1, SCG2, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_GND_OUT2_VS_OUT1_ERR)
//    			//printf("DOSER ERR. SCVS1, SCG2, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_VS_OUT2_ERR)
//    			//printf("DOSER ERR. SCVS2, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_SHORT_VS_OUT1_2_ERR)
//    			//printf("DOSER ERR. SCVS1, SCVS2, ");
//    		if(rd_diag_spi.bit.DIA_id == DOSER_VS_UNDERV_ERR)
//    			//printf("DOSER ERR. UV, ");
//
//    		//printf("\r\n"); //delete last comma and close bracket
 //   	}
}
void read_control_doser(doser_enum_t  id_doser){
	//doser_CTRL_t  rd_ctrl_spi;


	switch (id_doser){
				case(DOSER_1):
							      break;
				case(DOSER_2):
								  break;
				case(DOSER_3):
								  break;
				case(VIBRO_0):
								  break;
				default:
						          break;

		}

 // return (rd_diag_spi);
}


void  set_control_doser(doser_enum_t  id_doser , uint8_t cmd_ctrl){
		switch (id_doser){
						case(DOSER_1):
										  break;
						case(DOSER_2):
										  break;
						case(DOSER_3):
										  break;
						case(VIBRO_0):
										  break;
						default:
										  break;

				}

}



void  doser_vibro_manager(){
	uint8_t id_mot=0;

	for(id_mot=0;id_mot<4;id_mot++){
		if (doser_vibro[id_mot].cmd.b.FREQ!=0){//new command for DOSER or VIBRO
			set_new_freq_doser(id_mot,doser_vibro[id_mot].pwm_freq );
			doser_vibro[id_mot].cmd.b.FREQ=0;
		}
		if (doser_vibro[id_mot].cmd.b.DUTY!=0){//new command for DOSER or VIBRO
			set_new_duty_doser(id_mot,doser_vibro[id_mot].pwm_duty );
			doser_vibro[id_mot].cmd.b.DUTY=0;
		}
		if (doser_vibro[id_mot].cmd.b.DIR!=0){//new command for DOSER or VIBRO
			set_new_dir_doser(id_mot,doser_vibro[id_mot].DIR_pin );
			doser_vibro[id_mot].cmd.b.DIR=0;
		}
		if (doser_vibro[id_mot].cmd.b.DIS!=0){//new command for DOSER or VIBRO
			stop_start_doser(id_mot,doser_vibro[id_mot].DIS_pin );
			doser_vibro[id_mot].cmd.b.DIS=0;
		}

	}



}

void vibro_external_drive(void){

	if ((revo_state==REVO_READY_TO_USE)||(revo_state==REVO_GRINDING_RUN)){

			if (pos_sensor.reg_b.portafilter==1){
				 doser_vibro[VIBRO_0].pwm_duty=80;
				 //doser_vibro[VIBRO_0].DIR_pin=DIR_FORWARD;
				 doser_vibro[VIBRO_0].DIS_pin=START_MOT;
				 //doser_vibro[VIBRO_0].cmd.b.DIR=1;
				 doser_vibro[VIBRO_0].cmd.b.DUTY=1;
				 doser_vibro[VIBRO_0].cmd.b.DIS=1;
			}else if (doser_vibro[VIBRO_0].DIS_pin==GPIO_PIN_RESET){
				 doser_vibro[VIBRO_0].pwm_duty=0;
				 doser_vibro[VIBRO_0].DIS_pin=STOP_MOT;
				 doser_vibro[VIBRO_0].cmd.b.DUTY=1;
				 doser_vibro[VIBRO_0].cmd.b.DIS=1;
			     }
	}else if (doser_vibro[VIBRO_0].DIS_pin==GPIO_PIN_RESET){
			 doser_vibro[VIBRO_0].pwm_duty=0;
			 doser_vibro[VIBRO_0].DIS_pin=STOP_MOT;
			 doser_vibro[VIBRO_0].cmd.b.DUTY=1;
			 doser_vibro[VIBRO_0].cmd.b.DIS=1;
	      }

}

void init_doser_vibro(void){
uint8_t id;


	for(id=0;id <4;id++){
		doser_vibro[id].DIS_pin=GPIO_PIN_SET;
		doser_vibro[id].DIR_pin=GPIO_PIN_RESET;
		doser_vibro[id].pwm_duty=0;//0%
		doser_vibro[id].speed_rpm=0;
		if(id==VIBRO_0){
		    doser_vibro[id].pwm_freq=1000;
		}else{
			doser_vibro[id].pwm_freq=100;//1000;
		}
		set_new_freq_doser(id,doser_vibro[id].pwm_freq );
        set_new_duty_doser(id,doser_vibro[id].pwm_duty);
        set_new_dir_doser(id,doser_vibro[id].DIR_pin);
		stop_start_doser(id,doser_vibro[id].DIS_pin);

	}
}

