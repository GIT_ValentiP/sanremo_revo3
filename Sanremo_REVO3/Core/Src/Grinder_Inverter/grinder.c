/*
 ******************************************************************************
 *  @file      : grinder.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 07 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "grinder.h"
#include "usart.h"
#include "tim.h"

//#include "globals.h"
#include "st_l6474_stepper.h"

/* Private define ------------------------------------------------------------*/
#define VIBRO_ON_ALL_GRINDIG    1
#define __INVERTER_REVERSE_ROTATION    1

#define BUFFER_SIZE_UART1_GRIND 128

// modbus specific exceptions
#define ILLEGAL_FUNCTION 1
#define ILLEGAL_DATA_ADDRESS 2
#define ILLEGAL_DATA_VALUE 3


/* Variables -----------------------------------------------------------*/
extern UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;


unsigned char TxEnablePin;
// frame[] is used to recieve and transmit packages.
// The maximum number of bytes in a modbus packet is 256 bytes
// This is limited to the serial buffer of 128 bytes
unsigned char frame[BUFFER_SIZE_UART1_GRIND];
//unsigned char frameRX[BUFFER_SIZE_UART1_GRIND];

unsigned int total_no_of_packets;
Packet* packet; // current packet


static unsigned char cycle_vibro_num;
/* Private typedef -----------------------------------------------------------*/



/* Private macro -------------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

// function definitions




void flush_serial_frame(unsigned char bufferSize);
void sendPacket(unsigned char bufferSize);
unsigned int calculateCRC(unsigned char bufferSize);
void check_RX_inverter_input_reg(void);


void grinder_attiva_doser(uint8_t id_doser,uint8_t start);
void grinder_check_end(void);


/* Private user code ---------------------------------------------------------*/

void flush_serial_frame(unsigned char bufferSize){

	for (unsigned char i = 0; i < bufferSize; i++){
			frame[i]=0; //Serial.flush();
		}
}


void sendPacket(unsigned char bufferSize)
{

	RS485_RX_GRIND_DISABLE;//RS485_RX_GRIND_ENABLE;//
    RS485_TX_GRIND_ENABLE;

    HAL_UART_Transmit(&huart1, (uint8_t *)frame,bufferSize,1000);

    RS485_TX_GRIND_DISABLE;//RS485_TX_GRIND_ENABLE;//
	RS485_RX_GRIND_ENABLE;

	//previousTimeout = millis(); // initialize timeout delay
}




unsigned int calculateCRC(unsigned char bufferSize)
{
  unsigned int temp, temp2, flag;
  temp = 0xFFFF;
  for (unsigned char i = 0; i < bufferSize; i++)
  {
    temp = temp ^ frame[i];
    for (unsigned char j = 1; j <= 8; j++)
    {
      flag = temp & 0x0001;
      temp >>= 1;
      if (flag)
        temp ^= 0xA001;
    }
  }
  // Reverse byte order.
  temp2 = temp >> 8;
  temp = (temp << 8) | temp2;
  temp &= 0xFFFF;
  return temp; // the returned value is already swopped - crcLo byte is first & crcHi byte is last
}




void init_grinder(void){
	  RS485_RX_GRIND_ENABLE;//RS485_RX_GRIND_DISABLE;
	  RS485_TX_GRIND_ENABLE;
	  //  RS485_RX_GRIND_ENABLE;
	  MX_USART1_UART_Init();
	  HAL_UART_MspInit(&huart1);//Inverter RS485 Half-Duplex Communication
	  grinder.inverter_status=INV_INIT_STATE;//INV_IDLE_STATE;//
}


void check_RX_inverter_input_reg(void)
{
	//uint16_t power_old;
	if (grinder.mdb_msgOk==1){ // check for response


//		unsigned char no_of_registers = grinder.mdb_nreg;
//		unsigned char no_of_bytes = no_of_registers * 2;
//		unsigned char len=7;//no_of_bytes+5;
		grinder.input_reg[4] = grinder.frameRX[4];
		grinder.input_reg[4] <<=8 ;
		grinder.input_reg[4] += grinder.frameRX[5];

		//printf("\n\r INV.RX:%02X %02X %02X %02X %02X %02X %02X",grinder.frameRX[0],grinder.frameRX[1],grinder.frameRX[2],grinder.frameRX[3],grinder.frameRX[4],grinder.frameRX[5],grinder.frameRX[6]);
		grinder.frameRX[3]=0;
		grinder.frameRX[4]=0;
//		if ((grinder.frameRX[0] == grinder.mdb_slave_adr)&&(grinder.frameRX[1] == grinder.mdb_oc)){//&&(grinder.frameRX[2] == no_of_bytes)){ // check number of bytes returned
		    // combine the crc Low & High bytes
//			unsigned int recieved_crc = ((grinder.frameRX[len - 2] << 8) | grinder.frameRX[len - 1]);
//			unsigned int calculated_crc = calculateCRC(len- 2);
//			if (calculated_crc == recieved_crc){ // verify checksum
//			  unsigned char index = 3;
//			  for (unsigned char i = 0; i < no_of_registers; i++){
				// start at the 4th element in the recieveFrame and combine the Lo byte
//				grinder.input_reg[i+grinder.mdb_start_adr] = (grinder.frameRX[index] << 8) | grinder.frameRX[index + 1];
				//grinder.input_reg[4] = (grinder.frameRX[3] << 8) | grinder.frameRX[4];
//				index += 2;
//			  }

//			  messageOkFlag = 1; // message successful
//			}else{ // checksum failed
//			  messageErrFlag = 1; // set an error
//			}
//		}else { // incorrect number of bytes returned
//		   messageErrFlag = 1; // set an error
//		}
	   if (grinder.input_reg[4] > grinder.power_rd){
		  grinder.power_under_counter=0;
	   }else{
		   if (grinder.time_grinding <(GRINDING_TIME_MAX - 4000)){
		     grinder.power_under_counter++;
		   }

	   }
	   grinder.power_rd=grinder.input_reg[4];
	   grinder.mdb_msgOk=0;
	   grinder.mdb_timeout_RX=0;
	} // check message booleans

}



void grinder_read_register(uint8_t nbytes){
 unsigned char i;
	RS485_TX_GRIND_DISABLE;//RS485_TX_GRIND_ENABLE;//
	RS485_RX_GRIND_ENABLE;//RS485_RX_GRIND_DISABLE;

    grinder.mdb_msgOk=0;
    for(i=0;i<nbytes;i++){
    	grinder.frameRX[i]=0;
    }
    HAL_UART_Receive_IT(&huart1,(uint8_t*)grinder.frameRX, nbytes);//HAL_UART_Receive_DMA(&huart1, (uint8_t*)pData, 1);//Serial.read();
    grinder.mdb_timeout_RX=300;//nbytes*10;

    //RS485_TX_GRIND_ENABLE;//RS485_TX_GRIND_DISABLE;//
    //RS485_RX_GRIND_ENABLE;
}



void grinder_write_register(uint8_t op_code,uint16_t start_adr,uint16_t *value_buf,uint8_t nreg){
	unsigned char no_of_bytes ;
	unsigned char frameSize	;
	unsigned int temp;
	unsigned char index;
	unsigned char no_of_registers;
	uint16_t coil_val;
	  frame[0] = GRINDER_SLAVE_ADR_MDB;
	  frame[1] = op_code;
	  frame[2] = start_adr >> 8; // address Hi
	  frame[3] = start_adr & 0xFF; // address Lo


	  unsigned int crc16;

	  // construct the frame according to the modbus function
	  switch(op_code){
	   case(PRESET_MULTIPLE_REGISTERS):
			                            frame[4] = nreg >> 8; // no_of_registers Hi
										frame[5] = nreg & 0xFF; // no_of_registers Lo

										no_of_bytes =  2*nreg;
										frame[6] = no_of_bytes; // number of bytes
									    frameSize = 9 + no_of_bytes; // first 7 bytes of the array + 2 bytes CRC+ noOfBytes
										index = 7; // user data starts at index 7
										no_of_registers = nreg;
										for (unsigned char i = 0; i < no_of_registers; i++)
										{
										  temp = value_buf[i]; // get the data
										  frame[index] = temp >> 8;
										  index++;
										  frame[index] = temp & 0xFF;
										  index++;
										}
										crc16 = calculateCRC(frameSize - 2);
										frame[frameSize - 2] = crc16 >> 8; // split crc into 2 bytes
										frame[frameSize - 1] = crc16 & 0xFF;
										sendPacket(frameSize);

	                                    break;


	   case(READ_HOLDING_REGISTERS):
		                                frame[4] = nreg >> 8; // no_of_registers Hi
										frame[5] = nreg & 0xFF; // no_of_registers Lo
										crc16 = calculateCRC(6); // the first 6 bytes of the frame is used in the CRC calculation
										frame[6] = crc16 >> 8; // crc Lo
										frame[7] = crc16 & 0xFF; // crc Hi
										sendPacket(8); // a request with function 3, 4 & 6 is always 8 bytes in size

			                            break;

	   case(READ_DISCRETE_COIL):


	 			                        break;


	   case(READ_INPUT_REGISTERS):
									frame[4] = nreg >> 8; // no_of_registers Hi
									frame[5] = nreg & 0xFF; // no_of_registers Lo
									crc16 = calculateCRC(6); // the first 6 bytes of the frame is used in the CRC calculation
									frame[6] = crc16 >> 8; // crc Lo
									frame[7] = crc16 & 0xFF; // crc Hi

									sendPacket(8); // a request with function 3, 4 & 6 is always 8 bytes in size

									grinder.mdb_oc=READ_INPUT_REGISTERS;
									grinder.mdb_slave_adr=GRINDER_SLAVE_ADR_MDB;
									grinder.mdb_nreg=nreg;
									grinder.mdb_nbytes=((nreg*2)+5);
									//grinder_read_register(grinder.mdb_nbytes);

	   	 			                break;
	   case(WRITE_MULTIPLE_COIL):

		                                    frame[4] = nreg >> 8; // no_of_registers Hi
											frame[5] = nreg & 0xFF; // no_of_registers Lo

	  			  	 			            coil_val=(*value_buf);//(*value_buf & (0x0001 << start_adr));
	  			  	 			            if(nreg<9){
	  			  	 			              no_of_bytes =  1;
	  			  	 			            }else{
	  			  	 			              no_of_bytes =  2;
	  			  	 			            }
											frame[6] = no_of_bytes; // number of bytes
											frameSize = 9 + no_of_bytes; // first 7 bytes of the array + 2 bytes CRC+ noOfBytes
											index = 7; // user data starts at index 7
											no_of_registers = nreg;
											if(nreg<9){
											   frame[index] =coil_val & 0xFF;
											}else{

											  frame[index] = coil_val >> 8;
											  index++;
											  frame[index] =coil_val & 0xFF;

											}
	  	                                    crc16 = calculateCRC(frameSize - 2); // the first 6 bytes of the frame is used in the CRC calculation
	  	   									frame[frameSize-2] = crc16 >> 8; // crc Lo
	  	   									frame[frameSize-1] = crc16 & 0xFF; // crc Hi
	  	   									sendPacket(frameSize); // a request with function 3, 4 & 6 is always 8 bytes in size
	  	  	 			                    break;

	   case(WRITE_SINGLE_COIL):

		                                frame[4] = 0x00; // crc Lo
			  	 			            frame[5] = 0x00; // crc Hi
			  	 			            coil_val=(*value_buf & (0x0001 << start_adr));
	  	 			                    if (coil_val !=0) {
	  	 			                    	frame[4] = 0xFF; // crc Lo
	  	 			                    }
	                                    crc16 = calculateCRC(6); // the first 6 bytes of the frame is used in the CRC calculation
	   									frame[6] = crc16 >> 8; // crc Lo
	   									frame[7] = crc16 & 0xFF; // crc Hi
	   									sendPacket(8); // a request with function 3, 4 & 6 is always 8 bytes in size
	  	 			                    break;


	   default:
		       break;
	  }



}

void grinder_attiva_doser(uint8_t id_doser,uint8_t start){
	if(start==1){
		doser_vibro[id_doser].DIS_pin  =START_MOT;
		doser_vibro[id_doser].DIR_pin  =DIR_REVERSE;
		doser_vibro[id_doser].pwm_duty =80 ;

	}else{
		doser_vibro[id_doser].DIS_pin=STOP_MOT;
		doser_vibro[id_doser].DIR_pin=DIR_FORWARD;
		doser_vibro[id_doser].pwm_duty =0 ;
	}
	doser_vibro[id_doser].cmd.b.DIS=1;//new DIS for DOSER
    doser_vibro[id_doser].cmd.b.DUTY=1;//new DUTY for DOSER
	doser_vibro[id_doser].cmd.b.DIR =1;
}

void grinder_check_end(void){

	if(grinder.manual[MANUAL_START]==1){//GRINDING MANUAL
		if (grinder.time_grinding == 0){
		    grinder.inverter_status=INV_POWEROFF_STATE;
		}else if (grinder.time_grinding < 1500){
			grinder.manual[MANUAL_STOP]=1;
		}
		if(grinder.manual[MANUAL_STOP]==1){
		    //grinder.inverter_status=INV_POWEROFF_STATE;
			if (grinder.manual[FROM_DOSER1]==1){
			   grinder.manual[FROM_DOSER1]=0;
			   grinder_attiva_doser(DOSER_1,0);
			   hopper_burr[HOPPER_1].cmd=FINISH_MOVEMENT_STEPPER;
			}
			if (grinder.manual[FROM_DOSER2]==1){
			   grinder.manual[FROM_DOSER2]=0;
			   grinder_attiva_doser(DOSER_2,0);
			   hopper_burr[HOPPER_2].cmd=FINISH_MOVEMENT_STEPPER;
			}
			if (grinder.manual[FROM_DOSER3]==1){
			   grinder.manual[FROM_DOSER3]=0;
			   grinder_attiva_doser(DOSER_3,0);
			   hopper_burr[HOPPER_3].cmd=FINISH_MOVEMENT_STEPPER;
			}
		}else{
			 if (grinder.hopper[HOPPER_1]==1){
				grinder.hopper[HOPPER_1]=0;
				grinder_attiva_doser(DOSER_1,1);
			    hopper_burr[HOPPER_1].cmd=MOVE_STEPPER;
			 }
			if (grinder.hopper[HOPPER_2]==1){
				grinder.hopper[HOPPER_2]=0;
				grinder_attiva_doser(DOSER_2,1);
				hopper_burr[HOPPER_2].cmd=MOVE_STEPPER;
			}
			if (grinder.hopper[HOPPER_3]==1){
				grinder.hopper[HOPPER_3]=0;
				grinder_attiva_doser(DOSER_3,1);
				hopper_burr[HOPPER_3].cmd=MOVE_STEPPER;
			}

		}
	}else { //GRINDING AUTO
		if (grinder.time_grinding ==0){
		   grinder.inverter_status=INV_POWEROFF_STATE;
		   // grinder.timeout_TX=300;
		   // grinder.inverter_status=INV_POWEROFF_STATE;
		}else  if (grinder.time_grinding > (GRINDING_TIME_MAX-500)) {
				if (grinder.hopper[HOPPER_1]==1){
				grinder.hopper[HOPPER_1]=0;
				hopper_burr[HOPPER_1].cmd=OPEN_STEPPER;
				}
				if (grinder.hopper[HOPPER_2]==1){
				grinder.hopper[HOPPER_2]=0;
				hopper_burr[HOPPER_2].cmd=OPEN_STEPPER;
				}
				if (grinder.hopper[HOPPER_3]==1){
				grinder.hopper[HOPPER_3]=0;
				hopper_burr[HOPPER_3].cmd=OPEN_STEPPER;
				}
		}
	}

}

void grinder_manager(void){

		switch (grinder.inverter_status){

		         case(INV_IDLE_STATE):
		                                //if (flag_1min==1){
		                                //    grinder.inverter_status=INV_POWERON_STATE;

		                                	//flag_1min=0;
		                                 //  }
		        		                 break;
		         case(INV_INIT_STATE):
//		                                    	INV_HZSET          =0,
//				                            	INV_TIME           =1,
//									            INV_DECTIME        =2,
//												INV_DELAY_STEP_TIME=3,
//												INV_HZSET_REL      =4,//completed
//												INV_TIMEREL        =5,//not completed
//												INV_UNBLOCKPWRLIM  =6,
//												INV_HZSET_DEC      =7,//completed
										if (grinder.timeout_TX==0){
										  grinder.hold_reg[INV_HZSET]=40;
										  grinder.hold_reg[INV_TIME]=0;
										  grinder.hold_reg[INV_DECTIME]=0;
										  grinder.hold_reg[INV_DELAY_STEP_TIME]=5;
										  grinder.hold_reg[INV_HZSET_REL]=15;
										  grinder.hold_reg[INV_TIMEREL]=500;
										  grinder.hold_reg[INV_UNBLOCKPWRLIM]=910;
										  grinder.hold_reg[INV_HZSET_DEC]=0;
										  grinder_write_register(PRESET_MULTIPLE_REGISTERS,INV_HZSET,(uint16_t*)grinder.hold_reg,7);
										  grinder.timeout_TX=300;
										  grinder.inverter_status=INV_SET_PARAM_STATE;
										 }
		       		        		     break;

		         case(INV_SET_PARAM_STATE):
		                                 if (grinder.timeout_TX==0){
		                                 grinder.coil.coil.ROTATION=INVERTER_ROT_FORWARD;
				                         grinder_write_register(WRITE_SINGLE_COIL,INV_ROTATION_ADR,&grinder.coil.coil16_t,1);
				                         grinder.timeout_TX=300;
				                         grinder.inverter_status=INV_IDLE_STATE;//INV_SET_LIMIT_UNLOCK;
		                                 }
		       		        		     break;

		         case(INV_SET_LIMIT_UNLOCK)://(INV_SET_START_UNLOCK):
										 if (grinder.timeout_TX==0){
										 grinder.coil.coil.LIMITATION=1;
										 grinder_write_register(WRITE_SINGLE_COIL,INV_LIMITATION_ADR,&grinder.coil.coil16_t,1);
										 grinder.timeout_TX=300;
										 grinder.inverter_status=INV_IDLE_STATE;
										 }
										 break;

		         case(INV_GRINDING_SIZE_SETBURR):
		                                          if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
		                                        	  revo_state=REVO_SELF_TEST_RUN;
			                            	      }else{
		                                              revo_state=REVO_BURR_POS_RUN;//REVO_GRINDING_RUN;
			                            	      }
		                                         grinder.coil.coil.ROTATION=INVERTER_ROT_FORWARD;
		        				                 grinder_write_register(WRITE_SINGLE_COIL,INV_ROTATION_ADR,&grinder.coil.coil16_t,1);
						                         grinder.timeout_TX=300;
		                                         hopper_burr[BURR_0].cmd=BURR_POSITIONING_START_STEPPER;
				                                 grinder.inverter_status=INV_POWERON_STATE;


		        		                        break;

		         case(INV_POWERON_STATE):
		                                     if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
												  revo_state=REVO_SELF_TEST_RUN;
											  }else{
												  revo_state=REVO_BURR_POS_RUN;//REVO_GRINDING_RUN;
											  }
		                                // grinder_write_register(PRESET_MULTIPLE_REGISTERS,,0);
                                         //INVERTER_STATE_ON
		                                 if ((grinder.timeout_TX==0)&&(hopper_burr[BURR_0].cmd==IDLE_STEPPER)){
											 grinder.coil.coil.SETSTATE=INVERTER_STATE_ON;
											 grinder_write_register(WRITE_SINGLE_COIL,INV_SETSTATE_ADR,&grinder.coil.coil16_t,1);

                                             grinder.time_grinding=GRINDING_TIME_MAX;

                                             grinder.power_under_counter=0;
												// doser_vibro[VIBRO_0].pwm_duty=80;
												// doser_vibro[VIBRO_0].DIR_pin=DIR_FORWARD;
												// doser_vibro[VIBRO_0].DIS_pin=START_MOT;
												//
												// doser_vibro[VIBRO_0].cmd.b.DIR=1;
												// doser_vibro[VIBRO_0].cmd.b.DUTY=1;
												// doser_vibro[VIBRO_0].cmd.b.DIS=1;

											 fan[FAN_1].pwm_duty[0]=FAN1_DUTY_GRINDING;
					                         fan[FAN_2].pwm_duty[0]=FAN2_DUTY_GRINDING;
											 fan[FAN_1].cmd.b.DUTY=1;
											 fan[FAN_2].cmd.b.DUTY=1;

											 grinder.timeout_TX=300;
											 grinder.mdb_timeout_RX=0;
											 grinder.mdb_msgOk=0;

											 stepper_burr_stall(600);
											 revo_state=REVO_GRINDING_RUN;

											 grinder.inverter_status=INV_READ_PARAM_STATE;
		                                 }
		                                 //printf("POWER INVERTER %d",INVERTER_STATE_ON);
										 break;


		         case(INV_POWEROFF_STATE):
                                         //INVERTER_STATE_OFF
		                                 //grinder_write_register(WRITE_SINGLE_COIL,INV_SETSTATE_ADR,INVERTER_STATE_OFF);
		                                 if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
											  revo_state=REVO_SELF_TEST_RUN;
										 }else{
											  revo_state=REVO_GRINDING_RUN;
										 }
		                                 if (grinder.timeout_TX==0){
											 grinder.coil.coil.SETSTATE=INVERTER_STATE_OFF;
											 grinder_write_register(WRITE_SINGLE_COIL,INV_SETSTATE_ADR,&grinder.coil.coil16_t,1);
											 grinder.timeout_TX=100;
											 doser_vibro[VIBRO_0].count_down_vibro_off=doser_vibro[VIBRO_0].delay_over_stop;
                                             #ifdef VIBRO_ON_ALL_GRINDIG
												    doser_vibro[VIBRO_0].count_down_vibro_off=10;//300;//100;//
                                             #else
												    doser_vibro[VIBRO_0].count_down_vibro_off=10;//
                                             #endif

											 fan[FAN_1].pwm_duty[0]=FAN1_DUTY_STANDBY;
											 fan[FAN_2].pwm_duty[0]=FAN2_DUTY_STANDBY;
											 fan[FAN_1].cmd.b.DUTY=1;
											 fan[FAN_2].cmd.b.DUTY=1;

											 stepper_burr_stall(0);

											 cycle_vibro_num=1;
											 grinder.inverter_status=INV_POWEROFF_VIBRO_ON_STATE;

		                                  }
										  break;

		         case(INV_POWEROFF_VIBRO_ON_STATE):
		                                                //INVERTER_STATE_OFF
		                                            if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
														  revo_state=REVO_SELF_TEST_RUN;
													  }else{
														  revo_state=REVO_GRINDING_RUN;
													  }
		                                             if (doser_vibro[VIBRO_0].count_down_vibro_off==0){
                                                         #ifdef VIBRO_ON_ALL_GRINDIG
														     doser_vibro[VIBRO_0].count_down_vibro_off=10;//300;//100;//
                                                         #else
														     doser_vibro[VIBRO_0].pwm_duty=0;
														     doser_vibro[VIBRO_0].DIS_pin=STOP_MOT;
														     doser_vibro[VIBRO_0].cmd.b.DUTY=1;
														     doser_vibro[VIBRO_0].cmd.b.DIS=1;

														     doser_vibro[VIBRO_0].count_down_vibro_off=10;//
                                                         #endif

														 if(cycle_vibro_num >0){
															 cycle_vibro_num--;
														 	 revo_state=REVO_GRINDING_RUN;
														 	 grinder.inverter_status=INV_POWEROFF_VIBRO_OFF_STATE;
														 }else{ //
															 if (grinder.timeout_TX==0){
																 if(hopper_burr[HOPPER_1].cmd == IDLE_STEPPER){//open HOPPER1
																	  if (hopper_burr[HOPPER_2].cmd == IDLE_STEPPER){//open HOPPER2
																		  if (hopper_burr[HOPPER_3].cmd == IDLE_STEPPER){//open HOPPER3
																			  grinder.coil.coil.SETSTATE=INVERTER_STATE_OFF;
																			  grinder_write_register(WRITE_SINGLE_COIL,INV_SETSTATE_ADR,&grinder.coil.coil16_t,1);
																			  grinder.timeout_TX=300;
																			  //if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
																				//  grinder.clean_cycle=END_CLEAN_CYCLE;
																				//  revo_state=REVO_SELF_TEST_RUN;
																			  //}
																			  revo_state=REVO_GRINDING_END;
																			  cmd_rx_timeout=150;
																			  grinder.inverter_status=INV_IDLE_STATE;
																		 }
																	  }
																  }
															 }
														 }
		                                              }
														 #ifdef INVERTER_REVERSE_ROTATION
														   else{
															  if ((grinder.timeout_TX<300)&&(grinder.timeout_TX >200)){
																/*REVERSE ROTATION INVERTER GRINDER*/
															   grinder.coil.coil.SETSTATE=INVERTER_STATE_ON;
															   grinder_write_register(WRITE_MULTIPLE_COIL,INV_SETSTATE_ADR,&grinder.coil.coil16_t,2);
															   grinder.timeout_TX=190;
															 }
														  }
														 #endif
		       										 break;

		         case(INV_POWEROFF_VIBRO_OFF_STATE):
														//INVERTER_STATE_OFF VIBRO OFF
		                                             revo_state=REVO_GRINDING_RUN;
													 if (doser_vibro[VIBRO_0].count_down_vibro_off==0){
														 doser_vibro[VIBRO_0].pwm_duty=80;
														 doser_vibro[VIBRO_0].DIS_pin=START_MOT;
														 doser_vibro[VIBRO_0].cmd.b.DUTY=1;
														 doser_vibro[VIBRO_0].cmd.b.DIS=1;
                                                         #ifdef VIBRO_ON_ALL_GRINDIG
														    doser_vibro[VIBRO_0].count_down_vibro_off=2000;//3500;//700;//
                                                         #else
														    doser_vibro[VIBRO_0].count_down_vibro_off=2000;//
                                                         #endif

                                                         #ifdef INVERTER_REVERSE_ROTATION
															 grinder.coil.coil.ROTATION=INVERTER_ROT_REVERSE;//INVERTER_ROT_FORWARD;
															 grinder_write_register(WRITE_SINGLE_COIL,INV_ROTATION_ADR,&grinder.coil.coil16_t,1);
														 #endif
														 grinder.timeout_TX=600;
														 grinder.inverter_status=INV_POWEROFF_VIBRO_ON_STATE;
													  }
													 break;


		         case(INV_READ_PARAM_STATE):
                                          // if (	flag_1min==1){
                                        	//   grinder.inverter_status=INV_POWEROFF_STATE;
                                        	  // flag_1min=0;
                                           //}
		                                   if(grinder.clean_cycle==FIRST_CLEAN_CYCLE){
												  revo_state=REVO_SELF_TEST_RUN;
											  }else{
												  revo_state=REVO_GRINDING_RUN;
											  }
                                           grinder_check_end();

											  #ifdef VIBRO_ON_ALL_GRINDIG
												   if(doser_vibro[VIBRO_0].DIS_pin != GPIO_PIN_RESET){//Vibro Off
													   if (grinder.time_grinding < (GRINDING_TIME_MAX-1500)) {
																 doser_vibro[VIBRO_0].pwm_duty=80;
																 doser_vibro[VIBRO_0].DIR_pin=DIR_FORWARD;
																 doser_vibro[VIBRO_0].DIS_pin=START_MOT;

																 doser_vibro[VIBRO_0].cmd.b.DIR=1;
																 doser_vibro[VIBRO_0].cmd.b.DUTY=1;
																 doser_vibro[VIBRO_0].cmd.b.DIS=1;
													   }
												   }
											 #endif
                                           check_RX_inverter_input_reg();

                                           if (grinder.timeout_TX==0){
												//check_RX_inverter_input_reg();
											   // if (grinder.mdb_timeout_RX==0){
                                        	    //printf("\n INV Timeout");
                                        	    HAL_UART_AbortReceive_IT(&huart1);
                                        	    grinder_write_register(READ_INPUT_REGISTERS,INV_INPUT_POWER_ADR,(uint16_t *)grinder.input_reg,1);
											    grinder_read_register(grinder.mdb_nbytes);
											   // }
												grinder.timeout_TX=90;
												printf("\n\r INV. Power:%d [W]",grinder.input_reg[4]);
												printf("\n\r INV. UnderLOWPower:%d",grinder.power_under_counter);
												if ( grinder.power_under_counter> 5){
													//grinder.time_grinding=0;
												}
                                           }
										   break;

		         case(INV_POWER_OPEN_HOPPER):
		                                         revo_state=REVO_GRINDING_RUN;
		                                          if(hopper_burr[HOPPER_1].cmd == IDLE_STEPPER){//open HOPPER2
		                                        	  if (hopper_burr[HOPPER_2].cmd == IDLE_STEPPER){//open HOPPER3
		                                        		  if (hopper_burr[HOPPER_3].cmd == IDLE_STEPPER){//open HOPPER3
    														  grinder.timeout_TX=300;
															  grinder.inverter_status=INV_POWEROFF_STATE;
														 }
		                                        	  }
												  }


		       									 break;


		         case(INV_ERR_STATE):
										 break;

		         default:
		        	       break;


		}


}






/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/


