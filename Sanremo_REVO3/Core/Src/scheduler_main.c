/*
 ******************************************************************************
 *  @file      : scheduler_main.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 08 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "scheduler_main.h"
#include "ifx9201sg_doser.h"
#include "fan.h"
#include "st_l6474_stepper.h"
#include "balance.h"
#include "grinder.h"
#include "mdb_com.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

void scheduler_task(void){


	switch(revo_state){



	    case(REVO_SELF_TEST_RUN):
								 /*HOPPER and BURR Stepper Init*/
								  revo_state=REVO_SELF_TEST_RUN;
	                              if((revo_board_conf !=REVO_NOT_SET_CONFIG)&&(revo_op_mode.mode_op_reg!=0xFFFF)){
//DEBUG
	                            	  if (service.flag.selftest==0){
	                            		  printf("\n\rStartSELF TEST RUN HOPPER1");
	                            		  service.flag.selftest=1;
	                            		  hopper_burr[HOPPER_1].cmd=INIT_STEPPER;
	                            	  }else if (hopper_burr[BURR_0].init_cmd==HOME_INIT_STEPPER){
	                            			  if(grinder.grinding_size!=0){
	                            				printf("\n\rBURRs initial positioning");
												stepper_burr_position();
											  }
											  grinder.clean_cycle=0;
											  revo_state=REVO_READY_TO_USE;//REVO_CLEAN_CYCLE;//REVO_GRINDING_START;//

	                            		  }
	                               }
//ENDDEBUG

//	                            	  if(grinder.clean_cycle==END_CLEAN_CYCLE){
//										  grinder.clean_cycle=0;
//										  revo_state=REVO_READY_TO_USE;//REVO_CLEAN_CYCLE;//REVO_GRINDING_START;//
//	                            	  }else if(grinder.clean_cycle==0){
//	                            		  grinder.grinding_size=250;
//	                            		  grinder.clean_cycle=FIRST_CLEAN_CYCLE;
//	                            		  grinder.inverter_status=INV_GRINDING_SIZE_SETBURR;//INV_POWERON_STATE;
//	                            	  }


	   	    	                  break;


	    case(REVO_IDLE):
		                          revo_state=REVO_IDLE;
		                          if(revo_board_conf !=REVO_NOT_SET_CONFIG){
//		                        	  if((revo_board_conf !=REVO_NOT_SET_CONFIG)&&(revo_op_mode.mode_op_reg!=0xFFFF)){
//										  init_stepper();
//										  grinder.clean_cycle=0;//for Purge automatic at POWER-ON//FIRST_CLEAN_CYCLE;
//										  grinder.timeout_TX=2000;
									  revo_state=REVO_SELF_TEST_RUN;//REVO_READY_TO_USE;//REVO_CLEAN_CYCLE;//REVO_GRINDING_START;//
//		                              }
									  service.flag.selftest=0;
			                      }

	 	    	                 break;


	    case(REVO_CLEAN_CYCLE):
	    		                 grinder.grinding_size=250;
	                             revo_state=REVO_GRINDING_START;
	    		                 break;

	    case(REVO_READY_TO_USE):
		                          //if (revo_op_mode.bit.service==0){//RUN Mode
										  revo_state=REVO_READY_TO_USE;
										  if (revo_calibration_cmd.cal_b.BURR_POS_CAL_START==1){//Start Burr Zero Positioning
											  //revo_calibration_cmd.cal_b.BURR_POS_CAL_START=0;
											  revo_state=REVO_BURR_POS_CAL_RUN;
										  }
										  if (revo_op_cmd.cmd.WEIGH_HOP1==1){
											  balance.weight_target=hopper_burr[HOPPER_1].hopper_weight_target;
											  balance.time_target  =hopper_burr[HOPPER_1].hopper_time_target;
											  balance.weight_rd=0;
											  balance.timeout_Weight= balance.time_target ;
											  hopper_burr[HOPPER_1].hopper_weight_rd=0;
											  hopper_burr[HOPPER_1].hopper_time_rd=0;
											  balance.doser=DOSER_1;
											  #ifdef BALANCE_NOT_CONNECTED
											  balance.state=WEIGHING_BALANCE;//START_MEASURE_BALANCE;
											  #else
											  if (revo_op_mode.bit.run_weight_time==0){
											   balance.state=START_MEASURE_BALANCE;
											   //printf("\n\r HOP1:%lu [gr/10]",balance.weight_target);
											  }else{
											   balance.state=START_MEASURE_TIME;
											   //printf("\n\r HOP1:%d [s/10]",balance.time_target);
											  }
											  #endif
											  printf("\n\r Start Weigh HOP1");
											  revo_state=REVO_WEIGHT1_RUN;


										  }else if (revo_op_cmd.cmd.WEIGH_HOP2==1){
											  balance.weight_target=hopper_burr[HOPPER_2].hopper_weight_target;
											  balance.time_target  =hopper_burr[HOPPER_2].hopper_time_target;
											  balance.weight_rd=0;
											  balance.timeout_Weight= balance.time_target ;
											  hopper_burr[HOPPER_2].hopper_weight_rd=0;
											  hopper_burr[HOPPER_2].hopper_time_rd=0;
											  balance.doser=DOSER_2;
											  #ifdef BALANCE_NOT_CONNECTED
											  balance.state=WEIGHING_BALANCE;//START_MEASURE_BALANCE;
											  #else
											  //printf("\n Op.Mode:%d",revo_op_mode.mode_op_reg);
											  if (revo_op_mode.bit.run_weight_time==0){
											   balance.state=START_MEASURE_BALANCE;
											   //printf("\n\r HOP2:%lu [gr/10]",balance.weight_target);
											  }else{
											   balance.state=START_MEASURE_TIME;
											   //printf("\n\r HOP2:%d [s/10]",balance.time_target);
											  }

											  #endif
											  printf("\n\r Start Weigh HOP2");
											  revo_state=REVO_WEIGHT2_RUN;
											  //printf("\n\r HOP2:%lu [gr/10]",balance.weight_target);
											}else if (revo_op_cmd.cmd.WEIGH_HOP3==1){
												  balance.weight_target=hopper_burr[HOPPER_3].hopper_weight_target;
												  balance.time_target  =hopper_burr[HOPPER_3].hopper_time_target;
												  balance.weight_rd=0;
												  balance.timeout_Weight= balance.time_target ;
												  hopper_burr[HOPPER_3].hopper_weight_rd=0;
												  hopper_burr[HOPPER_3].hopper_time_rd=0;
												  balance.doser=DOSER_3;
												  #ifdef BALANCE_NOT_CONNECTED
												  balance.state=WEIGHING_BALANCE;//START_MEASURE_BALANCE;
												  #else
												  if (revo_op_mode.bit.run_weight_time==0){
												   balance.state=START_MEASURE_BALANCE;
												   //printf("\n\r HOP3:%lu [gr/10]",balance.weight_target);
												  }else{
												   balance.state=START_MEASURE_TIME;
												   //printf("\n\r HOP3:%d [s/10]",balance.time_target);
												  }
												  #endif
												  printf("\n\r Start Weigh HOP3");
												  revo_state=REVO_WEIGHT3_RUN;
												  //printf("\n\r HOP3:%lu [gr/10]",balance.weight_target);
												}else if((revo_op_cmd.cmd.GRIND_HOP1==1)||(revo_op_cmd.cmd.GRIND_HOP2==1)||(revo_op_cmd.cmd.GRIND_HOP3==1)){
													grinder.manual[MANUAL_START]=0;
													if(revo_op_cmd.cmd.GRIND_HOP1==1){
													   grinder.hopper[HOPPER_1]=1;
													   if(revo_op_cmd.cmd.GRIND_MAN==1){
														   grinder.manual[FROM_DOSER1]=1;
														   printf("\n\r Grind Manual Doser1");
													   }else{
													       printf("\n\r StartGrind1");
													   }
													}
													if(revo_op_cmd.cmd.GRIND_HOP2==1){
													   grinder.hopper[HOPPER_2]=1;
													   if(revo_op_cmd.cmd.GRIND_MAN==1){
														   grinder.manual[FROM_DOSER2]=1;
														   printf("\n\r Grind Manual Doser2");
													   }else{
													       printf("\n\r StartGrind2");
													   }
													}
													if(revo_op_cmd.cmd.GRIND_HOP3==1){
													   grinder.hopper[HOPPER_3]=1;
													   if(revo_op_cmd.cmd.GRIND_MAN==1){
														   grinder.manual[FROM_DOSER3]=1;
														   printf("\n\r Grind Manual Doser3");
													   }else{
														   printf("\n\r StartGrind3");
													   }
													}

													if(revo_op_cmd.cmd.GRIND_MAN==1){
													   grinder.manual[MANUAL_START]=1;
													   grinder.manual[MANUAL_STOP] =0;
											        }
													revo_state=REVO_BURR_POS_RUN;//REVO_GRINDING_RUN;//   revo_state=REVO_GRINDING_START;
												    grinder.inverter_status=INV_GRINDING_SIZE_SETBURR;//INV_POWERON_STATE;
													//   printf("\n\r StartGrind.Cmd:%d",revo_op_cmd.cmd_reg16);
												    printf("\n\r Grinding-Set Burr");//printf("\n\r Set Burr");
												  }
										  calibration_burr(1465,AVVICINA_MACINA,0,0);//1465 steps =10um =15� rotazione enconder)
										  vibro_external_drive();
									//}else{ //Go in SERVICE Mode
										//	  revo_state=REVO_SERVICE_MODE;
							    	    // }




	   	    	              break;


	    case(REVO_WEIGHING_START):

	   	    	              break;

	    case(REVO_WEIGHT1_RUN):
	                            printf("\n\r Run Weigh1:%d",balance.state);
	    	                    printf("\n\r Weight:%4ld",balance.weight_rd);
	   	    	                break;

	    case(REVO_WEIGHT1_END):
		                          // printf("\n\r End Weigh1");
	                              // printf("\n\r Weight:%4ld",balance.weight_rd);
	                               revo_state=REVO_WEIGHT1_END;
	  	 	                       if (cmd_rx_timeout==0){
									revo_op_cmd.cmd.WEIGH_HOP1=0;
									revo_state=REVO_READY_TO_USE;
	  	 	                       }
//									if (revo_op_cmd.cmd.WEIGH_HOP2==1){
//									  balance.weight_target=hopper_burr[HOPPER_2].hopper_weight_target;
//									  balance.weight_rd=0;
//									  hopper_burr[HOPPER_2].hopper_weight_rd=0;
//									  balance.doser=DOSER_2;
//									  balance.state=START_MEASURE_BALANCE;
//									  revo_state=REVO_WEIGHT2_RUN;
//									}else if (revo_op_cmd.cmd.WEIGH_HOP3==1){
//										  balance.weight_target=hopper_burr[HOPPER_3].hopper_weight_target;
//										  balance.weight_rd=0;
//										  hopper_burr[HOPPER_3].hopper_weight_rd=0;
//										  balance.doser=DOSER_3;
//										  balance.state=START_MEASURE_BALANCE;
//										  revo_state=REVO_WEIGHT3_RUN;
//										}else {
//											     revo_state=REVO_READY_TO_USE;
//											  }


//											if (revo_op_cmd.cmd.GRIND_HOP1==1){
//											   revo_state=REVO_GRINDING_START;
//											  }else{
//												   revo_state=REVO_READY_TO_USE;
//												  }

	   	    	              break;

	    case(REVO_WEIGHT1_ERROR):

	   	    	              break;

	    case(REVO_WEIGHT2_RUN):

		                          //printf("\n\r Run Weigh2:%d",balance.state);
	    	                      //printf("\n\r Weight:%4ld",balance.weight_rd);
	 	   	    	              break;


	 	case(REVO_WEIGHT2_END):
		                        //printf("\n\r End Weigh2");
	 	                        //printf("\n\r Weight:%4ld",balance.weight_rd);
	 	                        revo_state=REVO_WEIGHT2_END;
	 	                        if (cmd_rx_timeout==0){
		                           revo_op_cmd.cmd.WEIGH_HOP2=0;
		                           revo_state=REVO_READY_TO_USE;
	 	                        }
//								if (revo_op_cmd.cmd.WEIGH_HOP3==1){
//								   balance.weight_target=hopper_burr[HOPPER_3].hopper_weight_target;
//								   balance.weight_rd=0;
//								   hopper_burr[HOPPER_3].hopper_weight_rd=0;
//								   balance.doser=DOSER_3;
//								   balance.state=START_MEASURE_BALANCE;
//								   revo_state=REVO_WEIGHT3_RUN;
//								}else {
//									  revo_state=REVO_READY_TO_USE;
//									  }


//									if (revo_op_cmd.cmd.GRIND_HOP2==1){
//										   revo_state=REVO_GRINDING_START;
//									  }else{
//										   revo_state=REVO_READY_TO_USE;
//										  }

								  break;

	    case(REVO_WEIGHT2_ERROR):

	 	   	    	              break;

	    case(REVO_WEIGHT3_RUN):
		                          //printf("\n\r Run Weigh3:%d",balance.state);
	                              //printf("\n\r Weight:%4ld",balance.weight_rd);
	 	   	    	              break;

	 	case(REVO_WEIGHT3_END):
		                          //printf("\n\r End Weigh3");
	 	                          //printf("\n\r Weight:%4ld",balance.weight_rd);
	 	                          revo_state=REVO_WEIGHT3_END;
	 	                          if (cmd_rx_timeout==0){
		                              revo_op_cmd.cmd.WEIGH_HOP3=0;
	 	                              revo_state=REVO_READY_TO_USE;
	 	                          }
//		                          if (revo_op_cmd.cmd.GRIND_HOP3==1){
//									 revo_state=REVO_GRINDING_START;
//								  }else{
//									   revo_state=REVO_READY_TO_USE;
//									  }
	 	   	    	              break;

	 	case(REVO_WEIGHT3_ERROR):

	 	   	    	              break;

	    case(REVO_BURR_POS_RUN):
	 		                          //printf("\n\r Set Burr");

	 		                          revo_state=REVO_BURR_POS_RUN;//REVO_GRINDING_RUN;

	 	 	   	    	              break;

	    case(REVO_GRINDING_START):
		                          printf("\n\r Grinding-Start");
                                  /*Inverter command to start grinding*/
		                          //if((revo_op_cmd.cmd.GRIND_HOP1==1)||(revo_op_cmd.cmd.GRIND_HOP2==1)||(revo_op_cmd.cmd.GRIND_HOP3==1)){
		                          revo_state=REVO_GRINDING_RUN;
		                          grinder.inverter_status=INV_GRINDING_SIZE_SETBURR;//INV_POWERON_STATE;
		                          //}else{
		                          //	   revo_state=REVO_READY_TO_USE;
		                          //}
		                          //vibro_external_drive();
	 	   	    	              break;

	    case(REVO_GRINDING_RUN):
		                          //vibro_external_drive();
	                        	  //printf("\n\r Grind Run:%d",grinder.inverter_status);
		                          //printf("\n\r INV. Power:%d [W]",grinder.input_reg[4]);
	                              //printf("\n\r CountDown Grind:%d[100ms]",grinder.time_grinding);
	 	   	    	              break;

	    case(REVO_GRINDING_END):
		                              //printf("\n\r EndGrind.Cmd:%d",revo_op_cmd.cmd_reg16);

		                              revo_state=REVO_GRINDING_END;
		                              if (cmd_rx_timeout==0){
										  revo_op_cmd.cmd.GRIND_HOP1=0;
										  revo_op_cmd.cmd.GRIND_HOP2=0;
										  revo_op_cmd.cmd.GRIND_HOP3=0;
										  //printf("\n\r BeforeReady.Cmd:%d",revo_op_cmd.cmd_reg16);

										  if(hopper_burr[HOPPER_1].cmd==IDLE_STEPPER){
											  if(hopper_burr[HOPPER_2].cmd==IDLE_STEPPER){
												 if(hopper_burr[HOPPER_3].cmd==IDLE_STEPPER){
													balance.weight_rd=0;
													balance.time_target=0;
													balance_update_weight_act(balance.weight_rd) ;
													printf("\n\r End Grind:%d",grinder.inverter_status);
													revo_state=REVO_READY_TO_USE;
												 }
											  }
										  }
		                              }
	 	   	    	              break;



	    case(REVO_BURR_POS_CAL_RUN):
                                       revo_state=REVO_BURR_POS_CAL_RUN;
		                               stepper_burr_find_zero(BURR_0);
		                               revo_state=REVO_BURR_POS_CAL_END;
	  	 	   	    	              break;

	    case(REVO_BURR_POS_CAL_END):
		                               while(revo_calibration_cmd.cal_b.BURR_POS_CAL_START!=0){//Start Burr Zero Positioning
		                            	    revo_state=REVO_BURR_POS_CAL_END;
				                            mdb_comm_processMessage();/*Modbus poll update in each run*/
									   }
				                       revo_calibration_cmd.cal_b.BURR_POS_CAL_START=0;
			                           revo_state=REVO_READY_TO_USE;
	  	 	   	    	              break;

	    case(REVO_WEIGHT_POS_CAL_RUN):

		                              revo_state=REVO_WEIGHT_POS_CAL_RUN;
	  	 	   	    	              break;

	    case(REVO_WEIGHT_POS_CAL_END):

	  		                          revo_state=REVO_READY_TO_USE;
	  	 	   	    	              break;


	    case(REVO_GRINDING_ERROR):

	  	 	   	    	              break;


	    case(REVO_SERVICE_MODE):
		                              if (revo_op_mode.bit.service==0){//RUN Mode
		                            	  revo_state=REVO_READY_TO_USE;
		                              }else{//SERVICE Mode
		                            	  revo_state=REVO_SERVICE_MODE;
		                              }

	    		                      break;

	    default:
	    	    revo_state=REVO_IDLE;
	    	    break;
	}

//	 if (revo_state>REVO_IDLE){
//		  hopper_burr_manager();
//	 }
//
//	 if (revo_state>REVO_SELF_TEST_RUN){
//	    	 balance_weight_manager();
//			 doser_vibro_manager();
//			 fan_manager();
//			 grinder_manager();
//	    }
}














/* Private user code ---------------------------------------------------------*/


