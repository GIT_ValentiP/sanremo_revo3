/*
 *****************************************************************************
 *  @file      : st_l6474_stepper.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 21 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_ST_L6474_MOTOR_DRIVER_ST_L6474_STEPPER_H_
#define INC_ST_L6474_MOTOR_DRIVER_ST_L6474_STEPPER_H_
/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"


/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void init_stepper(void);
void init_encoder_burr(void);
void read_sensor_hall(void);
void read_portafilter(void);
void read_encoder_pos(void);
void stepper_clock_STCK(void);//{(stepper_enum_t id,uint16_t new_speed);
void calibration_burr(uint32_t step,BurrDir_t dir,uint16_t torque_val,uint16_t speed);//{(stepper_enum_t id,uint16_t new_speed);
void stepper_burr_position(void);
void hopper_burr_manager(void);
void stepper_burr_stall(float Tval);
void stepper_find_zero(stepper_enum_t id);
void stepper_burr_find_zero(stepper_enum_t id);
#endif /* INC_ST_L6474_MOTOR_DRIVER_ST_L6474_STEPPER_H_ */
