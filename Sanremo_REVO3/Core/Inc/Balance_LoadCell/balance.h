/*
 *****************************************************************************
 *  @file      : balance.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 21 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_BALANCE_LOADCELL_BALANCE_H_
#define INC_BALANCE_LOADCELL_BALANCE_H_
/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"


/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/
#define ACK_BALANCE              0x06 //ACK from balance command received without error
#define NACK_BALANCE             0x15 //NACK from balance re-send command
#define START1_WEIGHT_FRAME      0xCA
#define START2_WEIGHT_FRAME      0xFE
#define END_WEIGHT_FRAME         0x04


#define END1_HBM_FRAME         0x0D
#define END2_HBM_FRAME         0x0A

#define LEN_WEIGHT_FRAME_BALANCE         0x06
#define LEN_DATA_FRAME_BALANCE           0x03 //3 bytes for weight in [mg]



#define RS422_TX_CELL_ENABLE     HAL_GPIO_WritePin(RS485_CELLA_RTS_GPIO_Port,RS485_CELLA_RTS_Pin,GPIO_PIN_SET)
#define RS422_TX_CELL_DISABLE    HAL_GPIO_WritePin(RS485_CELLA_RTS_GPIO_Port,RS485_CELLA_RTS_Pin,GPIO_PIN_RESET)
#define RS422_RX_CELL_ENABLE     HAL_GPIO_WritePin(RS485_CELLA_CTS_GPIO_Port,RS485_CELLA_CTS_Pin,GPIO_PIN_RESET)
#define RS422_RX_CELL_DISABLE    HAL_GPIO_WritePin(RS485_CELLA_CTS_GPIO_Port,RS485_CELLA_CTS_Pin,GPIO_PIN_SET)

#define TOLERANCE_WEIGHT_BALANCE   1 //tolerance in [gr/10]
#define PESO_MAX_MG  500000 //peso massimo ricevibile  da bilancia [mg].

#define TIMEOUT_WEIGHING 58//168//200 //timeout in 100ms unit for weighing procedure




/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/


uint8_t buffer_RX_balance[LEN_WEIGHT_FRAME_BALANCE];
uint8_t buffer_TX_balance[LEN_WEIGHT_FRAME_BALANCE];

/* Function prototypes -----------------------------------------------*/
void check_rx_balance(void);
void init_balance(void);
void scan_baudrate_balance(void);
bool balance_RX_IT(void);

//uint32_t balance_read_weight(void);
//void balance_send_cmd(balance_CMD_enum_t cmd,uint16_t len);//{(stepper_enum_t id,uint16_t new_speed);
void balance_update_weight_act(uint16_t wgt);
bool balance_weighing_procedure(doser_enum_t id_doser,uint16_t accuracy,uint16_t target);
void balance_weight_manager(void);


#endif /* INC_BALANCE_LOADCELL_BALANCE_H_ */
