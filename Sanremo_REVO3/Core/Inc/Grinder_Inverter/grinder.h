/*
 ******************************************************************************
 *  @file      : grinder.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 07 ott 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_GRINDER_INVERTER_GRINDER_H_
#define INC_GRINDER_INVERTER_GRINDER_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"
/* Typedef -----------------------------------------------------------*/


#define INV_SETSTATE_ADR         0
#define INV_ROTATION_ADR         1
#define INV_LIMITATION_ADR       2
#define INV_UNLOCKSTART_ADR      3
#define INV_RESET_ADR            4
#define INV_MODE_ADR             5
#define INV_PWRDERAT_ADR         6
#define INV_SOFTST_ADR           7
#define INV_SOFTSTONALARM_ADR    8
#define INV_SOFTSTHZCHG_ADR      9
#define INV_PWRBOOST_ADR        10


#define INV_INPUT_HZREAL_ADR            0
#define INV_INPUT_VOUT_REAL_ADR         1
#define INV_INPUT_ELAPSE_TIME_ADR       2
#define INV_INPUT_VOLT_ADR              3
#define INV_INPUT_POWER_ADR             4
#define INV_INPUT_COUNTOV_ADR           5
#define INV_INPUT_TEMP_ADR             12




#define READ_DISCRETE_COIL          0x02
#define WRITE_SINGLE_COIL           0x05
#define READ_INPUT_REGISTERS        0x04
#define READ_HOLDING_REGISTERS      0x03
#define	PRESET_MULTIPLE_REGISTERS   0x10
#define WRITE_MULTIPLE_COIL         0x0F

#define INVERTER_STATE_ON     0x01
#define INVERTER_STATE_OFF    0x00

#define INVERTER_ROT_FORWARD     0x00
#define INVERTER_ROT_REVERSE     0x01

#define GRINDING_TIME_MAX       10000// 7000//11000 //time grinding  [ms]

typedef struct
{
  // specific packet info
  unsigned char id;
  unsigned char function;
  unsigned int address;
  unsigned int no_of_registers;
  unsigned int* register_array;

  // modbus information counters
  unsigned int requests;
  unsigned int successful_requests;
  unsigned long total_errors;
  unsigned int retries;
  unsigned int timeout;
  unsigned int incorrect_id_returned;
  unsigned int incorrect_function_returned;
  unsigned int incorrect_bytes_returned;
  unsigned int checksum_failed;
  unsigned int buffer_errors;

  // modbus specific exception counters
  unsigned int illegal_function;
  unsigned int illegal_data_address;
  unsigned int illegal_data_value;
	unsigned char misc_exceptions;

  // connection status of packet
  unsigned char connection;

}Packet;

typedef Packet* packetPointer;



/* Define ------------------------------------------------------------*/

#define GRINDER_RS485_BAUDRATE         9600
#define GRINDER_SLAVE_ADR_MDB             1


#define RS485_TX_GRIND_ENABLE     HAL_GPIO_WritePin(RS485_OPTO_MACINA_RTS_GPIO_Port,RS485_OPTO_MACINA_RTS_Pin,GPIO_PIN_SET)
#define RS485_TX_GRIND_DISABLE    HAL_GPIO_WritePin(RS485_OPTO_MACINA_RTS_GPIO_Port,RS485_OPTO_MACINA_RTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_GRIND_ENABLE     HAL_GPIO_WritePin(RS485_OPTO_MACINA_CTS_GPIO_Port,RS485_OPTO_MACINA_CTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_GRIND_DISABLE    HAL_GPIO_WritePin(RS485_OPTO_MACINA_CTS_GPIO_Port,RS485_OPTO_MACINA_CTS_Pin,GPIO_PIN_SET)


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
// function definitions
unsigned int modbus_update(Packet* packets);
void modbus_configure(long baud, unsigned int _timeout, unsigned int _polling,
											unsigned char _retry_count, unsigned char _TxEnablePin,
											Packet* packets, unsigned int _total_no_of_packets);
void init_grinder(void);
void grinder_send_msg(uint8_t *buf_TX,uint16_t len);
void grinder_read_msg(void);
void grinder_read_register(uint8_t nbytes);
void grinder_write_register(uint8_t op_code,uint16_t start_adr,uint16_t *value_buf,uint8_t nreg);
void grinder_manager(void);


#endif /* INC_GRINDER_INVERTER_GRINDER_H_ */
