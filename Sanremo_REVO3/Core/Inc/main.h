/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */




/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
/*ERROR LIST*/
#define SYSTEM_DMA_INIT_ERROR                                      0x0000
#define SYSTEM_RCC_CONFIG_ERROR                                    0x0001
#define SYSTEM_RCC_OVER_DRIVE_ERROR                                0x0002
#define SYSTEM_RCC_CLK_CONFIG_ERROR                                0x0003
#define SYSTEM_RCC_PERIPH_CLK_CONFIG_ERROR                         0x0004


#define SYSTEM_UART1_CONFIG_ERROR                                0x0004
#define SYSTEM_UART2_CONFIG_ERROR                                0x0005
#define SYSTEM_UART3_CONFIG_ERROR                                0x0006
#define SYSTEM_UART4_CONFIG_ERROR                                0x0007
#define SYSTEM_UART5_CONFIG_ERROR                                0x0008
#define SYSTEM_UART6_CONFIG_ERROR                                0x0009


#define SYSTEM_DMA_UART1_CONFIG_ERROR                                0x000A
#define SYSTEM_DMA_UART2_CONFIG_ERROR                                0x000B
#define SYSTEM_DMA_UART3_CONFIG_ERROR                                0x000C
#define SYSTEM_DMA_UART4_CONFIG_ERROR                                0x000D
#define SYSTEM_DMA_UART5_CONFIG_ERROR                                0x000E
#define SYSTEM_DMA_UART6_CONFIG_ERROR                                0x000F


#define SYSTEM_ADC_CONFIG_1_ERROR                                     0x0010
#define SYSTEM_ADC_CONFIG_2_ERROR                                     0x0011
#define SYSTEM_ADC_CONFIG_3_ERROR                                     0x0012
#define SYSTEM_ADC_CONFIG_4_ERROR                                     0x0013
#define SYSTEM_ADC_CONFIG_5_ERROR                                     0x0014
#define SYSTEM_ADC_CONFIG_6_ERROR                                     0x0015
#define SYSTEM_ADC_CONFIG_7_ERROR                                     0x0016
#define SYSTEM_ADC_CONFIG_8_ERROR                                     0x0017
#define SYSTEM_ADC_CONFIG_9_ERROR                                     0x0018
#define SYSTEM_ADC_CONFIG_10_ERROR                                     0x0019

#define SYSTEM_CRC_CONFIG_1_ERROR                                     0x0020

#define SYSTEM_I2C_CONFIG_1_ERROR                                     0x0030
#define SYSTEM_I2C_CONFIG_2_ERROR                                     0x0031
#define SYSTEM_I2C_CONFIG_3_ERROR                                     0x0032
#define SYSTEM_I2C_CONFIG_4_ERROR                                     0x0033
#define SYSTEM_I2C_CONFIG_5_ERROR                                     0x0034
#define SYSTEM_I2C_CONFIG_6_ERROR                                     0x0035
#define SYSTEM_I2C_CONFIG_7_ERROR                                     0x0036
#define SYSTEM_I2C_CONFIG_8_ERROR                                     0x0037
#define SYSTEM_I2C_CONFIG_9_ERROR                                     0x0038
#define SYSTEM_I2C_CONFIG_10_ERROR                                     0x0039
#define SYSTEM_I2C_CONFIG_11_ERROR                                     0x003A
#define SYSTEM_I2C_CONFIG_12_ERROR                                     0x003B


#define SYSTEM_IWDG_CONFIG_1_ERROR                                    0x0040
#define SYSTEM_IWDG_CONFIG_2_ERROR                                    0x0041

#define SYSTEM_RTC_CONFIG_1_ERROR                                     0x0050
#define SYSTEM_RTC_CONFIG_2_ERROR                                     0x0051
#define SYSTEM_RTC_CONFIG_3_ERROR                                     0x0052

#define SYSTEM_SPI_CONFIG_1_ERROR                                     0x0060
#define SYSTEM_SPI_CONFIG_2_ERROR                                     0x0061
#define SYSTEM_SPI_CONFIG_3_ERROR                                     0x0062

#define SYSTEM_TIM_CONFIG_1_ERROR                                     0x0070
#define SYSTEM_TIM_CONFIG_2_ERROR                                     0x0071
#define SYSTEM_TIM_CONFIG_3_ERROR                                     0x0072
#define SYSTEM_TIM_CONFIG_4_ERROR                                     0x0073
#define SYSTEM_TIM_CONFIG_5_ERROR                                     0x0074
#define SYSTEM_TIM_CONFIG_6_ERROR                                     0x0075
#define SYSTEM_TIM_CONFIG_7_ERROR                                     0x0076
#define SYSTEM_TIM_CONFIG_8_ERROR                                     0x0077
#define SYSTEM_TIM_CONFIG_9_ERROR                                     0x0078
#define SYSTEM_TIM_CONFIG_10_ERROR                                    0x0079
#define SYSTEM_TIM_CONFIG_11_ERROR                                    0x007A
#define SYSTEM_TIM_CONFIG_12_ERROR                                    0x007B
#define SYSTEM_TIM_CONFIG_13_ERROR                                    0x007C
#define SYSTEM_TIM_CONFIG_14_ERROR                                     0x007E
#define SYSTEM_TIM_CONFIG_15_ERROR                                     0x007F
#define SYSTEM_TIM_CONFIG_16_ERROR                                     0x0080
#define SYSTEM_TIM_CONFIG_17_ERROR                                     0x0081
#define SYSTEM_TIM_CONFIG_18_ERROR                                     0x0082
#define SYSTEM_TIM_CONFIG_19_ERROR                                     0x0083
#define SYSTEM_TIM_CONFIG_20_ERROR                                     0x0084
#define SYSTEM_TIM_CONFIG_21_ERROR                                     0x0085
#define SYSTEM_TIM_CONFIG_22_ERROR                                     0x0086
#define SYSTEM_TIM_CONFIG_23_ERROR                                     0x0087
#define SYSTEM_TIM_CONFIG_24_ERROR                                     0x0088
#define SYSTEM_TIM_CONFIG_25_ERROR                                    0x0089
#define SYSTEM_TIM_CONFIG_26_ERROR                                    0x008A
#define SYSTEM_TIM_CONFIG_27_ERROR                                    0x008B
#define SYSTEM_TIM_CONFIG_28_ERROR                                    0x008C
#define SYSTEM_TIM_CONFIG_29_ERROR                                    0x008D
#define SYSTEM_TIM_CONFIG_30_ERROR                                    0x008E
#define SYSTEM_TIM_CONFIG_31_ERROR                                    0x008F

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(uint16_t error);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SENSOR_RHT_INT_ADR 0x4A
#define SENR_RHT_EXT_ADR 0x4B



/*SPI4 DOSER+VIBRO */
#define SPI4_MISO_DOSER_Pin 		GPIO_PIN_5
#define SPI4_MISO_DOSER_GPIO_Port 	GPIOE
#define SPI4_MOSI_DOSER_Pin 		GPIO_PIN_6
#define SPI4_MOSI_DOSER_GPIO_Port 	GPIOE
#define SPI4_SCK_DOSER_Pin          GPIO_PIN_2
#define SPI4_SCK_DOSER_GPIO_Port    GPIOE

#define SPI4_NSS_DOSER1_Pin 		GPIO_PIN_4
#define SPI4_NSS_DOSER1_GPIO_Port 	GPIOE
#define SPI4_NSS_DOSER2_Pin         GPIO_PIN_3
#define SPI4_NSS_DOSER2_GPIO_Port 	GPIOE
#define SPI4_NSS_DOSER3_Pin 		GPIO_PIN_13
#define SPI4_NSS_DOSER3_GPIO_Port   GPIOC
#define SPI4_NSS_VIBRO_Pin 			GPIO_PIN_10
#define SPI4_NSS_VIBRO_GPIO_Port 	GPIOC


/*DOSER GPIO*/
#define DIR_DOSER1_Pin              GPIO_PIN_7
#define DIR_DOSER1_GPIO_Port        GPIOE
#define DIS_DOSER1_Pin              GPIO_PIN_1
#define DIS_DOSER1_GPIO_Port        GPIOG

#define DIR_DOSER2_Pin              GPIO_PIN_8
#define DIR_DOSER2_GPIO_Port        GPIOD
#define DIS_DOSER2_Pin 				GPIO_PIN_9
#define DIS_DOSER2_GPIO_Port 		GPIOD

#define DIR_DOSER3_Pin 				GPIO_PIN_10
#define DIR_DOSER3_GPIO_Port 		GPIOD
#define DIS_DOSER3_Pin 				GPIO_PIN_14
#define DIS_DOSER3_GPIO_Port 		GPIOG
/*VIBRO GPIO*/
#define DIR_VIBRO_Pin 				GPIO_PIN_15
#define DIR_VIBRO_GPIO_Port 		GPIOD
#define DIS_VIBRO_Pin			    GPIO_PIN_2
#define DIS_VIBRO_GPIO_Port 		GPIOG

#define PWM_DOSER1_Pin 				GPIO_PIN_5
#define PWM_DOSER1_GPIO_Port 		GPIOB
#define PWM_DOSER2_Pin 				GPIO_PIN_8
#define PWM_DOSER2_GPIO_Port 		GPIOC
#define PWM_DOSER3_Pin              GPIO_PIN_1
#define PWM_DOSER3_GPIO_Port        GPIOB
#define PWM_VIBRO_Pin 				GPIO_PIN_14
#define PWM_VIBRO_GPIO_Port 		GPIOD



/*SPI2 ENCODER MACINA*/
#define SPI2_MOSI_ENCODER_MAC_Pin 	GPIO_PIN_1
#define SPI2_MOSI_ENCODER_MAC_GPIO_Port GPIOC
#define SPI2_MISO_ENCODER_MAC_Pin 	GPIO_PIN_2
#define SPI2_MISO_ENCODER_MAC_GPIO_Port GPIOC
#define SPI2_CLK_ENCODER_MAC_Pin 	GPIO_PIN_3
#define SPI2_CLK_ENCODER_MAC_GPIO_Port GPIOD
#define SPI2_NSS_ENCODER_MAC_Pin 	GPIO_PIN_9
#define SPI2_NSS_ENCODER_MAC_GPIO_Port GPIOB


/*SPI1  STEPPER*/
#define SPI1_MISO_STEPPER_Pin       GPIO_PIN_6
#define SPI1_MISO_STEPPER_GPIO_Port GPIOA
#define SPI1_MOSI_STEPPER_Pin       GPIO_PIN_7
#define SPI1_MOSI_STEPPER_GPIO_Port GPIOA
#define SPI1_SCK_STEPPER_Pin        GPIO_PIN_5
#define SPI1_SCK_STEPPER_GPIO_Port  GPIOA

#define SPI1_NSS_STEPPER1_Pin       GPIO_PIN_4
#define SPI1_NSS_STEPPER1_GPIO_Port GPIOA
#define SPI1_NSS_STEPPER2_Pin 	    GPIO_PIN_11
#define SPI1_NSS_STEPPER2_GPIO_Port GPIOC
#define SPI1_NSS_STEPPER3_Pin 		GPIO_PIN_6
#define SPI1_NSS_STEPPER3_GPIO_Port GPIOD
#define SPI1_NSS_STEPPER4_Pin       GPIO_PIN_0
#define SPI1_NSS_STEPPER4_GPIO_Port GPIOG


/*STEPPER GPIO*/
#define SYNC_STEPPER1_Pin 			GPIO_PIN_7
#define SYNC_STEPPER1_GPIO_Port 	GPIOD
#define FLAG_STEPPER1_Pin 			GPIO_PIN_9
#define FLAG_STEPPER1_GPIO_Port 	GPIOG
#define FLAG_STEPPER1_EXTI_IRQn     EXTI9_5_IRQn
#define STBY_RST_STEPPER1_Pin 		GPIO_PIN_10
#define STBY_RST_STEPPER1_GPIO_Port GPIOG
#define STCK_STEPPER1_Pin 			GPIO_PIN_11
#define STCK_STEPPER1_GPIO_Port 	GPIOG
#define DIR_STEPPER1_Pin 			GPIO_PIN_12
#define DIR_STEPPER1_GPIO_Port 		GPIOG

#define SYNC_STEPPER2_Pin 			GPIO_PIN_12
#define SYNC_STEPPER2_GPIO_Port 	GPIOC
#define FLAG_STEPPER2_Pin 			GPIO_PIN_0
#define FLAG_STEPPER2_GPIO_Port 	GPIOD
#define FLAG_STEPPER2_EXTI_IRQn 	EXTI0_IRQn
#define STBY_RST_STEPPER2_Pin 		GPIO_PIN_1
#define STBY_RST_STEPPER2_GPIO_Port GPIOD
#define STCK_STEPPER2_Pin 			GPIO_PIN_2
#define STCK_STEPPER2_GPIO_Port 	GPIOD
#define DIR_STEPPER2_Pin 			GPIO_PIN_4
#define DIR_STEPPER2_GPIO_Port 		GPIOD

#define SYNC_STEPPER3_Pin 			GPIO_PIN_3
#define SYNC_STEPPER3_GPIO_Port 	GPIOG
#define FLAG_STEPPER3_Pin 			GPIO_PIN_4
#define FLAG_STEPPER3_GPIO_Port 	GPIOG
#define FLAG_STEPPER3_EXTI_IRQn 	EXTI4_IRQn
#define STBY_RST_STEPPER3_Pin 		GPIO_PIN_5
#define STBY_RST_STEPPER3_GPIO_Port GPIOG
#define STCK_STEPPER3_Pin 			GPIO_PIN_6
#define STCK_STEPPER3_GPIO_Port 	GPIOG
#define DIR_STEPPER3_Pin 			GPIO_PIN_7
#define DIR_STEPPER3_GPIO_Port 		GPIOG
/*STEPPER4 POSIZIONAMENTO MACINE GPIO*/
#define DIR_BURR_STEPPER4_Pin 		GPIO_PIN_6
#define DIR_BURR_STEPPER4_GPIO_Port GPIOF
#define STCK_BURR_STEPPER4_Pin 		GPIO_PIN_7
#define STCK_BURR_STEPPER4_GPIO_Port GPIOF
#define STBY_RST_BURR_STEPPER4_Pin 	GPIO_PIN_8
#define STBY_RST_BURR_STEPPER4_GPIO_Port GPIOF
#define FLAG_BURR_STEPPER4_Pin 	 	GPIO_PIN_9
#define FLAG_BURR_STEPPER4_GPIO_Port GPIOF
#define SYNC_BURR_STEPPER4_Pin GPIO_PIN_10
#define SYNC_BURR_STEPPER4_GPIO_Port GPIOF


/*FAN1  + FAN2*/
#define POWER_FAN2_Pin              GPIO_PIN_4
#define POWER_FAN2_GPIO_Port        GPIOC
#define POWER_FAN1_Pin              GPIO_PIN_5
#define POWER_FAN1_GPIO_Port        GPIOC
#define PWM_FAN1_Pin                GPIO_PIN_9
#define PWM_FAN1_GPIO_Port          GPIOE
#define PWM_FAN2_Pin                GPIO_PIN_11
#define PWM_FAN2_GPIO_Port          GPIOE

#define TACHI_FAN1_Pin              GPIO_PIN_13
#define TACHI_FAN1_GPIO_Port        GPIOE
#define TACHI_FAN2_Pin              GPIO_PIN_14
#define TACHI_FAN2_GPIO_Port        GPIOE


/*HALL SENSORs DOSER*/
#define SENS_OPEN_HALL1_Pin         GPIO_PIN_2
#define SENS_OPEN_HALL1_GPIO_Port   GPIOB
#define SENS_CLOSE_HALL1_Pin        GPIO_PIN_11
#define SENS_CLOSE_HALL1_GPIO_Port  GPIOF
#define SENS_OPEN_HALL2_Pin         GPIO_PIN_12
#define SENS_OPEN_HALL2_GPIO_Port   GPIOF
#define SENS_CLOSE_HALL2_Pin        GPIO_PIN_13
#define SENS_CLOSE_HALL2_GPIO_Port  GPIOF
#define SENS_OPEN_HALL3_Pin         GPIO_PIN_14
#define SENS_OPEN_HALL3_GPIO_Port   GPIOF
#define SENS_CLOSE_HALL3_Pin        GPIO_PIN_15
#define SENS_CLOSE_HALL3_GPIO_Port  GPIOF

/*SENSORs PORTA FILTRO-PRESSINO*/
#define CTRL_IN_P_FILTRO_Pin        GPIO_PIN_12
#define CTRL_IN_P_FILTRO_GPIO_Port  GPIOE
#define CTRL_OUT_P_FILTRO_Pin       GPIO_PIN_15
#define CTRL_OUT_P_FILTRO_GPIO_Port GPIOE

/*I2C1  TEMPERATURE HUMIDITY SENSOR*/
#define I2C1_SCL_SENSOR_Pin 		GPIO_PIN_6
#define I2C1_SCL_SENSOR_GPIO_Port 	GPIOB
#define I2C1_SDA_SENSOR_Pin 		GPIO_PIN_7
#define I2C1_SDA_SENSOR_GPIO_Port 	GPIOB

/*I2C2 EEPROM*/
#define I2C2_SDA_EEPROM_Pin 		GPIO_PIN_0
#define I2C2_SDA_EEPROM_GPIO_Port 	GPIOF
#define I2C2_SCL_EEPROM_Pin 		GPIO_PIN_1
#define I2C2_SCL_EEPROM_GPIO_Port 	GPIOF

/*MICRO_SWITCH_DOSER*/
#define MICRO_DOSER1_Pin 			GPIO_PIN_2
#define MICRO_DOSER1_GPIO_Port	    GPIOF
#define MICRO_DOSER2_Pin 			GPIO_PIN_3
#define MICRO_DOSER2_GPIO_Port 		GPIOF
#define MICRO_DOSER3_Pin 			GPIO_PIN_4
#define MICRO_DOSER3_GPIO_Port 		GPIOF

/*ANALOGICI*/
#define ADC3_AN15_ANALOG_Pin 		GPIO_PIN_5
#define ADC3_AN15_ANALOG_GPIO_Port 	GPIOF
#define ADC1_AN10_ENCODER_Pin 		GPIO_PIN_0
#define ADC1_AN10_ENCODER_GPIO_Port GPIOC
#define ADC1_AN13_NTC_Pin 			GPIO_PIN_3
#define ADC1_AN13_NTC_GPIO_Port 	GPIOC


/*LEDs SERVICE*/
#define LED1_SERVICE_Pin 			GPIO_PIN_8
#define LED1_SERVICE_GPIO_Port 		GPIOB
#define FLAG_STEPPER3_EXTI_IRQn EXTI4_IRQn
#define LED2_SERVICE_Pin 			GPIO_PIN_15
#define LED2_SERVICE_GPIO_Port 		GPIOG
#define LED3_SERVICE_Pin 			GPIO_PIN_5
#define LED3_SERVICE_GPIO_Port 		GPIOD
#define LED4_SERVICE_Pin            GPIO_PIN_10
#define LED4_SERVICE_GPIO_Port      GPIOE
#define LED5_SERVICE_Pin            GPIO_PIN_8
#define LED5_SERVICE_GPIO_Port      GPIOE
#define LED6_SERVICE_Pin            GPIO_PIN_0
#define LED6_SERVICE_GPIO_Port      GPIOB


/*ILLUMINAZIONE PWM*/
#define PWM_ILLUMINA_Pin 			GPIO_PIN_13
#define PWM_ILLUMINA_GPIO_Port 		GPIOD



/*RS485 INVERTER OPTOSISOLATA UART1*/
#define RS485_OPTO_MACINA_TX_Pin 	GPIO_PIN_9
#define RS485_OPTO_MACINA_TX_GPIO_Port GPIOA
#define RS485_OPTO_MACINA_RX_Pin 	GPIO_PIN_10
#define RS485_OPTO_MACINA_RX_GPIO_Port GPIOA
#define RS485_OPTO_MACINA_CTS_Pin 	GPIO_PIN_11
#define RS485_OPTO_MACINA_CTS_GPIO_Port GPIOA
#define RS485_OPTO_MACINA_RTS_Pin 	GPIO_PIN_12
#define RS485_OPTO_MACINA_RTS_GPIO_Port GPIOA
#define USART1_DIR_GPIO_Port        RS485_OPTO_MACINA_CTS_GPIO_Port
#define USART1_DIR_Pin              RS485_OPTO_MACINA_CTS_Pin

/*RS485  CELLA CARICO UART2*/
#define RS485_CELLA_CTS_Pin 		GPIO_PIN_0
#define RS485_CELLA_CTS_GPIO_Port 	GPIOA
#define RS485_CELLA_RTS_Pin 		GPIO_PIN_1
#define RS485_CELLA_RTS_GPIO_Port 	GPIOA
#define RS485_CELLA_TX_Pin 			GPIO_PIN_2
#define RS485_CELLA_TX_GPIO_Port 	GPIOA
#define FLAG_STEPPER2_EXTI_IRQn EXTI0_IRQn
#define RS485_CELLA_RX_Pin 			GPIO_PIN_3
#define RS485_CELLA_RX_GPIO_Port 	GPIOA


/*RS232 DEBUG UART3*/
#define RS232_DBG_TX_Pin            GPIO_PIN_10
#define RS232_DBG_TX_GPIO_Port      GPIOB
#define RS232_DBG_RX_Pin            GPIO_PIN_11
#define RS232_DBG_RX_GPIO_Port      GPIOB
#define RS232_DBG_CTS_Pin 			GPIO_PIN_11
#define RS232_DBG_CTS_GPIO_Port 	GPIOD
#define RS232_DBG_RTS_Pin 			GPIO_PIN_12
#define RS232_DBG_RTS_GPIO_Port 	GPIOD


/*RS485 HMI UART6*/
#define RS485_HMI_RTS_Pin 			GPIO_PIN_8 //RS485 DE pin
#define RS485_HMI_RTS_GPIO_Port 	GPIOG
#define RS485_HMI_TX_Pin 			GPIO_PIN_6
#define RS485_HMI_TX_GPIO_Port 		GPIOC
#define RS485_HMI_RX_Pin 			GPIO_PIN_7
#define RS485_HMI_RX_GPIO_Port 		GPIOC
#define RS485_HMI_CTS_Pin 			GPIO_PIN_13
#define RS485_HMI_CTS_GPIO_Port 	GPIOG





/*I2C3 Service*/
#define I2C3_SDA_SERVICE_Pin 		GPIO_PIN_9
#define I2C3_SDA_SERVICE_GPIO_Port  GPIOC
#define I2C3_SCL_SERVICE_Pin 		GPIO_PIN_8
#define I2C3_SCL_SERVICE_GPIO_Port 	GPIOA

/*UART8  MONITOR SERVICE*/
#define UART8_RX_SERVICE_Pin 		GPIO_PIN_0
#define UART8_RX_SERVICE_GPIO_Port 	GPIOE
#define UART8_TX_SERVICE_Pin 		GPIO_PIN_1
#define UART8_TX_SERVICE_GPIO_Port 	GPIOE

#define LED8_SERVICE_GPIO_Port      GPIOE//UART8_RX_SERVICE_GPIO_Port
#define LED8_SERVICE_Pin            GPIO_PIN_0//UART8_RX_SERVICE_Pin
#define LED9_SERVICE_GPIO_Port      GPIOE//UART8_TX_SERVICE_GPIO_Port
#define LED9_SERVICE_Pin            GPIO_PIN_1//UART8_TX_SERVICE_Pin

//						HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port,
/* USER CODE BEGIN Private defines */
#define PWM_FAN1_CHANNEL                        0x00000000U                          /*!< Capture/compare channel 1 identifier      */
#define PWM_FAN2_CHANNEL                        0x00000004U
#define PWM_LED6_CHANNEL                        0x00000004U                          /*!< Capture/compare channel 2 identifier      */
#define PWM_DOSER1_CHANNEL                      0x00000004U
#define PWM_DOSER2_CHANNEL                      0x00000008U
#define PWM_DOSER3_CHANNEL                      0x0000000CU
#define PWM_ILLUMINA_CHANNEL                    0x00000004U
#define PWM_VIBRO_CHANNEL                       0x00000008U

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
