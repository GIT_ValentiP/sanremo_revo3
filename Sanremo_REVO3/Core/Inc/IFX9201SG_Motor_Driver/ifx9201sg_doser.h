/*
 ******************************************************************************
 *  @file      : ifx9201sg_doser.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 20 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_IFX9201SG_MOTOR_DRIVER_IFX9201SG_DOSER_H_
#define INC_IFX9201SG_MOTOR_DRIVER_IFX9201SG_DOSER_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"
//#include "tim.h"
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/
//IFX9201SG DIAGNOSIS REGISTER FAILURE
#define DOSER_NO_ERR                         0x0F //No failure 1 1 1 1 0xF -
#define DOSER_SHORT_GND_OUT1_ERR             0x0E //Short to GND at OUT1 (SCG1) 1 1 1 0 0xE latched
#define DOSER_SHORT_VS_OUT1_ERR              0x0D // Short to VS at OUT1 (SCVS1) 1 1 0 1 0xD latched
#define DOSER_OPEN_LOAD_ERR                  0x0C //Open Load (OL) 1 1 0 0 0xC not latched
#define DOSER_SHORT_GND_OUT2_ERR             0x0B //Short to GND at OUT2 (SCG2) 1 0 1 1 0xB latched
#define DOSER_SHORT_GND_OUT1_2_ERR           0x0A //Short to GND at OUT1 and OUT2 (SCG1, SCG2) 1 0 1 0 0xA latched
#define DOSER_SHORT_GND_OUT2_VS_OUT1_ERR     0x09 //Short to VS at OUT1 and short to GND at OUT2 (SCVS1, SCG2) 1 0 0 1 0x9 latched
#define DOSER_SHORT_VS_OUT2_ERR              0x07 //Short to Supply at OUT2 (SCVS2) 0 1 1 1 0x7 latched
#define DOSER_SHORT_GND_OUT1_VS_OUT2_ERR     0x06 //Short to GND at OUT1 and short to VS at OUT2 (SCG1, SCVS2) 0 1 1 0 0x6 latched
#define DOSER_SHORT_VS_OUT1_2_ERR            0x05 //Short to VS at OUT1 and OUT2 (SCVS1, SCVS2) 0 1 0 1 0x5 latched
#define DOSER_VS_UNDERV_ERR                  0x03 //VS Undervoltage (VS_UV) 0 0 1 1 0x3 not latched

//IFX9201SG SPI Command
// SPI command set
#define IFX9201_RD_DIA	        0x00
#define IFX9201_RES_DIA	        0x80
#define IFX9201_RD_REV	        0x20
#define IFX9201_RD_CTRL	        0x60
#define IFX9201_WR_CTRL_RD_DIA	0xD0
#define IFX9201_WR_CTRL	        0xE0


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void set_new_freq_doser(doser_enum_t id_doser,uint16_t nfreq_hz );
void set_new_duty_doser(doser_enum_t id_doser,uint16_t nduty_x100);
void set_new_dir_doser(doser_enum_t  id_doser,doser_dir_enum_t ndir);
void stop_start_doser(doser_enum_t  id_doser,doser_dis_enum_t ndir);
void  read_diagnosis_doser(doser_enum_t  id_doser);
void  read_control_doser(doser_enum_t  id_doser);
void  set_control_doser(doser_enum_t  id_doser , uint8_t cmd_ctrl);
void  doser_vibro_manager(void);
void vibro_external_drive(void);
void init_doser_vibro(void);
#endif /* INC_IFX9201SG_MOTOR_DRIVER_IFX9201SG_DOSER_H_ */
