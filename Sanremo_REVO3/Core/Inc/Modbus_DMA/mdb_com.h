/*
 * mdb_com.h
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_MODBUS_DMA_MDB_COM_H_
#define INC_MODBUS_DMA_MDB_COM_H_

#include "main.h"
#include "mdb.h"


/*=== Constant definitions ===============================================================================================================*/

// DMA buffer size divided by 4 has to be greater then maximum message length
//EODBG DELME DELME ORIG #define DMA_COMM_BUFFER_SIZE                (128)       // [bytes]
#define DMA_COMM_BUFFER_SIZE                (256)       // [bytes]

#define DMA_COMM_CYCLIC_RX_BUFFER_SIZE      (32)        // [bytes]

#define DMA_NR_OF_TX_BUFFERS                (1)

#define MDB_MB_ADDRESS        ((uint8_t)1)

/* DMA ENABLE mask */
#define CCR_ENABLE_Set          ((uint32_t)0x00000001)
#define CCR_ENABLE_Reset        ((uint32_t)0xFFFFFFFE)

/*=== Macro definitions ==================================================================================================================*/

/*=== Type definitions ===================================================================================================================*/

typedef struct {
    uint32_t       prefix;
    uint8_t        data[DMA_COMM_BUFFER_SIZE];
    uint32_t       postfix;
} T_DMA_COMM_BUFFER;

typedef struct {
	UART_HandleTypeDef*             uart;                                  // Uart used for this channel
	DMA_HandleTypeDef*             txDma;                          // TX-DMA channel connected to the UART
	DMA_HandleTypeDef*             rxDma;                          // RX-DMA channel connected to the UART
	DMA_Stream_TypeDef*        DmaStreamTx;
	DMA_Stream_TypeDef*        DmaStreamRx;                          // TX-DMA channel connected to the UART
	//DMA_Stream_TypeDef*       DmaChannel;                          // RX-DMA channel connected to the UART

    /* The following flags are used by the DMA and should indicate the flag specific for this DMA device */
    uint32_t                 txReadyFlag;                           // Flag indicating if TX is Ready
    uint32_t                 rxReadyFlag;                           // Flag indicating if RX is Ready

    bool                         firstTx;                               // The TX Ready flag should not be checked on the first TX

    T_DMA_COMM_BUFFER           txBuffer[DMA_NR_OF_TX_BUFFERS];        // One buffer to send, one to be processed
    T_DMA_COMM_BUFFER           rxBuffer;                              // Receive buffer
    uint32_t                    txLength[DMA_NR_OF_TX_BUFFERS];        // Nr of bytes in the TX buffers
    uint8_t                     txBufferIndex;                         // Index to the buffer to be used by software
} T_DMA_COMM_CONTROL;

/*=== Type definitions for events ========================================================================================================*/

/*=== Macro-function definitions =========================================================================================================*/

/*=== Inline-function definitions ========================================================================================================*/

/*=== Function declarations (Interface functions) ========================================================================================*/

void dma_comm_Initialize(T_DMA_COMM_CONTROL* dmaControl);

void dma_comm_sendBytes(T_DMA_COMM_CONTROL * const dmaControl, const uint8_t length);

bool dma_comm_ReceiveByte(T_DMA_COMM_CONTROL * const dmaControl, uint8_t* byte, const uint32_t timeout);

void dma_comm_startReceiveMessage(T_DMA_COMM_CONTROL * const dmaControl, const bool continous);

void dma_comm_endReceiveMessage(T_DMA_COMM_CONTROL * const dmaControl);

uint8_t* dma_comm_getTxBuffer(T_DMA_COMM_CONTROL* const dmaControl);

uint8_t* dma_comm_getRxBuffer(T_DMA_COMM_CONTROL* const dmaControl);

void dma_comm_cleanRxBuffer(T_DMA_COMM_CONTROL* const dmaControl);

void dma_comm_waitForTxReady(T_DMA_COMM_CONTROL* const dmaControl);

bool dma_comm_isTxReady(T_DMA_COMM_CONTROL* const dmaControl);


void mdb_comm_Initialize(void);
void mdb_comm_processMessage(void);


#endif /* INC_MODBUS_DMA_MDB_COM_H_ */
