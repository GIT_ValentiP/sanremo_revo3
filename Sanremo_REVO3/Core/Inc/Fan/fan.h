/*
 ******************************************************************************
 *  @file      : fan.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_FAN_FAN_H_
#define INC_FAN_FAN_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "globals.h"
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void set_new_freq_fan(uint8_t id_fan,uint16_t nfreq_hz );
void set_new_duty_fan(uint8_t id_fan,uint16_t nduty_x100);
void power_on_off_fan(uint8_t id_fan,uint8_t power );

void  fan_manager(void);
void init_fan(void);




#endif /* INC_FAN_FAN_H_ */
