/*
 ******************************************************************************
 *  @file      : globals.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 16 set 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : Sanremo_REVO3 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


#define REVO_FW_VERSION          1u
#define REVO_FW_SUBVERSION       0x04



#define __HBM_AD105x //BALANCE HBM AD105x

/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/

// types
typedef struct system_timer_s
{
	double tick_1ms;
	double tick_10ms;              //
    double tick_100ms;            //
    double seconds;               //
    double mins;                  //
    double hours;                 //
} sys_timer_t;

//
typedef union{
	struct bit_s{
			uint16_t b0:1;
			uint16_t b1:1;
			uint16_t b2:1;
			uint16_t b3:1;
			uint16_t b4:1;
			uint16_t b5:1;
			uint16_t b6:1;
			uint16_t b7:1;
			uint16_t b8:1;
			uint16_t b9:1;
			uint16_t b10:1;
			uint16_t b11:1;
			uint16_t b12:1;
			uint16_t b13:1;
			uint16_t b14:1;
			uint16_t b15:1;
	}bit;

	    uint16_t word16;
}word_bit_t;

typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef union{
	struct cat{
			uint16_t communication:1;
			uint16_t load_cell:1;
			uint16_t motor:1;
			uint16_t sensor:1;
			uint16_t workflow:1;
			uint16_t general:1;
			uint16_t empty6:1;
			uint16_t empty7:1;
	}cat;

	    uint8_t err_cat_byte;
}error_cat_t;


typedef union{
	struct flag_s{
			uint16_t selftest:1;
			uint16_t b1:1;
			uint16_t b2:1;
			uint16_t b3:1;
			uint16_t b4:1;
			uint16_t b5:1;
			uint16_t b6:1;
			uint16_t b7:1;
			uint16_t b8:1;
			uint16_t b9:1;
			uint16_t b10:1;
			uint16_t b11:1;
			uint16_t b12:1;
			uint16_t b13:1;
			uint16_t b14:1;
			uint16_t b15:1;
	}flag;

	    uint16_t all;
}service_bit_t;





typedef enum {
			NOTHING_TO_READ,
			FW_REL_READ,
			REVO_STATUS_READ
}RS485_PARAM_READ_enum_t;

typedef enum {
			VIBRO_0=0,
			DOSER_1=1,
			DOSER_2=2,
			DOSER_3=3,
			DOSER_MAX=4,
}doser_enum_t;

typedef enum {
			HOPPER_1=0,
			HOPPER_2=1,
			HOPPER_3=2,
			BURR_0=3,
			HOPPER_MAX=4,
}stepper_enum_t;

typedef enum {
  AVVICINA_MACINA= 0,
  ALLONTANA_MACINA = 1,
} BurrDir_t;

typedef enum {
			FAN_1=0,
			FAN_2=1,
			FAN_MAX=2,
}fan_enum_t;

typedef enum {
			NTC_ADC1_CH13 =0,
			VREF_ADC1_CH17=1,
			ENC_ADC1_CH10 =2,
			//GENERIC_ADC3_CH15=3,
			NUM_MAX_ADC_CH=3,
}adc_ch_enum_t;

typedef enum {
			INT_TEMP_SENS =0,
			EXT_TEMP_SENS =1,
			NTC_TEMP_SENS =2,
			NUM_MAX_TEMP_SENSOR=3,
}temperature_sensor_enum_t;

typedef enum{
	PORTA_FILTER_0=0,//type of PORTA FILTER sensor  00:Not Present; 01:Naked;10: Becc1; 11:Becc2
	DOSER1_PRESENT=1,//1:inserted 0 : not present
	DOSER2_PRESENT=2,//1:inserted 0 : not present
	DOSER3_PRESENT=3,//1:inserted 0 : not present
	HOPPER1_OPEN  =4,
	HOPPER1_CLOSE =5,
	HOPPER2_OPEN  =6,
	HOPPER2_CLOSE =7,
	HOPPER3_OPEN  =8,
	HOPPER3_CLOSE =9,
	BURR_FAR      =10,
    BURR_NEAR     =11,
	SENSOR_POS_MAX=12,
}sensor_pos_enum_t;


typedef enum {
			INV_HZSET          =0,
			INV_TIME           =1,
			INV_DECTIME        =2,
			INV_DELAY_STEP_TIME=3,
			INV_HZSET_REL      =4,//completed
			INV_TIMEREL        =5,//not completed
			INV_UNBLOCKPWRLIM  =6,
			INV_HZSET_DEC      =7,//completed
		    INV_HOLD_MAX       =8,
}REVO_inverter_hold_enum_t;

typedef enum {
			INV_HZREAL=0,
			INV_VOUTREAL=1,
			INV_ELAPTIME=2,
			INV_VOLT=3,
			INV_PWR=4,//completed
			INV_CNT_OV=5,//not completed
			INV_CNT_CURR=6,
			INV_CNT_TEMP=7,//completed
		    INV_CNT_OVV=8,
			INV_CNT_UVV=9,
			INV_CNT_ERRCOM=10,
			INV_SET_VLT=11,
			INV_TEMPERATURE=12,
			INV_GIRI=13,
			INV_FW_VER=14,
			INV_CURRENT=15,
			INV_INPUT_MAX=16
}REVO_inverter_input_enum_t;

typedef enum {
			INV_IDLE_STATE                   =0,
			INV_POWERON_STATE                =1,
			INV_POWEROFF_STATE               =2,
			INV_READ_PARAM_STATE             =3,
			INV_INIT_STATE                   =4,
			INV_SET_PARAM_STATE              =5,
			INV_POWEROFF_VIBRO_OFF_STATE     =6,
			INV_GRINDING_SIZE_SETBURR        =7,
			INV_POWEROFF_VIBRO_ON_STATE      =8,
			INV_SET_START_UNLOCK             =9,
			INV_SET_LIMIT_UNLOCK             =11,
			INV_POWER_OPEN_HOPPER                  =12,
//			INV_SET_POWER_LIMIT_RELEASE      =11,
			INV_ERR_STATE                    =10,
}REVO_inverter_status_enum_t;

typedef enum {
            FROM_DOSER1                       = 0,
		    FROM_DOSER2                       = 1,
		    FROM_DOSER3                       = 2,
			FROM_BYPASS_DOOR                  = 3,
			MANUAL_START                      = 4,
			MANUAL_STOP                       = 5,
}grinding_manual_enum_t;

typedef enum {
			DIR_FORWARD=1,
			DIR_REVERSE=0,
}doser_dir_enum_t;

typedef enum {
			STOP_MOT=1,
			START_MOT=0,
}doser_dis_enum_t;
typedef enum {
			REVO_IDLE=0x0000,
			REVO_SELF_TEST_RUN=0x0001,
			REVO_READY_TO_USE=0x0002,
			REVO_WEIGHING_START=0x0003,
			REVO_WEIGHT1_RUN=0x0010,
			REVO_WEIGHT1_END=0x0011,//completed
			REVO_WEIGHT1_ERROR=0x0012,//not completed
			REVO_WEIGHT2_RUN=0x0013,
			REVO_WEIGHT2_END=0x0014,//completed
			REVO_WEIGHT2_ERROR=0x0015,//not completed
			REVO_WEIGHT3_RUN=0x0016,
			REVO_WEIGHT3_END=0x0017,//completed
			REVO_WEIGHT3_ERROR=0x0018,//not completed
			REVO_GRINDING_START=0x0006,//
			REVO_GRINDING_RUN=0x0007,//
			REVO_GRINDING_END=0x0008,//
			REVO_GRINDING_ERROR=0x0009,
			REVO_BURR_POS_CAL_RUN=0x0030,//
			REVO_BURR_POS_CAL_END=0x0031,//
			REVO_WEIGHT_POS_CAL_RUN=0x0035,//
		    REVO_WEIGHT_POS_CAL_END=0x0036,//
			REVO_BURR_POS_RUN=0x0040,//
		    REVO_BURR_POS_END=0x0041,//
			REVO_SERVICE_MODE=0x0050,
			REVO_CLEAN_CYCLE=0x0060,
			REVO_ERROR=0x0080,
}REVO_STATUS_enum_t;





typedef enum {
			ERROR_NONE             =0x0000,
			ERROR_COMMUNICATION    =0x0100,
			ERROR_LOADCELL         =0x0200,
			ERROR_MOTOR            =0x0400,
			ERROR_SENSOR           =0x0800,
			ERROR_WORKFLOW         =0x1000,
			ERROR_GENERAL          =0x2000,//not completed
}REVO_ERROR_enum_t;

typedef enum {
			CALIB_BAL_CMD   =0x43,//  CALIBRATION at 50gr
			TARA_BAL_CMD    =0x54, //TARA to cut offset
			WEIGHT_BAL_CMD  =0x4D,//Start continuous weighing measure
			STOP_BAL_CMD    =0x53,//STOP weighing
}balance_CMD_enum_t;


typedef enum {
			TARA_HBM_CMD       =0, //TARA to cut offset//  "TAR;",
			CALIB_HBM_CMD      =1, //CALIBRATION at 50gr//  "CWT;",
			WEIGHT_HBM_CMD     =2, //Start continuous weighing measure//  "MSV?;",
            BAUD_HBM_CMD       =3,//  "BDR115200,0;",
            CONV_RATE_HBM_CMD  =4,//  "ICR0;",
            SAVE_HBM_CMD       =5,//  "TDD1;",
			SW_VER_HBM_CMD     =6,//  "SWV?;",
			BAUD_RD_HBM_CMD    =7,//  "BDR?;",
			DPT_HBM_CMD        =8,//"DPT3;",
			RSN_HBM_CMD        =9,//"RSN1;",
			NOM_VAL_HBM_CMD    =10,//"NOV100000;",
			CAL_ZERO1_HBM_CMD  =11,//"LDW2147483647;",
			CAL_ZERO2_HBM_CMD  =12,//"LWT128542;",
			LDW_RD_HBM_CMD     =13,//"LDW?;",
			LWT_RD_HBM_CMD     =14,//"LWT?;",
			CAL_WEIGHT1_HBM_CMD=15,//"CWT133333;",
			CAL_WEIGHT2_HBM_CMD=16,//"LDW",
			CAL_WEIGHT3_HBM_CMD=17,//"LWT2147483647;",
			COF_MEAS_HBM_CMD   =18,//"COF0;"
			SPW_PSW_HBM_CMD    =19,//"SPWAED;"
			HSM_HBM_CMD        =20,//"HSM0;",
		    FMD_HBM_CMD        =21,//"FMD3;",
			ASF_HBM_CMD        =22,//"ASF4;",
			FTL_HBM_CMD        =23,//"FTL0;",
			NOTCH1_HBM_CMD     =24,//"NTF10;",
			NOTCH2_HBM_CMD     =25,//"NTF20;",
		    LAST_HBM_CMD       =26,
}balance_HBM_CMD_enum_t;


typedef enum {
			LDW_HBM_PARAM           =0,//"LDW?;",
			LWT_HBM_PARAM           =1,//"LWT?;",
			COF_HBM_PARAM           =2,//"COF0;"
		    LAST_HBM_PARAM          =3,
}balance_HBM_param_enum_t;


typedef enum {
			IDLE_BALANCE             =0,
			INIT_BALANCE             =1,
			CALIB_BALANCE            =2,//  CALIBRATION at 50gr
			WEIGHING_BALANCE         =3,//continuous weighing measure
			STOP_BALANCE             =4,//STOP weighing
			ERR_BALANCE              =5,
			RESEND_CMD_BALANCE       =6,
			TARA_BALANCE             =7,//TARA to cut offset
			WAIT_ACK_BALANCE         =8,//T
			WAIT_END_MEAS_BALANCE    =9,//
			START_MEASURE_BALANCE    =10,//Start continuous weighing measure
			END_TARA_WEIGHT_BALANCE  =11,//Start continuous weighing measure
			START_CALIB_BALANCE      =12,
			START_MEASURE_TIME       =13,//Start continuous weighing measure
			WAIT_END_MEAS_TIME       =14,//
			SCAN_BAUD_BALANCE        =15,
}balance_status_enum_t;


typedef enum {
			IDLE_STEPPER                   =0,
			INIT_STEPPER                   =1,
			MOVE_STEPPER                   =2,//
			OPEN_STEPPER                   =3,//
			STOP_MOVE_STEPPER              =4,//
			ERR_STEPPER                    =5,
			HOMING_STEPPER                 =6,
			STALL_STEPPER                  =7,//
			FINISH_MOVEMENT_STEPPER        =8,//
			SERVICE_CONTROL_STEPPER        =9,
			BURR_POSITIONING_START_STEPPER =10,
			BURR_POSITIONING_END_STEPPER   =11,
			INIT_HOPPER1                   =12,
			INIT_HOPPER2                   =13,
			INIT_HOPPER3                   =14,
			INIT_BURR                      =15,
}stepper_CMD_enum_t;


typedef enum {
			START_INIT_STEPPER                     =0,
			ENABLE_INIT_STEPPER                    =1,
			STOP_MOVE_INIT_STEPPER                 =2,
			MOVE_INIT_STEPPER                      =3,//
			HOME_INIT_STEPPER                      =4,//
			END_INIT_STEPPER                       =5,//
}stepper_INIT_enum_t;


typedef enum{
	REVO1_CONFIG=0x0000,//REVO1
	REVO3_CONFIG=0x0001,//REVO3
	REVO_NOT_SET_CONFIG=0xFFFF,//
}REVO_CONFIG_enum_t;




typedef union{
	struct DIA_REG{
			uint8_t DIA_id:4;   //DIA4 3 r Diagnosis bit 4
								//DIA3 2 r Diagnosis bit 3
								//DIA2 1 r Diagnosis bit 2
								//DIA1 0 r Diagnosis bit 1
			uint8_t CL:1;     //CL 4 r 0 = current limitation active
			uint8_t TV:1;     //TV 5 r Always 0 - used for transmission validation
			uint8_t OT:1;     //OT 6 r 0 = overtemperature shutdown
			uint8_t EN:1;     //EN 7 r 1= outputs enabled by low signal on pin DIS
			                  //0 = outputs disabled by high signal on pin DIS
	}bit;
	    uint8_t all;
}doser_DIA_t;






typedef union{
	struct CTRL_REG{
			uint8_t SPWM:1;   //SPWM 0 rw PWM Signal in case of SPI control (SIN=1)
			uint8_t SDIR:1;     //SDIR 1 rw DIR Signal in case of SPI control (SIN=1)
			uint8_t SEN:1;      //SEN 2 rw 1: Enable outputs in case of SPI control (SIN=1)
			                    //0: Disable outputs in case of SPI control (SIN=1)
			uint8_t SIN:1;      //SIN 3 rw SPI control
								//0: Control outputs via PWM/DIR inputs
								//1: Control outputs via SPI
								//Note: can only be set if DIS=0 and PWM=0 and DIR=0.
								//Any change of the DIS, PWM or DIR signals will reset
								//this bit and revert to standard control via PWM/DIR
			uint8_t OLDIS:1;     //OLDIS 4 rw Open Load Disconnect
			                    //1: Open load current source disconnected.
			uint8_t CMD:3;      //CMD 7:5 rw Command
								//011: RD_CTRL
								//110: WR_CTRL_RD_DIA
								//111: WR_CTRL

	}bit;
	    uint8_t all;
}doser_CTRL_t;

typedef union{
	struct DOSER_CMD{
			uint8_t DUTY:1;    //New duty cycle pwm
			uint8_t DIR:1;     //new direction
			uint8_t DIS:1;     //new stop start
			uint8_t FREQ:1;    //new frequency pwm
			uint8_t TIME:1;     //					//
			uint8_t PARAM1:1;     //					//
			uint8_t PARAM2:1;     //					//
			uint8_t PARAM3:1;     //					//
	}b;
	    uint8_t all;
}doser_cmd_t;



typedef union{
	struct sensor_bit_s{
	uint16_t PFtype:2;//type of PORTA FILTER sensor  00:Not Present; 01:Naked;10: Becc1; 11:Becc2
	uint16_t dos1_pres:1;//1:inserted 0 : not present
	uint16_t dos2_pres:1;//1:inserted 0 : not present
	uint16_t dos3_pres:1;//1:inserted 0 : not present
	uint16_t portafilter:1;
	uint16_t free7:1;
	uint16_t free8:1;
	uint16_t hop1_open:1;//hopper sensor 0:open 1:close
	uint16_t hop1_close:1; //hopper sensor for motor in zero position 1:in ZP 0:not in ZeroPosition
	uint16_t hop2_open:1;//hopper sensor 0:open 1:close
    uint16_t hop2_close:1; //hopper sensor for motor in zero position 1:in ZP 0:not in ZeroPosition
	uint16_t hop3_open:1;//hopper sensor 0:open 1:close
	uint16_t hop3_close:1; //hopper sensor for motor in zero position 1:in ZP 0:not in ZeroPosition
	uint16_t burr_far:1;
	uint16_t burr_near:1;
	}reg_b;
	uint16_t reg_w;
}REVO_sensor_t;



typedef struct {
	uint8_t  type_sens;
	int16_t  temperature;
	float temperature_degree;
	float humidity_real;
	uint16_t humidity;
	uint16_t error;
}REVO_temp_sensor;

typedef struct {
	ADC_TypeDef *ADCIstance;
	uint32_t  ADC_ch;
	uint32_t  id_rank;
	uint32_t  sampling_time;
	uint16_t num_sample;
	uint32_t sample_raw_sum;
	uint16_t raw_value;
	float    filtered_value;
	uint32_t  millivolt;
	uint16_t error;
}REVO_ADC_ch;

typedef struct {
	uint16_t pwm_freq;//Hertz
	uint16_t pwm_duty;
	doser_cmd_t cmd;//0: no action 1:run
    uint16_t error;
}REVO_leds_t;

typedef struct {
	uint8_t spi_id;
	GPIO_TypeDef* spi_nss_port;
	uint16_t spi_nss_pin;
	uint32_t timer_channel;
	doser_dir_enum_t DIR;
	doser_dis_enum_t DIS;
	GPIO_PinState DIR_pin;//
	GPIO_PinState DIS_pin;//
	uint16_t pwm_freq;//Hertz
	uint16_t pwm_duty;
	uint16_t speed_rpm;
	doser_cmd_t cmd;//0: no action 1:run
	uint16_t delay_over_stop;
	uint16_t count_down_vibro_off;
	doser_CTRL_t control_reg;
	doser_DIA_t  diagnosis_reg;
    uint16_t error;
}REVO_doser_vibro_t;

typedef union{
	struct l6474_status_bit_s{
	uint16_t HiZ:1;//Dual Bridge coil output in HiZ
	uint16_t fix1:1;//reserved
	uint16_t fix2:1;//reserved
	uint16_t fix3:1;//reserved
	uint16_t DIR:1;//DIREZIONE verso giro
	uint16_t fix5:1;//reserved
	uint16_t fix6:1;//reserved
	uint16_t NOTPERF_CMD:1;//hopper NOT EXECUTED COMMAND
	uint16_t WRONG_CMD:1; //hopper wrong command
	uint16_t UVLO:1;//undervoltage alarm
    uint16_t TH_WRN:1; //hopper  thermal warning,
	uint16_t TH_SD:1;//hopper thermal shutdown
	uint16_t OCD:1; //hopper overcurrent detect
	uint16_t fix13:1;//reserved
	uint16_t fix14:1; //reserved
	uint16_t fix15:1;//reserved
	}flag;
	uint16_t status16t;
}REVO_l6474_status_t;


typedef union{
	struct fan_bit_s{
	uint16_t AUTO_OFF:1;//1: automatic cycle in grinding/ 0:cooling off
	uint16_t MANUAL:1;//
	uint16_t free2:1;//
	uint16_t free3:1;//
	uint16_t free4:1;//
	uint16_t free5:1;//
	uint16_t free6:1;//
	uint16_t free7:1;//
	uint16_t DUTY_FAN_MAN:8;//duty cycle in manual
	}fan_b;
	uint16_t fan_w;
}REVO_cool_fan_t;


typedef union{
	struct abs_pos_bit_s{
    uint16_t PAR:1;//bit of parity
	uint16_t M1M0:2;//11:data not valid
	uint16_t E1E0:2;//00:valid positional data
	uint16_t E2:1;//1:valid positional data
	uint16_t ANG_POS:10;//absolute angular position data - MSB clocked out first-
	}pos_bit;
	uint16_t pos_w;
}ROTALINK_encoder_t;

typedef struct {
	uint8_t spi_id;
	GPIO_TypeDef* spi_nss_port;
	uint16_t spi_nss_pin;

	GPIO_PinState DIR_pin;//output set direction
	GPIO_PinState STOP_pin;//stop
	GPIO_PinState STANDBY_RST_pin;//output set L6474 in RESET
	GPIO_PinState FLAG_ERR_pin;//input pin FLAG status
	GPIO_PinState hall_close;
	GPIO_PinState hall_open;
	uint16_t pwm_freq;//Hertz
	uint16_t speed_rpm;
	uint8_t stop_cmd;//1: fermo il motore 0:run
	uint16_t delay_over_stop;
	REVO_l6474_status_t motor_status;
	uint16_t error;
	uint16_t hopper_weight_rd;
	uint16_t hopper_time_rd;
	uint16_t hopper_weight_target;//[gr/10]
	uint16_t hopper_time_target;//[s/10]
	uint16_t hopper_weight_offset;//[1/10gr]
	uint16_t hopper_speed_target;//0 ->100%
	uint16_t burr_distance_target;//[micron]
	uint16_t burr_distance_rd;//[micron]
	uint32_t step_pos;//absolute position in step
	uint16_t step_from_home;//absolute position in step
	uint8_t  step_mode;
	uint8_t  torque_current;
	uint16_t open_time;
	stepper_CMD_enum_t cmd;
	stepper_INIT_enum_t init_cmd;
}REVO_stepper_t;

typedef struct {
	ROTALINK_encoder_t ang_pos;
	uint16_t angle_pos;//[� degree]encoder absolute position; degree in 360�angle
	uint16_t angle_zero_pos; //encoder angle at burr zero position
	uint16_t pre_grind_pos;
	uint16_t after_grind_pos;
	uint16_t start_angle; //
	uint16_t micron_moved_pos;
	uint16_t old_pos;
	uint16_t delta_angle_pos;
	BurrDir_t direction;
}REVO_encoder_t;

typedef struct {
	uint32_t timer_channel;
	GPIO_PinState POWER_pin;//
	uint16_t pwm_freq;//Hertz
	uint16_t pwm_duty[4];//duty proportional 4 temperature range
	uint16_t speed_rpm;
	uint16_t error;
	doser_cmd_t cmd;//0: no action 1:run
	REVO_cool_fan_t cooling_set;
	uint16_t temp[4];
}REVO_fan_t;

typedef struct {
	uint8_t  cmd;//command
	balance_status_enum_t state;
	uint8_t  rs422_rx[6];
	uint16_t len_RX;
	uint16_t error;
	uint32_t weight_rd;//[mg]
	uint32_t weight_target;//[gr/10]
	uint16_t offset;//[1/10gr]
	uint32_t weight_cal;
	uint32_t tara;
	doser_enum_t doser;
	uint32_t timeout_RX;
	uint16_t timeout_Weight;
	uint16_t time_target;
	uint32_t sw_ver[2];
	uint32_t param_hbm[LAST_HBM_PARAM];
	uint32_t baud;
	bool new_weight;

}REVO_balance_t;


typedef union{
	struct c_s{
			uint16_t SETSTATE:1;
			uint16_t ROTATION:1;
			uint16_t LIMITATION:1;
			uint16_t UNLOCKSTART:1;
			uint16_t RESET:1;
			uint16_t MODE:1;
			uint16_t PWRDERAT:1;
			uint16_t SOFTST:1;
			uint16_t SOFTSTONALARM:1;
			uint16_t SOFTSTHZCHG:1;
			uint16_t PWRBOOST:1;
			uint16_t c12:1;
			uint16_t c13:1;
			uint16_t c14:1;
			uint16_t c15:1;
			uint16_t c16:1;
	}coil;

	    uint16_t coil16_t;
}REVO_inverter_coil_bit_t;

typedef union{
	struct discr_reg{
			uint16_t STATE:1;
			uint16_t OVRLOAD:1;
			uint16_t OVRCURR:1;
			uint16_t OVRTEMP:1;
			uint16_t UNDERVOLT:1;
			uint16_t OVRVOLT:1;
			uint16_t ERRCOM:1;
			uint16_t ERRMEM:1;
			uint16_t VOLTPRG:1;
			uint16_t PRECHARGE:1;
			uint16_t SOFTREADY:1;
			uint16_t START:1;
			uint16_t UNLLIMIT:1;
			uint16_t HWLIMIT:1;
			uint16_t TEMPLIMIT:1;
			uint16_t DERAT:1;
	}al;

	    uint16_t alarm16_t;
}REVO_inverter_alarm_t;

typedef struct {
	uint16_t inverter_status;
	uint16_t inverter_cmd;
	uint16_t error;
	uint16_t grinding_size;//[micron]
	uint16_t speed_rpm;//[15=200rpm  ->60 =1200rpm
	uint16_t time_grinding;//[200->64000] in 100ms 0: infinito
	REVO_inverter_coil_bit_t coil;
	REVO_inverter_alarm_t alarm;
	uint16_t input_reg[16];
	uint16_t hold_reg[8];
    uint16_t power_rd;
    uint16_t power_target;
    uint8_t  power_under_counter;
    uint8_t  hopper[4];
    uint16_t timeout_TX;
    uint16_t mdb_timeout_RX;
    uint8_t  mdb_oc;
    uint8_t  mdb_slave_adr;
    uint16_t mdb_start_adr;
    uint8_t  mdb_nreg;
    uint8_t  mdb_nbytes;
    uint8_t  mdb_nbytes_RX;
    uint8_t  frameRX[48];
	uint8_t  mdb_msgOk;
    uint8_t  mdb_msgErr;
    uint8_t  clean_cycle;
    uint8_t  manual[6];
}REVO_grinder_t;

typedef union{
	struct mode_bit_s{
			uint16_t run_manual_auto:1;
			uint16_t run_weight_time:1;
			uint16_t calibration:1;
			uint16_t service:1;
			uint16_t run_doser_bypass:1;
			uint16_t run_autostart_off_on:1;
			uint16_t b6:1;
			uint16_t b7:1;
			uint16_t b8:1;
			uint16_t b9:1;
			uint16_t b10:1;
			uint16_t b11:1;
			uint16_t b12:1;
			uint16_t b13:1;
			uint16_t b14:1;
			uint16_t b15:1;
	}bit;

	    uint16_t mode_op_reg;
}REVO_Mode_t;


typedef union{
	struct command_bit_s{
			uint16_t GRIND_HOP1:1;
			uint16_t GRIND_HOP2:1;
			uint16_t GRIND_HOP3:1;
			uint16_t GRIND_MAN:1;
			uint16_t WEIGH_HOP1:1;
			uint16_t WEIGH_HOP2:1;
			uint16_t WEIGH_HOP3:1;
			uint16_t WEIGH_EXT:1;
			uint16_t cmd9:1;
			uint16_t cmd10:1;
			uint16_t cmd11:1;
			uint16_t cmd12:1;
			uint16_t cmd13:1;
			uint16_t cmd14:1;
			uint16_t cmd15:1;
			uint16_t cmd16:1;
	}cmd;

	    uint16_t cmd_reg16;

}REVO_Op_CMD_t;

typedef union{
	struct cal_bit_s{
			uint16_t CLEAN_CYCLE_START:1;
			uint16_t WEIGH_CAL_START:1;
			uint16_t BURR_POS_CAL_START:1;
			uint16_t cmd4:1;
			uint16_t cmd5:1;
			uint16_t cmd6:1;
			uint16_t cmd7:1;
			uint16_t cmd8:1;
			uint16_t cmd9:1;
			uint16_t cmd10:1;
			uint16_t cmd11:1;
			uint16_t cmd12:1;
			uint16_t cmd13:1;
			uint16_t cmd14:1;
			uint16_t cmd15:1;
			uint16_t cmd16:1;
	}cal_b;

	    uint16_t cal16;
}REVO_CAL_CMD_t;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

//GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={SW1_LEFT_T1_GPIO_Port,SW1_CENTER_T2_GPIO_Port ,SW1_RIGHT_T3_GPIO_Port,SW2_LEFT_T4_GPIO_Port,SW2_CENTER_T5_GPIO_Port ,SW2_RIGHT_T6_GPIO_Port,SW3_LEFT_T7_GPIO_Port,SW3_CENTER_T8_GPIO_Port ,SW3_RIGHT_T9_GPIO_Port,SW4_LEFT_T10_GPIO_Port,SW4_CENTER_T11_GPIO_Port ,SW4_RIGHT_T12_GPIO_Port};
//uint16_t      switch_pin[NUM_MAX_SWITCHES] ={SW1_LEFT_T1_Pin,SW1_CENTER_T2_Pin ,SW1_RIGHT_T3_Pin,SW2_LEFT_T4_Pin,SW2_CENTER_T5_Pin ,SW2_RIGHT_T6_Pin,SW3_LEFT_T7_Pin,SW3_CENTER_T8_Pin ,SW3_RIGHT_T9_Pin,SW4_LEFT_T10_Pin,SW4_CENTER_T11_Pin ,SW4_RIGHT_T12_Pin};
/* Define ------------------------------------------------------------*/
#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif


#define BIT_0         (0x01U)
#define BIT_1         (0x02U)
#define BIT_2         (0x04U)
#define BIT_3         (0x08U)
#define BIT_4         (0x10U)
#define BIT_5         (0x20U)
#define BIT_6         (0x40U)
#define BIT_7         (0x80U)

#define BIT_8         (0x0100U)
#define BIT_9         (0x0200U)
#define BIT_10        (0x0400U)
#define BIT_11        (0x0800U)
#define BIT_12        (0x1000U)
#define BIT_13        (0x2000U)
#define BIT_14        (0x4000U)
#define BIT_15        (0x8000U)

#define BIT_16        (0x010000U)
#define BIT_17        (0x020000U)
#define BIT_18        (0x040000U)
#define BIT_19        (0x080000U)
#define BIT_20        (0x100000U)
#define BIT_21        (0x200000U)
#define BIT_22        (0x400000U)
#define BIT_23        (0x800000U)

#define BIT_24        (0x01000000U)
#define BIT_25        (0x02000000U)
#define BIT_26        (0x04000000U)
#define BIT_27        (0x08000000U)
#define BIT_28        (0x10000000U)
#define BIT_29        (0x20000000U)
#define BIT_30        (0x40000000U)
#define BIT_31        (0x80000000U)



#define RS485_TX_HMI_ENABLE     HAL_GPIO_WritePin(RS485_HMI_RTS_GPIO_Port,RS485_HMI_RTS_Pin,GPIO_PIN_SET)
#define RS485_TX_HMI_DISABLE    HAL_GPIO_WritePin(RS485_HMI_RTS_GPIO_Port,RS485_HMI_RTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_HMI_ENABLE     HAL_GPIO_WritePin(RS485_HMI_CTS_GPIO_Port,RS485_HMI_CTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_HMI_DISABLE    HAL_GPIO_WritePin(RS485_HMI_CTS_GPIO_Port,RS485_HMI_CTS_Pin,GPIO_PIN_SET)

/*RS485  DEFINE*/
#define REVO3_HMI_RS485_ADDR                               16
#define RS485_HMI_BAUDRATE                              115200
#define RS485_CMD_RD_TASTI                             32



/*TIMERS*/
#define TIMER_PRESCALER_TO_1MHZ                    48
#define TIMER_UP_RELOAD_TO_100us           99//(65536-100)
#define TIMER_UP_RELOAD_TO_1ms            999//(65536-1000)
#define TIMER_UP_RELOAD_TO_10ms          9999//(65536 -10000)


/*Error Management*/
#define NUM_ERROR_CATEGORY 10
/*Fan*/
#define FAN1_DUTY_STANDBY            20
#define FAN1_DUTY_GRINDING           30
#define FAN2_DUTY_STANDBY            40
#define FAN2_DUTY_GRINDING           55

/*Balance*/
#define  __BALANCE_NOT_CONNECTED   1

/*INVERTER*/
#define LEN_RX_RS232         6
#define LEN_TX_RS232         6
#define FIRST_CLEAN_CYCLE  0x01
#define END_CLEAN_CYCLE    0x02


/*BURRs Stepper*/
#define BURR_SCREW_PITCH_MICRON  1000  //micron
#define BURR_DISTANCE_COEFF      (2000/BURR_SCREW_PITCH_MICRON)
/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/
/*INVERTER COMMUNICATION  ModBus*/
uint8_t transmission_ready_Flag;
uint8_t messageOkFlag, messageErrFlag;
uint8_t retry_count;
uint16_t timeout, polling;
uint16_t T1_5; // inter character time out in microseconds
uint16_t T3_5; // frame delay in microseconds
uint32_t previousTimeout, previousPolling;
uint32_t timeout_TX_INV_mdb;




/* ----------------------- Static variables ---------------------------------*/

uint8_t buffer_RX_RS232[10];
uint8_t buffer_TX_RS232[10];

uint8_t buffer_BALANCE_RX[48];
bool new_data_rx;

bool wait_reply;

bool new_data_rx_balance;
uint16_t nrepeat_tara;
uint8_t  nrepeat_calibra;
uint8_t TOGGLE_PIN_LED;

/*Clock System timer Variables*/
sys_timer_t main_clock;

uint16_t cmd_rx_timeout;
uint16_t adr_modbus_reg;//Slave address in RS485 communication to HMI board
REVO_STATUS_enum_t revo_state;
REVO_CONFIG_enum_t revo_board_conf;

REVO_Mode_t revo_op_mode;
REVO_Op_CMD_t revo_op_cmd;
REVO_CAL_CMD_t revo_calibration_cmd;
//REVO_Op_CMD_t revo_cmd_rd;

uint8_t  DP_SW_RS485_adr;//dipswitch to set address of POWER BOARD on ModBus HMI

uint32_t version_fw[3];


/*Time variables*/
uint32_t last_recv_mdb_time ;
uint8_t flag_1sec;
uint8_t flag_1min;
uint16_t timeout_zero_stepper;
/**/


/*Error*/
REVO_ERROR_enum_t  revo_error_cat;
word_bit_t  errorList[NUM_ERROR_CATEGORY];
error_cat_t error_cat;
/*Sensor*/

REVO_encoder_t encoder_burr;
REVO_sensor_t pos_sensor;
REVO_temp_sensor temp_sens[NUM_MAX_TEMP_SENSOR];
uint32_t adc_raw_value[NUM_MAX_ADC_CH];
REVO_ADC_ch adc123_ch[NUM_MAX_ADC_CH];


REVO_leds_t illuminazione;
REVO_doser_vibro_t doser_vibro[DOSER_MAX];
REVO_stepper_t hopper_burr[HOPPER_MAX];//
REVO_fan_t fan[2];
REVO_grinder_t grinder;
REVO_balance_t balance;


/*Service*/
service_bit_t service;

/* Function prototypes -----------------------------------------------*/



#endif /* INC_GLOBALS_H_ */
