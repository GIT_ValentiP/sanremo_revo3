/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SENSOR_RHT_INT_ADR 0x4A
#define SENR_RHT_EXT_ADR 0x4B
#define LED7_SERVICE_Pin GPIO_PIN_5
#define LED7_SERVICE_GPIO_Port GPIOF
#define LED6_SERVICE_Pin GPIO_PIN_0
#define LED6_SERVICE_GPIO_Port GPIOB
#define LED5_SERVICE_Pin GPIO_PIN_8
#define LED5_SERVICE_GPIO_Port GPIOE
#define LED4_SERVICE_Pin GPIO_PIN_10
#define LED4_SERVICE_GPIO_Port GPIOE
#define BTN_VIBRO_Pin GPIO_PIN_12
#define BTN_VIBRO_GPIO_Port GPIOE
#define RS232_DBG_TX_Pin GPIO_PIN_10
#define RS232_DBG_TX_GPIO_Port GPIOB
#define RS232_DBG_RX_Pin GPIO_PIN_11
#define RS232_DBG_RX_GPIO_Port GPIOB
#define LED3_SERVICE_Pin GPIO_PIN_5
#define LED3_SERVICE_GPIO_Port GPIOD
#define LED2_SERVICE_Pin GPIO_PIN_15
#define LED2_SERVICE_GPIO_Port GPIOG
#define LED1_SERVICE_Pin GPIO_PIN_8
#define LED1_SERVICE_GPIO_Port GPIOB
#define LED8_SERVICE_Pin GPIO_PIN_0
#define LED8_SERVICE_GPIO_Port GPIOE
#define LED9_SERVICE_Pin GPIO_PIN_1
#define LED9_SERVICE_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
