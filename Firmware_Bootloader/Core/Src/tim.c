/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
#include "globals.h"
/* USER CODE END 0 */

TIM_HandleTypeDef htim14;

/* TIM14 init function */
void MX_TIM14_Init(void)
{

  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 1080-1;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 100-1;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM14)
  {
  /* USER CODE BEGIN TIM14_MspInit 0 */

  /* USER CODE END TIM14_MspInit 0 */
    /* TIM14 clock enable */
    __HAL_RCC_TIM14_CLK_ENABLE();
  /* USER CODE BEGIN TIM14_MspInit 1 */

     HAL_TIM_Base_Start_IT(tim_baseHandle);
  /* USER CODE END TIM14_MspInit 1 */
  }
}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM14)
  {
  /* USER CODE BEGIN TIM14_MspDeInit 0 */

  /* USER CODE END TIM14_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM14_CLK_DISABLE();

    /* TIM14 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM8_TRG_COM_TIM14_IRQn);
  /* USER CODE BEGIN TIM14_MspDeInit 1 */

  /* USER CODE END TIM14_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */


/* Initialize main_clock structure
 *
 *
 *
 *
 * */
void Main_Clock_Init(void){
main_clock.tick_1ms=0;
main_clock.tick_10ms=0;
main_clock.tick_100ms=0;
main_clock.hours=0;
main_clock.mins=0;
main_clock.seconds=0;

TOGGLE_PIN_LED =0;
n_sec=0;
sec_cnt=0;

}


void TIM_PWM_Init(){


}


/* Update System Time Calendar every 10ms
 *
 *
 *
 *
 * */
void System_Clock_Timer_TickMain(void){
//uint8_t id;
//uint16_t count_sec;
	uart3_tx_end=1;
  if (++main_clock.tick_1ms>9){
	  main_clock.tick_1ms=0;
	if (++main_clock.tick_10ms>9){//elapsed 100milliseconds
		//mdb_comm_processMessage();
		main_clock.tick_10ms=0;
		if(++main_clock.tick_100ms > 9){//elapsed 1s
			main_clock.tick_100ms=0;
			//count_sec=(uint16_t)main_clock.seconds;
			//if((count_sec%3)==0){
			flag_1sec=1;
			//}
			if(++main_clock.seconds >59){//elapsed 1 minute
				main_clock.seconds=0;
				flag_1min=1;
//				if(++main_clock.mins > 59){//elapsed 1 hour
//					main_clock.hours++;
//					main_clock.mins=0;
//				}
		    }
		    TOGGLE_PIN_LED =((TOGGLE_PIN_LED==1) ? 0:1);
		}
	}
  }


}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
