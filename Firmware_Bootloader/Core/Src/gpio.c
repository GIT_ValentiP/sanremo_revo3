/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED6_SERVICE_Pin|LED1_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, LED5_SERVICE_Pin|LED4_SERVICE_Pin|LED8_SERVICE_Pin|LED9_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED7_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED7_SERVICE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin */
  GPIO_InitStruct.Pin = LED6_SERVICE_Pin|LED1_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PEPin PEPin PEPin PEPin */
  GPIO_InitStruct.Pin = LED5_SERVICE_Pin|LED4_SERVICE_Pin|LED8_SERVICE_Pin|LED9_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BTN_VIBRO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_VIBRO_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED3_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED3_SERVICE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED2_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED2_SERVICE_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */
void led_service_init(void){



	HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, GPIO_PIN_SET);

    HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, GPIO_PIN_SET);


    /*ILUUMINA Led stripe*/

//    htim4.Instance->ARR=990;
//    htim4.Init.Period  =999;
//   	htim4.Instance->CCR2=0;
//    illuminazione.pwm_duty=65;
//    illuminazione.cmd.b.DUTY=1;

}

void led_service_ON_OFF(unsigned char id_led,unsigned char status){
 GPIO_PinState set_status_led;
 set_status_led=(status>0 ?GPIO_PIN_SET: GPIO_PIN_RESET);
 if (id_led < 10){
	 switch	(id_led){
		 case(1):
						HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, set_status_led);
						break;
		 case(2):
						HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port, LED2_SERVICE_Pin, set_status_led);
						break;
		 case(3):
						HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port, LED3_SERVICE_Pin, set_status_led);
						break;
		 case(4):
						HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin, set_status_led);
						break;
		 case(5):
						HAL_GPIO_WritePin(LED5_SERVICE_GPIO_Port, LED5_SERVICE_Pin, set_status_led);
						break;
		 case(6):
						HAL_GPIO_WritePin(LED6_SERVICE_GPIO_Port, LED6_SERVICE_Pin, set_status_led);
						break;
		 case(7):
						HAL_GPIO_WritePin(LED7_SERVICE_GPIO_Port, LED7_SERVICE_Pin, set_status_led);
						break;
		 case(8):
						HAL_GPIO_WritePin(LED8_SERVICE_GPIO_Port, LED8_SERVICE_Pin, set_status_led);
						break;
		 case(9):
						HAL_GPIO_WritePin(LED9_SERVICE_GPIO_Port, LED9_SERVICE_Pin, set_status_led);
						break;
	 }
 }



}

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
