/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "fatfs.h"
#include "iwdg.h"
#include "tim.h"
#include "usart.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
#include "bootloader.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
extern ApplicationTypeDef Appli_state ;

extern char USBHPath[4];   /* USBH logical drive path */

pFunction Jump;
uint32_t  JumpAddress ;
TCHAR logFileName[]="KTRONIC_LOG.txt";
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */
//USB Write Function
bool Usb_Write_FS(TCHAR *msg);
bool Usb_Read_FS(TCHAR *fileName,uint16_t len);

void Enter_Bootloader(void);
void SystemPeripheralDeInit(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */
  

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  MX_CRC_Init();
  //MX_IWDG_Init();
  MX_TIM14_Init();
  MX_FATFS_Init();
  MX_USB_HOST_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

  led_service_init();

  /*TIMERs Init*/
    //TIM_PWM_Init();
    Main_Clock_Init();
    HAL_TIM_Base_MspInit(&htim14);
    //HAL_TIM_Base_MspInit(&htim13);
    flag_1min=0;

  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  printf("\n\r BOOTLOADER v%2d.%2d \n\r",BOOT_FW_VERSION,BOOT_FW_SUBVERSION);
  memset((void *)msg,0,40);
  bootloader_status=BOOT_IDLE;
  boot.all16=0x0000;

  while (1)
  {
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
    switch(Appli_state){
      case(APPLICATION_IDLE):

    	                	     break;
      case(APPLICATION_START):
    		                    if (boot.flag.mount==0){
									 res_file=f_mount(&myUsbFatFS,(const TCHAR*)USBHPath,0);
									 if (res_file!= FR_OK){
														 //FatFs Initialization Error
										   /* f_mount failed */
											printf("USB cannot be mounted.");
											//sprintf(msg, "USB cannot be mounted.FatFs error code: %u", res_file);
											//printf(msg);
											Error_Handler();

									 }else{
                                            printf("\n\r USB mounted.");
                                        	//sprintf(msg, "\n\r USB mounted.");

                                            boot.flag.mount=1;
											led_service_ON_OFF(2,LED_ON); //TOGGLE_PIN_LED);
									 }
    		                    }
         	                	 break;
      case(APPLICATION_READY):

		                          //flag_1min=0;
		                          //sprintf((char *)appFileName,"REVO3_Boot0100.TXT");
								  //if(Usb_Write_FS(appFileName)!=0){//write successfully
								  //	led_service_ON_OFF(7,LED_ON);
								  // }else{//write error
								  //	Error_Handler();
								  //}

		                          if (boot.flag.mount==1){
		                        	  if (boot.flag.start==0){
										 boot.flag.start=1;
										 sprintf(rw_buffer,"\n\rREVO3 ver.:%3d.%3d LOGGING\n\r USB mounted",BOOT_FW_VERSION,BOOT_FW_SUBVERSION);
										 //Usb_Write_FS(msg_buffer);
										 f_open(&logFile,(const TCHAR *)logFileName,FA_WRITE | FA_CREATE_ALWAYS);
										 f_write(&logFile,(const void*)rw_buffer,strlen(rw_buffer),&bytes_wr);
										 f_close(&logFile);
										 sprintf(rw_buffer,"\n\rSec:%ld Status:%d ",sec_cnt,bootloader_status);
										 Usb_Write_FS(msg_buffer);
										 bootloader_status=BOOT_OPEN_FILE;
		                             }
		                          }

         	                	 break;
      case(APPLICATION_DISCONNECT):
		                         if (boot.flag.mount==1){
									 printf("USB disconnect.");
									 //sprintf(msg, "USB disconnect.");
									 boot.all16=0x0000;
									 led_service_ON_OFF(2,LED_OFF);//TOGGLE_PIN_LED);
		                         }
         	                	 break;

    }
    Enter_Bootloader();
    if (flag_1sec==1){
    	sec_cnt++;
    	    if (boot.flag.mount==0){
    	       //n_sec++;
    	       if (++n_sec >1){
    	    	   if(boot.flag.jump==0){
    	    	    bootloader_status=BOOT_CHECK_FOR_APP;
    	    	    boot.flag.jump=1;
    	    	   }
    	       }
    	    }
    	    if (boot.flag.start==1){
    	    	sprintf(rw_buffer,"\n\rSec:%ld Status:%d ",sec_cnt,bootloader_status);
    	        Usb_Write_FS(msg_buffer);
    	    }
  			printf("\n\r Sec:%ld Boot status:%d",sec_cnt,bootloader_status);
    	    //printf(msg);
    	    //memset((void *)msg,0,40);
  			led_service_ON_OFF(4,TOGGLE_PIN_LED);
  			flag_1sec=0;
    }

    if(boot.flag.valid==1){
    	led_service_ON_OFF(7,LED_ON);
    }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* RCC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RCC_IRQn);
  /* USART3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* TIM8_TRG_COM_TIM14_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM8_TRG_COM_TIM14_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM8_TRG_COM_TIM14_IRQn);
}

/* USER CODE BEGIN 4 */
bool Usb_Write_FS(TCHAR *msg){
//FRESULT res;
	//Open or Create file for writing appFileName
	if (f_open(&logFile,(const TCHAR *)logFileName,FA_WRITE | FA_OPEN_ALWAYS)!= FR_OK){
		//led_service_ON_OFF(5,LED_ON);
		return (0);//error open

	}else{
	    /* Seek to end of the file to append data */
		res_file = f_lseek(&logFile, f_size(&logFile));
		if(res_file!= FR_OK){
					//led_service_ON_OFF(6,LED_ON);
					return(0);//error write
	    }else{
			//Copy test in the temporary read/write buffer
			//sprintf(rw_buffer,"REVO3 ver.:%3d.%3d LOGGING",BOOT_FW_VERSION,BOOT_FW_SUBVERSION);
			//Write to appFile
			res_file=f_write(&logFile,(const void*)rw_buffer,strlen(rw_buffer),&bytes_wr);
			if((res_file!= FR_OK)||(bytes_wr==0)){
				//led_service_ON_OFF(6,LED_ON);
				return(0);//error write
			}
	    }
	}
    f_close(&logFile);
    return(1);
}


bool Usb_Read_FS(TCHAR *fileName,uint16_t len){
uint16_t i	;

	if (f_open(&appFile,(const TCHAR *)fileName,FA_READ)!= FR_OK){
		//led_service_ON_OFF(5,LED_ON);
		return (0);//error open
	}else{
       if (len < BUFFER_SIZE_RW_FILE ){
    	   for(i=0;i<len;i++){
    		   //res_file=f_read(&appFile,(const void*)rw_buffer,len,&bytes_rd);
    		   res_file=f_read(&appFile,(void *)rw_buffer,1,&bytes_rd);
    		   if(bytes_rd!=0){
    			   bytes_rd=i;
    		   }else{
    			   break;
    		   }
    	   }
    	   if(bytes_rd==0){
    	     return(0);
    	   }
       }else{
    	   return(0);
       }
	}
 f_close(&appFile);
 return(1);
}


/*Peripheral Deinit*/
void SystemPeripheralDeInit(void){
    HAL_UART_MspDeInit(&huart3);
	HAL_CRC_MspDeInit(&hcrc);
	HAL_TIM_Base_MspDeInit(&htim14);
	FATFS_UnLinkDriver(USBHPath);
	/* GPIO Ports Clock Disable */
//	__HAL_RCC_GPIOA_CLK_DISABLE();
//	__HAL_RCC_GPIOB_CLK_DISABLE();
//	__HAL_RCC_GPIOC_CLK_DISABLE();
//	__HAL_RCC_GPIOD_CLK_DISABLE();
//	__HAL_RCC_GPIOE_CLK_DISABLE();
//	__HAL_RCC_GPIOF_CLK_DISABLE();
//	__HAL_RCC_GPIOG_CLK_DISABLE();
//	__HAL_RCC_GPIOH_CLK_DISABLE();
}

/*** Bootloader ***************************************************************/
void Enter_Bootloader(void)
{
    FRESULT  fr;
    UINT     num;
//    uint8_t  i;
    uint8_t  status,nbyte_rd;
    uint32_t data;
    uint32_t *app_adr;
//    uint32_t cntr;
//    uint32_t addr;
    //char     msg[40] = {0x00};


    /* Check for flash write protection */
//    if(Bootloader_GetProtectionStatus() & BL_PROTECTION_WRP)
//    {
//        printf("Application space in flash is write protected.");
//        printf("Press button to disable flash write protection...");
//        LED_R_ON();
//        for(i=0; i<100; ++i)
//        {
//            //LED_Y_TG();
//            HAL_Delay(50);
//            if(IS_BTN_PRESSED())
//            {
//                printf("Disabling write protection and generating system reset...");
//                Bootloader_ConfigProtection(BL_PROTECTION_NONE);
//            }
//        }

//        printf("Button was not pressed, write protection is still active.");
//        printf("Exiting Bootloader.");
//        return;
//        printf("Disabling write protection and generating system reset...");
//       Bootloader_ConfigProtection(BL_PROTECTION_NONE);
//    }



    switch(bootloader_status){
			case (BOOT_IDLE):
		                              bootloader_status=BOOT_IDLE;
									  break;
			case (BOOT_MOUNT_DEVICE):

									  break;
			case (BOOT_CHECK_FLASH):
									 /* Step 1: Init Bootloader and Flash */
										Bootloader_Init();
			    	    	            sprintf(rw_buffer,"\n\rCheckFlash\n\r ");
							            Usb_Write_FS(msg_buffer);
										//led_service_ON_OFF(6,LED_ON);
										bootloader_status=BOOT_ERASE;
									  break;
			case (BOOT_CHECK_SIZE):
									    //sprintf(msg, "Software found on USB/SD.");
										//printf(msg);
		                                printf("\n\rSoftware found on USB/SD.");
			                            sprintf(rw_buffer,"\n\rApplication REVO3.bin found on USB/SD.\n\r ");
									    Usb_Write_FS(msg_buffer);
										/* Check size of application found on SD card */
										if(Bootloader_CheckSize( f_size(&appFile) ) != BL_OK)
										{
											printf("\n\r ");
											sprintf(rw_buffer,"\n\rErr:file too large.\n\r ");
											Usb_Write_FS(msg_buffer);
											led_service_ON_OFF(5,LED_ON);
											f_close(&appFile);
											//SD_Eject();
											//printf("SD ejected.");
											bootloader_status=BOOT_ERROR;
										}else{
											//sprintf(msg, "App size OK.");
									        //printf(msg);
											printf("\n\r App size OK.");
											sprintf(rw_buffer,"\n\rApp size OK.\n\r ");
											Usb_Write_FS(msg_buffer);
									        bootloader_status=BOOT_CHECK_FLASH;

										}

									  break;

			case (BOOT_ERASE):
									    /* Step 2: Erase Flash */
		                                printf("\n\rErasing flash");
			                            sprintf(rw_buffer,"\n\rErasing flash");
										Usb_Write_FS(msg_buffer);
									    if (Bootloader_Erase() != BL_OK){
									    	printf("\n\rErr. Erase sector");
									    	sprintf(rw_buffer,"\n\rErr. Erase sector");
											Usb_Write_FS(msg_buffer);
											//led_service_ON_OFF(5,LED_ON);
											f_close(&appFile);
											bootloader_status=BOOT_ERROR;
									    }else{
										    //LED_Y_OFF();
									    	printf("\n\rFlash erase end.");
									    	sprintf(rw_buffer,"\n\rFlash erase end.");
									    	Usb_Write_FS(msg_buffer);
									    	boot.flag.erase=1;
										    bootloader_status=BOOT_PROGRAM_START;
									    }
										//LED_Y_ON();
									    break;
			case (BOOT_PROGRAM_START):
									    /* Step 3: Programming */

										//LED_Y_ON();
										cntr_word=0;//cntr = 0;
										Bootloader_FlashBegin();
										printf("\n\rProgram Flash.Start adr:%lx",flash_ptr);
										sprintf(rw_buffer,"\n\rProgram Flash.Start adr:%lx",flash_ptr);
										Usb_Write_FS(msg_buffer);
										bootloader_status=BOOT_PROGRAM_DATA;
   									    break;
			case (BOOT_PROGRAM_DATA):
		                                    data = 0xFFFFFFFF;//data = 0xFFFFFFFFFFFFFFFF;
			                                nbyte_rd=4;//8
											fr = f_read(&appFile, &data,nbyte_rd, &num);
											if (fr == FR_OK){
												if(num){
													status = Bootloader_FlashNext(data);
													if(status == BL_OK)
													{
														cntr_word++;
														bootloader_status=BOOT_PROGRAM_DATA;
													}
													else
													{
														printf("\n\r Err.Flash Write:%d",status);
														sprintf(msg, "\n\r Programming error at: %lu byte", (cntr_word*8));
														sprintf(rw_buffer,"\n\rErr.Flash Write:%d Programming error at: %lu byte",status, (cntr_word*8));
													    Usb_Write_FS(msg_buffer);
														printf(msg);
														f_close(&appFile);
														bootloader_status=BOOT_ERROR;
													}

												}else{
													printf("\n\rEnd file");
													sprintf(rw_buffer,"\n\rEnd to transfer data APP into Flash");
												    Usb_Write_FS(msg_buffer);
													bootloader_status=BOOT_PROGRAM_END;

												}
												if(cntr_word % 256 == 0)
												{
													/* Toggle green LED during programming */
													led_service_ON_OFF(6,TOGGLE_PIN_LED);
												}

											}else{
												printf("\n\rError read file");
												sprintf(rw_buffer,"\n\rError reading .bin file");
												Usb_Write_FS(msg_buffer);
												f_close(&appFile);
												bootloader_status=BOOT_ERROR;
											}

										    break;
			case (BOOT_PROGRAM_END):
										    /* Step 4: Finalize Programming */
		                                    led_service_ON_OFF(6,LED_OFF);
											Bootloader_FlashEnd();
											f_close(&appFile);
											printf("\n\rProgramming finished.");
											sprintf(msg, "\n\rFlashed: %lu bytes.", (cntr_word*4));
											printf(msg);
											sprintf(rw_buffer,"\n\rProgramming finished.Flashed: %lu bytes.", (cntr_word*4));
											Usb_Write_FS(msg_buffer);
											boot.flag.program=1;
											/* Open file for verification */
											fr = f_open(&appFile, CONF_FILENAME, FA_READ);
											if(fr != FR_OK)
											{
												/* f_open failed */
												printf("\n\rFile cannot be opened.");
												sprintf(msg, "\n\rFatFs error code: %u", fr);
												printf(msg);
												sprintf(rw_buffer,"\n\rError open file FatFs err.: %u", fr);
												Usb_Write_FS(msg_buffer);
												bootloader_status=BOOT_ERROR;
											}else{
												bootloader_status=BOOT_VERIFY_START;
											}

										    break;
			case (BOOT_OPEN_FILE):
		                                //sprintf(msg, "Start open file...");
									    //printf(msg);
		                                printf("\n\rOpen file");
										/* Open file for programming */
										fr = f_open(&appFile,CONF_FILENAME,FA_READ);//f_open(&SDFile, CONF_FILENAME, FA_READ);
										if(fr != FR_OK)
										{
											/* f_open failed */
											//printf("File cannot be opened.");
											sprintf(msg, "\n\rFatFs err.: %u", fr);
											printf(msg);
											sprintf(rw_buffer,"\n\rError open file FatFs err.: %u", fr);
											Usb_Write_FS(msg_buffer);
											bootloader_status=BOOT_ERROR;
										}else{
											bootloader_status=BOOT_CHECK_SIZE;
										}

									  break;
			case (BOOT_VERIFY_START):
			            				/* Step 5: Verify Flash Content */
		                                addr_flash_app = APP_ADDRESS;
			                            cntr_word = 0;
			                            bootloader_status=BOOT_VERIFY_DATA;
									  break;
			case (BOOT_VERIFY_DATA):
						              /* Step 5: Verify Flash Content */
									  data = 0xFFFFFFFF;//0xFFFFFFFFFFFFFFFF;
			                          num=0;
					   			      fr = f_read(&appFile, &data, 4, &num);
									  if(fr == FR_OK){
										if(num)
										{
											if(*(uint32_t*)addr_flash_app == (uint32_t)data)
											{
												addr_flash_app += 4;
												cntr_word++;
												bootloader_status=BOOT_VERIFY_DATA;
											}
											else
											{
												sprintf(msg, "\n\rVerification error at: %lu byte.", (cntr_word*4));
												printf(msg);
												sprintf(rw_buffer,"\n\rErr.Verification err. at: %lu byte.", (cntr_word*4));
												Usb_Write_FS(msg_buffer);
												f_close(&appFile);
												bootloader_status=BOOT_ERROR;

											}
										}else{
											printf("\n\rEnd file");
											sprintf(rw_buffer,"\n\rFinish read .bin file");
								            Usb_Write_FS(msg_buffer);
											bootloader_status=BOOT_VERIFY_END;
										}
										if(cntr_word % 256 == 0)
										{
											/* Toggle green LED during verification */
											//LED_G_TG();
											led_service_ON_OFF(6,TOGGLE_PIN_LED);
										}
									  }else{
										  printf("\n\rError read file verify");
										  sprintf(rw_buffer,"\n\rError read file verify");
										  Usb_Write_FS(msg_buffer);
									      f_close(&appFile);
										  bootloader_status=BOOT_ERROR;
									  }
									   break;
			case (BOOT_VERIFY_END):
		                            led_service_ON_OFF(6,LED_OFF);
		                            f_close(&appFile);
									bootloader_status=BOOT_CRC_CALC;//BOOT_IDLE;
									printf("\n\r Verification passed.");
									sprintf(rw_buffer,"\n\rProgram Verify passed.");
									Usb_Write_FS(msg_buffer);
									boot.flag.verify=1;
									boot.flag.valid=1;
									break;

			case (BOOT_CRC_CALC):
	                             	printf("\n\rCRC calculus...");
			                        sprintf(rw_buffer,"\n\rCHKSUM calculus...");
								    Usb_Write_FS(msg_buffer);
					                status=Bootloader_VerifyChecksum();
					                if(status ==BL_OK){
										bootloader_status=BOOT_CRC_SET;
										printf("\n\rApplication CRC:%lX",calculatedCrc);
										sprintf(rw_buffer,"\n\rApplication CHKSUM:%lX",calculatedCrc);
										Usb_Write_FS(msg_buffer);
										boot.flag.crc=1;
					                }else{
					                	  printf("\n\rError CRC calculus");
					                	  sprintf(rw_buffer,"\n\rError CHKSUM:%lX",calculatedCrc);
					                	  Usb_Write_FS(msg_buffer);
										  bootloader_status=BOOT_ERROR;
					                }
									break;

			case (BOOT_CRC_SET):
		                            printf("\n\rSet CRC flash");
			                        status=Bootloader_SetCRC(calculatedCrc);

								    if(status ==BL_OK){
										bootloader_status=BOOT_IDLE;
										printf("\n\rApplication CRC in flash");
										sprintf(rw_buffer,"\n\rSet Application CHKSUM :%lX in flash",calculatedCrc);
										Usb_Write_FS(msg_buffer);
										boot.flag.crc_ver=1;
								    }else{
					                	  printf("\n\rError CRC set flash");
										  bootloader_status=BOOT_ERROR;
					                }
                 					break;
			case (BOOT_CRC_VERIFY):
									led_service_ON_OFF(6,LED_OFF);
									f_close(&appFile);
									bootloader_status=BOOT_IDLE;
									printf("\n\r Verification passed.");
									sprintf(rw_buffer,"\n\rErr. Erase sector");
									Usb_Write_FS(msg_buffer);
									boot.flag.verify=1;
									boot.flag.valid=1;
									break;
			case (BOOT_ERROR):
		                              if (flag_1sec==1){
										printf("\n\r Error:");
										if( boot.flag.jump==1){
											 printf("JUMP Err");
										}else if(boot.flag.erase==0){
                                        	    printf("ERASE Err");
                                              }else if(boot.flag.program==0){
                                        	          printf("PROGRAM Err");
                                                    }else if(boot.flag.verify==0){
                                               	            printf("VERIFY Err");
                                                         }
										led_service_ON_OFF(5,TOGGLE_PIN_LED);

		                              }
			                          led_service_ON_OFF(6,LED_OFF);
									  break;
			case (BOOT_FLASH_POTECTION):

											/* Enable flash write protection */
										#if (USE_WRITE_PROTECTION)
											printf("\n\rEnablig flash write protection and generating system reset...");
											if(Bootloader_ConfigProtection(BL_PROTECTION_WRP) != BL_OK)
											{
												printf("\n\rFailed to enable write protection.");
												printf("\n\rExiting Bootloader.");
											}
										#endif
    	                              break;

			case (BOOT_CHECK_FOR_APP):

			                             app_adr=(uint32_t*)(0x08040000);
	                                     printf("\n\rCheck for APPLICATION in flash");
										/* Check if there is application in user flash area */
			                             printf("\n\rAPP.SP adr:%lX",(*(uint32_t *)app_adr));
			                             //printf("\n\rRAM Start adr:%lX",RAM_BASE);
										 if(Bootloader_CheckForApplication() == BL_OK)
										 {
											 /* Verify application checksum */
											 if(Bootloader_VerifyAppInFlashChecksum() != BL_OK)
											 {
												 printf("\n\rChecksum APP. in flash Error.");
												 bootloader_status=BOOT_ERROR;
											 }
											 else
											 {
												 printf("\n\rChecksum APP. in flash OK.");
												 bootloader_status=BOOT_APP_DEINIT;
											 }
										 }else{
											 printf("\n\rAPP. not present in flash");
											 bootloader_status=BOOT_ERROR;
										 }
		    	                         break;

			case (BOOT_APP_DEINIT):
								 /* De-initialize bootloader hardware & peripherals */
									printf("\n\rDeInit Peripherals");
									SystemPeripheralDeInit();
									JumpAddress = *(__IO uint32_t*)(APP_ADDRESS + 4);
									Jump = (pFunction)JumpAddress;
									HAL_RCC_DeInit();
									HAL_DeInit();
									SysTick->CTRL = 0;
									SysTick->LOAD = 0;
									SysTick->VAL  = 0;
									//SCB->VTOR = APP_ADDRESS;
									__set_MSP(*(__IO uint32_t*)APP_ADDRESS);
									Jump();
									while(1){}
									//Bootloader_JumpToApplication();
									//bootloader_status=BOOT_APP_JUMP;
									bootloader_status=BOOT_IDLE;
									break;

			case (BOOT_APP_JUMP):
								 /* Launch application */
		                         printf("\n\rLaunching Application.");
			                     bootloader_status=BOOT_IDLE;
								 Bootloader_JumpToApplication();

								 break;
			default:

							     break;
    }

}





/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1){

	  if (flag_1sec==1){
		printf("\n\r Error");
		led_service_ON_OFF(5,TOGGLE_PIN_LED);
		flag_1sec=0;
	  }
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
