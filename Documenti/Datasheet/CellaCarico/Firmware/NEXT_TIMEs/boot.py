import time
import array
import struct
import os

from machine import Pin, SPI, UART
import dht
import esp

# Read ADC
def ReadAdc(Mode, DataPin, ClkPin) :
	result = 0
	if Mode == 0 :
		for i in range(0, 25):
			ClkPin.value(0)	
			ClkPin.value(1)						
			result = result | DataPin.value()
			ClkPin.value(0)
		        result = result<<1
		if result & 0x800000 == 1 :
			result = result | 0xFF000000

	if Mode == 1 :
		for i in range(0, 26):
			ClkPin.value(0)	
			ClkPin.value(1)						
			result = result | DataPin.value()
			ClkPin.value(0)
		        result = result<<1
		if result & 0x800000 == 1 :
			result = result | 0xFF000000

	if Mode == 2 :
		for i in range(0, 27):
			ClkPin.value(0)	
			ClkPin.value(1)						
			result = result | DataPin.value()
			ClkPin.value(0)
		        result = result<<1
		if result & 0x800000 == 1 :
			result = result | 0xFF000000

	return result
 
ADC1_DATA = Pin(35, Pin.IN)
ADC1_CLK  = Pin(32, Pin.OUT)
ADC2_DATA = Pin(22, Pin.IN)
ADC2_CLK  = Pin(23, Pin.OUT)

uart = UART(2, 115200) # init with given baudrate
uart.init(115200, bits=8, parity=None, stop=1) 
uart.write("Hello World!")

adc_value = ReadAdc(2, ADC1_DATA, ADC1_CLK)
time.sleep_ms(100)
buf = bytearray(10)
cntr_buf = bytearray(2)
tm_buf  = bytearray(6)

cntr_buf[0] = 0x06
cntr_buf[1] = 0x16

tm_buf[0] = 0xCA
tm_buf[1] = 0xFE
tm_buf[5] = 0x04

tara = 0
caloffset = 0
wait_cmd = 1
calfactor = 0.

#FIRST TIME uncomment the following 5 lines then after first run comment them again:
#print('DISK Directory Content: ',os.listdir('/'))
#f = open('/LCGS_Config.txt', 'w')
#f.write('482002.531'+'\n')    # LCSG Calibration Factor
#f.write('LCGS Board number: E_BDR #1'+'\n')    # LCSG Unit number
#f.close()

print('\n')
f = open('/LCGS_Config.txt', 'r')
saved_calfact = f.readline()
print('Stored Cal Factor: ',saved_calfact)
board_info = f.readline()
print('Board info: ',board_info)
f.close()
calfactor = float(saved_calfact)

while 1 :
  while wait_cmd == 1 :
    if uart.any() !=0:
      uart.readinto(buf)
      if chr(buf[0])  == 'T':
        print("Tara!")
        adc_value = 0  
        for j in range(0, 50):
      # while 1==1 :
          while (ADC1_DATA.value()==1) :
            pippo = 1
          time.sleep_ms(1)
          adc_value =  adc_value + ReadAdc(2, ADC1_DATA, ADC1_CLK)
      #  print("current step ",j,"Tara", adc_value)
        tara = adc_value/50
        uart.write(chr(cntr_buf[0]))
        print("current Tara: ", tara)
        
      if chr(buf[0])  == 'C':
        print("Calib!")
        adc_value = 0  
        for j in range(0, 50):
          while (ADC1_DATA.value()==1) :
            pippo = 1
          time.sleep_ms(1)
          adc_value = adc_value  + ReadAdc(2, ADC1_DATA, ADC1_CLK)
    #     print("current step ",j,"Tara", adc_value)
        caloffset = adc_value/50
        uart.write(chr(cntr_buf[0]))
        print("current cal offeset: ", caloffset)
        calfactor = 100000.0/(1.0*(caloffset-tara))      
        print('DISK Directory Content: ',os.listdir('/'))
        f = open('/LCGS_Config.txt', 'w')
        saved_calfact = str(calfactor)
        f.write(saved_calfact+'\n')    # LCSG Calibration Factor
        f.write('LCGS Board number: E_BDR #1'+'\n')    # LCSG Unit number
        f.close()
        f = open('/LCGS_Config.txt', 'r')
        saved_calfact = f.readline()
        print('Stored Cal Factor: ',saved_calfact)
        f.close()

        
      if chr(buf[0])  == 'M':
        print("Measure!")   
        measure_f=1
        while measure_f == 1 : # uart.any() == 0 :
          if uart.any() != 0 : 
           uart.readinto(buf)
           if chr(buf[0])  == 'S':
               measure_f =0
          pippo = 0
          adc_value = 0
          for i in range(0,10) :
            while (ADC1_DATA.value()==1) :
              pippo = 1
            time.sleep_ms(1)
            adc_value = adc_value + ReadAdc(2, ADC1_DATA, ADC1_CLK)
          adc_value  = adc_value/10
          time.sleep_ms(1)  
          #print("ADC1:",adc_value) 
          trasm_float = 1.0*(adc_value - tara)
          trasm_float = trasm_float*calfactor
          trasm_val = int(trasm_float)
          print("ADC1:",trasm_val) 
          if 1==1:
            tm_buf[2] = (trasm_val & 0xFF0000)>>16
            tm_buf[3] = (trasm_val & 0xFF00)>>8
            tm_buf[4] = (trasm_val & 0xFF)
          else :
            tm_buf[2] = (adc_value & 0xFF0000)>>16
            tm_buf[3] = (adc_value & 0xFF00)>>8
            tm_buf[4] = (adc_value & 0xFF) 
          uart.write(tm_buf)  
     
   #
    time.sleep_ms(500)
  time.sleep_ms(1000)





















